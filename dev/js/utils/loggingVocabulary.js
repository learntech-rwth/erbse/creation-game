let _verbs = {
  moved: {
    "id":"moved", 
    "display": {
      "en-US": "moved"
    }
  },
  added: {
    "id":"added", 
    "display": {
      "en-US": "added"
    }
  },
  inserted: {
    "id" : "inserted", 
    "display": {
      "en-US": "inserted"
    }
  },
  appended: {
    "id" : "appended",
    "display": {
      "en-US": "appended"
    }
  },
  removed: {
    "id" : "removed",
    "display": {
      "en-US": "removed"
    }
  }
};

let _objects = {

}