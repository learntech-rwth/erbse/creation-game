function saver() {
    const STORAGE_KEY = "GAMESTATE_STORAGE";
    const GAMESTATE_KEY = "STORED_GAMESTATE";
    const ASSOCIATED_UUID = "ASSOCIATED_UUID";

    this.gameStateStorage = {};

    this.save = () => {
        this.setEntry(ASSOCIATED_UUID, _logger.getUUID())
        if(window.hasOwnProperty("gameState")){
            this.setEntry(GAMESTATE_KEY, gameState);
        }
    }

    this.load = () => {
        if(this.storageExists()){
            if(this.hasSavedGameState()){
                gameState = JSON.parse(JSON.stringify(this.gameStateStorage[GAMESTATE_KEY]));
                // Creation game: cheat = true => MTLG.lc.levelFinished(gameState) loads exactly the level specified instead of the next one!
                gameState.cheat = true;
            }
            else {
                gameState = {nextLevel: "level1"};
                this.setEntry(GAMESTATE_KEY, gameState);
            }
        }
        else {
            this.createStorage();
        }

        if(!window.hasOwnProperty("gameState") || gameState == null || gameState == {}){
            gameState = {nextLevel: "level1"};
            this.setEntry(GAMESTATE_KEY, gameState);
        }

        return gameState;
    }

    this.checkUUID = () => {
        if(this.storageExists()){
            let currentUUID = _logger.getUUID();
            this.gameStateStorage = JSON.parse(localStorage.getItem(STORAGE_KEY));
            if(!(ASSOCIATED_UUID in this.gameStateStorage) || currentUUID != this.gameStateStorage[ASSOCIATED_UUID]){
                this.reset();
            }
        }
    }

    this.reset = () => {
        if(this.storageExists()){
            localStorage.removeItem(STORAGE_KEY);
        }
    }

    this.storageExists = () => {
        if (window.localStorage.hasOwnProperty(STORAGE_KEY)) {
            return true;
        }
        return false;
    }

    this.hasSavedGameState = () => {
        if(this.storageExists() && GAMESTATE_KEY in this.gameStateStorage && this.gameStateStorage[GAMESTATE_KEY] != {}){
            return true;
        }
        return false;
    }

    this.init = () => {
        this.checkUUID();
        if(this.storageExists()){
            this.gameStateStorage = JSON.parse(localStorage.getItem(STORAGE_KEY));
        }
    }

    this.initStorage = () => {
        this.setEntry(ASSOCIATED_UUID, _logger.getUUID());
    }

    this.setEntry = (key, value) => {
        this.gameStateStorage[key] = value;
        let gameStateString = JSON.stringify(this.gameStateStorage);
        window.localStorage.setItem(STORAGE_KEY, gameStateString);
    }

    this.createStorage = () => {
        this.gameStateStorage = {};
        let gameStateString = JSON.stringify(this.gameStateStorage);
        window.localStorage.setItem(STORAGE_KEY, gameStateString);
        this.initStorage();
    }

    this.getNextLevel = () => {
        if(this.hasSavedGameState() && "nextLevel" in this.gameStateStorage[GAMESTATE_KEY]){
            return this.gameStateStorage[GAMESTATE_KEY]["nextLevel"]
        }
        return "level1"
    }

    this.init();
}
  