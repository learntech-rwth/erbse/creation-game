/**
 * Creates Button
 */
function button(x, y, label, callback, stage = null, options = null){

  let defaultOptions = {
    txtColor: MTLG.getOptions().button_txtColor1,
    bgColor: MTLG.getOptions().button_bgColor1,
  };

  options = Object.assign(defaultOptions, options);
  this._view = createTextBoxButton(label, options);
  //this._view.removeAllEventListeners();
  this._view.addEventListener('pressup', callback);
  this._view.x = x;
  this._view.y = y;

  stage.addChild(this._view);
}

/*
 * Colored Button
 */
function cbutton(x, y, label, callback, colorscheme, stage = null, options = null){

 let defaultOptions;

 switch(colorscheme){
   case "blue":
   defaultOptions = {
     txtColor: MTLG.getOptions().button_txtColor1,
     bgColor: MTLG.getOptions().button_bgColor1,
   };
    break;
    case "red":
    defaultOptions = {
     txtColor: MTLG.getOptions().button_txtColor2,
     bgColor: MTLG.getOptions().button_bgColor2,
    };
    break;
    default:
    console.log("Some Buttons have no valid colorscheme, used scheme blue");
    defaultOptions = {
     txtColor: MTLG.getOptions().button_txtColor1,
     bgColor: MTLG.getOptions().button_bgColor1,
    };
  }

  options = Object.assign(defaultOptions, options);

  this._view = createTextBoxButton(label, options);
  this._view.x = x;
  this._view.y = y;

  //this._view.removeAllEventListeners();
  this._view.addEventListener('pressup', callback);
  //this._view.addEventListener('mouseover', colorChange(options.moColor).bind(this));

  stage.addChild(this._view);

  /* Experimental
    function colorChange(newColor){
    shape = this._view.children.find(elem => elem.myType === "Rect");
    shape.color = newColor;
  }*/
}

/**
 * Creates Box
 */
function createBox(x, y, width, height, options = null) {
  let defaultOptions = {
    bgColor: MTLG.getOptions().box_bgColor,
    alpha: MTLG.getOptions().box_alpha,
  }
  options = Object.assign(defaultOptions, options);

  let retCont = new createjs.Container();
  let shape = new createjs.Shape();
  shape.graphics.beginFill(options.bgColor).drawRect(0, 0, width, height);
  shape.myType = "Rect";
  shape.alpha = options.alpha;
  retCont.addChild(shape);
  retCont.x = x;
  retCont.y = y;
  return retCont;
}

/**
 * Creates Text
 */
function createText(label, options = null) {

  let defaultOptions = {
    txtColor: MTLG.getOptions().text_txtColor,
    bgColor: MTLG.getOptions().text_bgColor,
    padding: MTLG.getOptions().text_padding,
    fontSize: MTLG.getOptions().text_fontSize,
    font: MTLG.getOptions().text_font,
    lineWidth: MTLG.getOptions().text_lineWidth
  }
  options = Object.assign(defaultOptions, options);

  let retCont = new createjs.Container();
  let text = new createjs.Text(label+" ", options.fontSize+" "+options.font, options.txtColor);
  text.set({lineWidth: (options.lineWidth-2*options.padding.lr)});
  text.textBaseline = "top";
  text.x = options.padding.lr; //TODO
  text.y = options.padding.tb;
  text.myType = "Text";
  //text.outline = 1;
  retCont.text = text;
  let bounds = text.getBounds();
  bounds.width += options.padding.lr*2; //TODO
  bounds.height += options.padding.tb*2; //TODO

  retCont.addChild(text);
  retCont.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);

  /**
   * Appends character to text in input bar
   */
  retCont.addChar = function (newChar) {
    // console.log("add char "+newChar);
    this.text = this.text.trim()+newChar.trim();
    // logging event
    _logger.log(`Player added new character "${newChar.trim()}" via keyboard.`);
  }.bind(text);

  retCont.setText = function (newText) {
    this.text = newText;
  }.bind(text);

  /**
   * Removes last character of text in input bar
   */
  retCont.removeChar = function () {
    // console.log("remove last char");
    this.text = this.text.slice(0, -1);
    // logging event
    _logger.log(`Player removed last character with 'Backspace' key.`);
  }.bind(text);

  /**
   * Returns text in text element
   */
  retCont.getText = function () {
    return this.text;
  }.bind(text);

  /**
   * Resets text in text element
   */
  retCont.resetText = function () {
    this.text = "";
    this.color = "#000000"
  }.bind(text);

  return retCont;
}

/**
 * Creates TextBox of a Button
 */
function createTextBoxButton(label, options = null) {

  defaultOptions = {
    txtColor: MTLG.getOptions().textbox_txtColor,
    bgColor: MTLG.getOptions().textbox_bgColor,
    strkColor: MTLG.getOptions().textbox_strkColor,
    padding : MTLG.getOptions().textbox_padding
  };
  options = Object.assign(defaultOptions, options);

  let retCont = new createjs.Container();
  let text = new createjs.Text(label, "30px Arial", options.txtColor);
  text.lineWidth = options.lineWidth;
  text.lineHeight = options.lineHeight;
  text.x = options.padding.lr; //TODO
  text.y = options.padding.tb;
  text.textBaseline = "top";
  text.myType = "Text";

  let bounds = text.getBounds();
  bounds.width += options.padding.lr*2; //TODO
  bounds.height += options.padding.tb*2; //TODO

  let shape = new createjs.Shape();
  shape.color = shape.graphics.beginFill(options.bgColor).command;
  shape.graphics.drawRoundRect(bounds.x, bounds.y, bounds.width, bounds.height, 5);
  shape.myType = "Rect";

  retCont.addChild(shape);
  retCont.addChild(text);
  retCont.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);

  return retCont;
}

/**
 * Creates TextBox
 */
function createTextBox(label, options = null) {

  defaultOptions = {
    txtColor: MTLG.getOptions().textbox_txtColor,
    bgColor: MTLG.getOptions().textbox_bgColor,
    strkColor: MTLG.getOptions().textbox_strkColor,
    padding : MTLG.getOptions().textbox_padding
  };
  options = Object.assign(defaultOptions, options);

  let retCont = new createjs.Container();
  //retCont.dragStartX;
  //retCont.dragStartY;
  retCont.dragged = false;
  retCont.clicked
  let text = new createjs.Text(label, "30px Arial", options.txtColor);
  text.lineWidth = options.lineWidth;
  text.lineHeight = options.lineHeight;
  text.x = options.padding.lr; //TODO
  text.y = options.padding.tb;
  text.textBaseline = "top";
  text.myType = "Text";

  let bounds = text.getBounds();
  bounds.width += options.padding.lr*2; //TODO
  bounds.height += options.padding.tb*2; //TODO

  let shape = new createjs.Shape();
  shape.color = shape.graphics.beginFill(options.bgColor).command;
  shape.graphics.beginStroke(options.strkColor).drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
  shape.myType = "Rect";

  retCont.addChild(shape);
  retCont.addChild(text);
  retCont.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);
  retCont.addEventListener('pressmove', draggable);
  retCont.addEventListener('pressup', hitObject);
  retCont.addEventListener('click', moveIt);

  return retCont;
}

/**
 * Creates TextBox for Tutorial
 */
function createTextBoxTutorial(label, options = null) {

  defaultOptions = {
    fontSize: MTLG.getOptions().textboxTutorial_fontSize,
    txtColor: MTLG.getOptions().textboxTutorial_txtColor,
    bgColor: MTLG.getOptions().textboxTutorial_bgColor,
    padding : MTLG.getOptions().textboxTutorial_padding
  };
  options = Object.assign(defaultOptions, options);

  let retCont = new createjs.Container();
  let text = new createjs.Text(label, options.fontSize, options.txtColor);
  text.lineWidth = options.lineWidth;
  text.lineHeight = options.lineHeight;
  text.x = options.padding.lr; //TODO
  text.y = options.padding.tb;
  text.textBaseline = "top";
  text.myType = "Text";

  let bounds = text.getBounds();
  bounds.width += options.padding.lr*2; //TODO
  bounds.height += options.padding.tb*2; //TODO

  let shape = new createjs.Shape();
  var changeColor = shape.graphics.beginFill(options.bgColor).command;
  shape.graphics.drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
  shape.myType = "Rect";

  retCont.addChild(shape);
  retCont.addChild(text);
  retCont.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);

  // enable color changes for events
  retCont.changeColor = function(newColorString){
    changeColor.style = newColorString;
  };

  return retCont;
}

/**
 * Implements drag and drop of URL parts
 */
function draggable (e) { // eslint-disable-line no-unused-vars
  if(_globalController.movementUrlParts === "drag" || _globalController.movementUrlParts === "clickdrag"){
  // if Text is selected set target to Container
  let target = e.target;
  //if (e.target.toString().slice(1, 5) === 'Text' || e.target.toString().slice(1, 5) === 'Bitm' || e.target.toString.slice(1,5) === 'Rect') {
  if (["Text", "Rect"].includes(e.target.myType)){
    target = e.target.parent;
  }

  // Testing Click + Drag
  target.dragged = true;

  // translates local coordinates in container (where the event occurs)
  // to the general coordinates on stage
  let coords = target.localToLocal(e.localX, e.localY, MTLG.getStageContainer());
  target.x = coords.x;
  target.y = coords.y;

  let urlBarView = _globalController.getUrlBarView();

  coords = e.target.localToLocal(0,0, urlBarView._view);
  if(coords.x > 0 && coords.x < urlBarView.width && coords.y > 0 && coords.y < urlBarView.height){

    let addedViews = urlBarView.getAddedViews();
    let insertPosition = addedViews.length;

    if (addedViews.length > 0) {
      let first = addedViews[0];
      let widths = addedViews.map((x)=>{return x.width});

      let left = 0;
      for (let i = 0; i<widths.length; i++) {
        let currentView = addedViews[i];
        let width = currentView.width;
        let right = left + width;

        //console.log("left: "+left+"; right: "+right+"; current: "+coords.x);
        if (coords.x >= left && coords.x < right) {
          let middle = left + width/2;
          // console.log("on elem: ",addedViews[i]);
          if (coords.x < middle) {
            // here we now want to position the elem in between i-1 and i
            //console.log("before ", currentView);
            insertPosition = i;
          } else {
            // here we now want to position the elem in between i and i+1
            //console.log("after ", currentView);
            insertPosition = i + 1;
          }
        }
        left += width;
      }
    }
  }
  /*

  this.x = coords.x
  this.y = coords.y

  /// ////////////////
  // verbose event logging
  /*
  let globalCoords = {
    x: e.stageX,
    y: e.stageY
  }

  this.moveCoord.push({
    x: globalCoords.x,
    y: globalCoords.y
  })
  if (this.moveCoord.length > 1) {
    this.moveDiff.push(Math.sqrt(Math.pow(globalCoords.x - this.moveCoord[this.moveCoord.length - 2].x, 2), Math.pow(globalCoords.y - this.moveCoord[this.moveCoord.length - 2].y, 2)))
  } else {
    this.moveDiff.push(Math.sqrt(Math.pow(globalCoords.x - this.startX, 2), Math.pow(globalCoords.y - this.startY, 2)))
  }
  /// //////////////////
  //
  */
}
}

/**
 * Implements add and remove by click of URL parts to URL bar
 */
function moveIt (e){
  if(_globalController.movementUrlParts === "click" || _globalController.movementUrlParts === "clickdrag"){
    console.log("Testmove");

    if (["Text", "Rect"].includes(e.target.myType)) {
      target = e.target.parent;
    }

    if(target.dragged !== false) {
      return;
    }

    target.clicked = true;
    _globalController.addPart(target.viewObject);
    target.removeEventListener('click', moveIt);

    if (_globalController.movementUrlParts === "click") {
      target.addEventListener('click', removeObject);
    } else {
      target.addEventListener('pressdown', removeObject);
      target.addEventListener('mousedown', removeObject);
    }
  }
}

/**
 * Implements removal of URL parts from URL bar
 */
function removeObject (e) {

  if (["Text", "Rect"].includes(e.target.myType)){
    target = e.target.parent
  }

  _globalController.removePart(target.viewObject);

  if(_globalController.movementUrlParts === "drag" || _globalController.movementUrlParts === "clickdrag"){
    target.removeEventListener('pressdown', removeObject);
    target.removeEventListener('mousedown', removeObject);
  }

  if(_globalController.movementUrlParts === "click" /*|| _globalController.movementUrlParts === "clickdrag" (_globalController.movementUrlParts === "clickdrag" && Math.abs(target.dragStartX - target.x) < 10 && Math.abs(target.dragStartY - target.y) < 10)*/){
    let logicTarget = _globalController.urlPartViews.find(elem => elem._view.viewObject === target.viewObject);
    if(_globalController.movementUrlParts === "click"){
      target.removeEventListener('click', removeObject);
      target.visible = false;
    }
    let i = 0;
    do {
      coords = _globalController.urlPartAreaView.getRandomCoords(logicTarget);
      target.x = coords.x;
      target.y = coords.y;
      i++;
    } while (hasOverlap(logicTarget, _globalController.urlPartViews) && i<100);
    target.visible = true;
  }
  if(_globalController.movementUrlParts === "click" || _globalController.movementUrlParts === "clickdrag"){
    target.addEventListener('click', moveIt);
  }

}

/**
 * Implements hit test with URL bar
 */
function hitObject (e) {
  // Check if bar was hit
  // Append current object to end of bar
  // Via controller
  console.log("Testhit", _globalController.movementUrlParts);
  if(_globalController.movementUrlParts === "drag" || _globalController.movementUrlParts === "clickdrag"){

    if (["Text", "Rect"].includes(e.target.myType)){
      target = e.target.parent;
    }

    let offset = {
      x: 2,
      y: 4
    };

    // Would end in a double add otherwise in click + drag mode
    if(target.clicked === true) { // testing click + drag
      target.clicked = false;
      return;
    }

    let urlBarView = _globalController.getUrlBarView();

    // Check if bar was hit
    let coords = e.target.localToLocal(0,0, urlBarView._view);

    // Testing click + drag
    target.dragged = false;

    let urlPartView = _globalController.getUrlPartView(target.viewObject)

    if(coords.x > 0-urlPartView.width*0.75 &&
       coords.x < urlBarView.width &&
       coords.y > 0-urlPartView.height &&
       coords.y < urlBarView.height ) {

      let addedViews = urlBarView.getAddedViews();
      let insertPosition = addedViews.length;

      if (addedViews.length > 0) {
        let first = addedViews[0];
        let widths = addedViews.map((x)=>{return x.width});

        let left = 0;
        for (let i = 0; i<widths.length; i++) {
          let currentView = addedViews[i];
          let width = currentView.width;
          let right = left + width;

          //console.log("left: "+left+"; right: "+right+"; current: "+coords.x);
          if (coords.x >= left && coords.x < right) {
            let middle = left + width/2;
            // console.log("on elem: ",addedViews[i]);
            if (coords.x < middle) {
              // here we now want to position the elem in between i-1 and i
              //console.log("before ", currentView);
              insertPosition = i;
            } else {
              // here we now want to position the elem in between i and i+1
              //console.log("after ", currentView);
              insertPosition = i + 1;
            }
          }
          left += width;
        }
      }

      target.x = urlBarView.x + offset.x;
      target.y = urlBarView.y + offset.y;
      //console.log(target.x, target.y);
      if (insertPosition == addedViews.length) {
        _globalController.addPart(target.viewObject);
      } else {
        //console.log(insertPosition);
        _globalController.insertPart(target.viewObject, insertPosition);
      }
      target.addEventListener('pressdown', removeObject);
      target.addEventListener('mousedown', removeObject);
      //console.log(target);

      //TODO: This does not yet work

      //target.x = global_bar._view.localToLocal(0,0, MTLG.getStageContainer()).x;
      //target.y = global_bar._view.localToLocal(0,0, MTLG.getStageContainer()).y;
      //for(item of global_bar.addedViews){
      //console.log(item);
      //target.x += item._view.getBounds().x;
      //}
      //global_bar.addedViews.push(target.viewObject);
    } else {
      // logging event
      _logger.log(`Player moved UrlPart "${_globalController.getUrlPart(target.viewObject).getLabel()}".`);
    }
  }
}

/**
 * Returns random number is given range
 */
function getRandomNumber (min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}

/**
 * Checks whether overlaps of UrlPartViews exist
 */
function hasOverlap (urlPartView, urlPartViews) {
  for (tmpUrlPartView of urlPartViews) {
    //console.log(tmpUrlPartView, urlPartView);
    if (tmpUrlPartView.getId() != urlPartView.getId()) {
      let isRight = tmpUrlPartView.getX() + tmpUrlPartView.width < urlPartView.getX();
      let isLeft = urlPartView.getX() + urlPartView.width < tmpUrlPartView.getX();
      let isAbove = urlPartView.getY() + urlPartView.height < tmpUrlPartView.getY();
      let isBelow = tmpUrlPartView.getY() + tmpUrlPartView.height < urlPartView.getY();
      if (isRight || isLeft || isAbove || isBelow) {
        continue;
      } else {
        //console.log(tmpUrlPartView, urlPartView);
        return true;
      }
    }
  }
  return false;
}

/**
* returns a container with the specified picture. The picture gets scaled to the specified width and height
* the bitmap can have a hitArea as an overlay
* the bitmap is at x=y=0
* the bitmap can be generated invisible
* @param {string} mtlgBitmap - a bitmap returned by MTLG.assets.getBitmap()
* @param {number} width - picture width
* @param {number} height - picture height
* @param {boolean} hitarea - if true, the picture will have a rectangular hitarea with its width and height
* @param {boolean} stealth - if true, the picture will spawn with visible = false
* @param {boolean} regXYOnTop - if true, the center of the picture (for rotation and movement) is on the midpoint of the top border
*/
function createPicture(mtlgBitmap, width=42, height=42, hitarea=false, stealth=false, regXYOnTop=false)
{

  //Bild lässt sich irgendwie nur im Container skalieren und zentrieren, ohne die hitArea falsch zu platzieren
  var picture1 = mtlgBitmap;
  var picture = new createjs.Container();
  picture.addChild(picture1);

  let widthmiddle = width/2;
  let heightmiddle = height/2;

  picture1.scaleX = width/picture1.image.width;
  picture1.scaleY = height/picture1.image.height;

  picture.regX = widthmiddle;
  if(!regXYOnTop)
  {
    picture.regY = heightmiddle;
  }
  else if(regXYOnTop)
  {
    picture.regY = 0;
  }


  if(hitarea)
  {
      var hitBox = new createjs.Shape();
      hitBox.graphics.beginFill("#FFFFFF").drawRect(0,0,width,height);

      //alpha = 0 makes this ignore click events
      // so 0.01 is the least visible but working overlay
      hitBox.alpha = 0.01;
      picture.hitArea = hitBox;

      picture.addChild(hitBox);
  }

  if(stealth)
  {
     picture.visible = false;
  }

  return picture;
}