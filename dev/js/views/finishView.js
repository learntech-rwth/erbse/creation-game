function finishView(gameState, stage) {
  this.gameState = gameState;
  this.stage = stage;

  this.width = 1000;
  this.title = l('finish-title');
  this.text = l('finish-text-1') + ((gameState.points === undefined || isNaN(gameState.points)) ? "0" : gameState.points) + " " + l('finish-text-2') + l('finish-text-3');

  this._boxView = createBox((MTLG.getOptions().width-this.width)/2, 150, this.width, MTLG.getOptions().height-2*150, {bgColor:"#304b78"});

  this._titleView = createText(this.title, {
    txtColor: "#ffffff",
    padding : {
      lr: 50, //left-right padding
      tb: 25 //top-bottom padding
    },
    fontSize:"bold 40px",
    lineWidth: this.width-50
  });
  //this._titleView.setText("Congratulations!\n\nYou finished the game!");
  this._titleView.x = (MTLG.getOptions().width-this.width)/2;
  this._titleView.y = 150;

  this._textView = createText(this.text,{
    txtColor: "#ffffff",
    padding : {
      lr: 50, //left-right padding
      tb: 25 //top-bottom padding
    },
    lineWidth: this.width
  });
  //this._textView.setText((result.successful) ? "Congratulations!": "Sorry, this was wrong.");
  this._textView.x = (MTLG.getOptions().width-this.width)/2;
  this._textView.y = 250;

  let character = new characterView(this.stage);

  this.stage.addChild(this._boxView);
  this.stage.addChild(this._titleView);
  this.stage.addChild(this._textView);
}
