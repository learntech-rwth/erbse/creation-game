/**
 * Container for input bar and 'generate' button
 */
function inputAreaView(inputBar, x, y, width, height, stage) {
  this.inputBar = inputBar;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;

  this._container = new createjs.Container();
  this._container._stage = this.stage;
  this._container.x = this.x;
  this._container.y = this.y;

  this._labelView = createText(l('url-part-type-exp'), {txtColor:"#ffffff"});
  this._labelView.x = 0;
  this._labelView.y = 0;

  this._container.addChild(this._labelView);

  this._inputBarView = new inputBarView(inputBar, 0, 50, width, 50, this._container);

  this.addGeneratedUrlPart = () => {
    this._inputBarView.addGeneratedUrlPart();
  }

  this.activateInputBar = () => {
    this._inputBarView.activate();
  }

  this.deactivateInputBar = () => {
    this._inputBarView.deactivate();
  }

  this._generateButtonView = new button(this.width + 20, 50, l('button_Generate'), this.addGeneratedUrlPart, this._container, {bgColor:"#56bca4"});

  stage.addChild(this._container);
}
