function characterView(stage) {
  let figure = createPicture(MTLG.assets.getBitmap("img/"+MTLG.getOptions().characterImg), 300*1.5, 225*1.5);
    figure.x = 200;
    figure.y = 300;
    stage.addChild(figure);
  return figure;
}