function statusAreaView(gameState, x, y, width, height, stage) {
  this.gameState = gameState;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;

  this._container = new createjs.Container();
  this._container.x = this.x;
  this._container.y = this.y;

  this._titleView = createText(l('score'), {
    txtColor: MTLG.getOptions().textbox_statusArea_txtColor,
    padding : {
      lr: 50, //left-right padding
      tb: 25 //top-bottom padding
    },
    fontSize:MTLG.getOptions().textbox_statusArea_fontSize,
    lineWidth: this.width
  });
  this._titleView.x = 0;
  this._titleView.y = 0;

  //console.log(gameState);
  this.points = this.gameState.points || "0";
  this._textView = createText(this.points+ " " + l('points'),
    {
      txtColor: MTLG.getOptions().textbox_statusArea_txtColor,
      padding : {
        lr: 50, //left-right padding
        tb: 25 //top-bottom padding
      },
      lineWidth: this.width
    });
  this._textView.x = 0;
  this._textView.y = 50;

  this._boxView = createBox(0, 0, this.width, this.height, {bgColor:MTLG.getOptions().textbox_statusArea_bgColor});

  this._container.addChild(this._boxView);
  this._container.addChild(this._titleView);
  this._container.addChild(this._textView);

  this.stage.addChild(this._container);

}
