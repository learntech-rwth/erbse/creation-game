function taskDescriptionView(objective, x, y, width, height, stage) {
  this.objective = objective;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;

  this._container = new createjs.Container();
  this._container.x = this.x;
  this._container.y = this.y;

  this.description = objective.taskDescription;// + "Lorem ipsum dolor sit amet, consetetur sadipscing "+
  //"elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."+
  //" At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus"+
  //" est Lorem ipsum";
  //console.log(this.description.length);



  this._titleView = createText(l('task'), {
    txtColor: MTLG.getOptions().textboxTutorial2_txtColor,
    padding : {
      lr: 50, //left-right padding
      tb: 25 //top-bottom padding
    },
    fontSize:"bold 40px",
    lineWidth: this.width
  });
  this._titleView.x = 0;
  this._titleView.y = 0;

  this._textView = createText(this.description,
    {
      txtColor: MTLG.getOptions().textboxTutorial2_txtColor,
      padding : {
        lr: 50, //left-right padding
        tb: 25 //top-bottom padding
      },
      lineWidth: this.width
    });
  this._textView.x = 0;
  this._textView.y = 50;

  this._boxView = createBox(0, 0, this.width, this.height, {bgColor: MTLG.getOptions().textboxTutorial2_bgColor});

  this._container.addChild(this._boxView);
  this._container.addChild(this._titleView);
  this._container.addChild(this._textView);

  this.stage.addChild(this._container);
}
