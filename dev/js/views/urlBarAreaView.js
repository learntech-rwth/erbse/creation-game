function urlBarAreaView(urlBar, x, y, width, height, stage) {
  this.urlBar = urlBar;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;

  this._container = new createjs.Container();
  this._container.x = this.x;
  this._container.y = this.y;

  this._labelView = createText(l("url-build-parts-exp"), {txtColor:"#ffffff"});
  this._labelView.x = 0;
  this._labelView.y = 0;

  this._container.addChild(this._labelView);

  this._urlBarView = new urlBarView(this.urlBar, 0, 50, this.width, this.height, this._container);

  this.verify = () => {
    _globalController.verifyUrl();
  };

  this._verificationButtonView = new button(this._urlBarView.width + 20, 50, l('button_Verify'), this.verify, this._container);

  this.getUrlBarView = () => {
    return this._urlBarView;
  }

  this.stage.addChild(this._container);
}
