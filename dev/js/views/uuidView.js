 /**
  * Use to create the view containing the UUID. Also contains a copy button for
  * copying the UUID to the clipboard.
  *
  * @param {string} uuid - The uuid of the user.
  * @param {number} x - x Position
  * @param {number} y - y Position
  * @param {number} width - width of the container
  * @param {number} height - height of the container
  */
function uuidView(uuid, x = 1550, y = 1100, width = 400, height = 300)
{
    const copyButtonText = MTLG.lang.getString("copy-button");
    let uuidContainer = new createjs.Container();
    uuidContainer.x = x;
    uuidContainer.y = y;

    let text = new createjs.Text(MTLG.lang.getString("playerid") + ": \n" + uuid, MTLG.getOptions().uuidTextFont, "white");
    text.x = -width/2;
    text.y = -height/2 - 80;
    text.textAlign = "left";

    this.isCopied = false;

    let copyButton = new MTLG.utils.uiElements.addButton({
        text: copyButtonText,
        sizeX: width,
        sizeY: height/2 - 80,
        bgColor1: MTLG.getOptions().button_bgColor2,
        textColor: MTLG.getOptions().button_txtColor2,
    },
    () => {
        navigator.clipboard.writeText(uuid);
        this.isCopied = true;
    });
    copyButton.x = 0;
    copyButton.y = height/2 - 240;
    copyButton.cursor = "pointer";

    const buttonContainer = copyButton.children[0];
    const buttonText = copyButton.children[1];
    let buttonTimeout = undefined;

    // Visuell mittiger
    buttonText.y = buttonText.y * 1.05;

    uuidContainer.on("mousedown", () => {
        buttonContainer.alpha = 0.9;
    });

    uuidContainer.on("pressup", () => {
        buttonContainer.alpha = 1;
        buttonText.text = MTLG.lang.getString("copy-button-success");
        if (buttonTimeout) {
            clearTimeout(buttonTimeout);
        }
        buttonTimeout = setTimeout(() => buttonText.text = copyButtonText, 2000);
    });

    uuidContainer.addChild(text);
    //uuidContainer.addChild(copyButton);
    MTLG.getStageContainer().addChild(uuidContainer);
}
