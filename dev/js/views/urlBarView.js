/**
 * View for URL bar
 */
function urlBarView (pUrlBar, x, y, width, height, stage) {
    this.urlBar = pUrlBar; // sets model
    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
    this._view = createBox(this.x, this.y, this.width, this.height); // sets view
    this.addedViews = [];
    stage.addChild(this._view);
  
    /**
     * Adds an UrlPartView at index
     */
    this.addUrlPartView = (newPart, index) => {
      if (Number.isInteger(index)) {
        if (index > this.addedViews.length) {
          index = this.addedViews.length;
        }
        this.addedViews.splice(index, 0, newPart);
      } else {
        this.addedViews.push(newPart);
      }
    }
  
    /**
     * Returns added UrlPartViews
     */
    this.getAddedViews = () => {
      return this.addedViews
    };
  
    /**
     * Realign UrlPartViews
     */
    this.realignUrlPartViews = (curIndex) => {
      // Set next objs x to new values
      if(curIndex == 0){
        if(this.addedViews.length > 0){
          this.addedViews[curIndex].align(0); //TODO: Hardcoded
          curIndex++;
        }
      }
      
      if(curIndex < 0){
        console.log("index of removed object is less than 0. This should not happen.");
        return;
      }
  
      for(i = curIndex; i <= this.addedViews.length -1; i++){
        let lastView = this.addedViews[i - 1];
        this.addedViews[i].align(lastView._view.x + lastView.width);
      }
    }
  }