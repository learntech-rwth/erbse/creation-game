/**
 * View of generated URL Part
 *
 * Extension of UrlPartView
 */
function generatedUrlPartView(pUrlPart, id, x = 0, y = 0, stage = null, options = null) {
  let defaultOptions = {
    bgColor: MTLG.getOptions().urlPart_generated_bgColor,
    txtColor: MTLG.getOptions().urlPart_generated_txtColor, 
    padding:{lr:10, tb:6}
  };
  options = Object.assign(defaultOptions, options);

  urlPartView.call(this, pUrlPart, id, x, y, stage, options);
}

generatedUrlPartView.prototype = Object.create(urlPartView.prototype);
