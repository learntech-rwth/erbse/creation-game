function resultView(result, x, y, width, height, callback, stage) {
  this.result = result;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;

  // semi-tranparent overlay to "block" events
  this._overlay = new createjs.Container();
  let gray = new createjs.Shape();
  gray.graphics.beginFill('rgba(68, 68, 68, 0.4)').drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height, 10);
  this._overlay.addChild(gray);
  this._overlay.addEventListener('pressup', function(evt){
    ;
  });
  this.stage.addChild(this._overlay);

  // Deactivate keyboard input
  if(_globalController.inputAreaView){
    _globalController.inputAreaView.deactivateInputBar();
  }

  this._container = new createjs.Container();
  this._container.x = this.x;
  this._container.y = this.y;
  this._container.width = this.width;
  this._container.height = this.height;

  this._boxView = createBox(0, 0, this.width, this.height, {bgColor: MTLG.getOptions().textbox_result_bgColor, alpha:0.95});

  this._titleView = createText(result.text,{
    txtColor: MTLG.getOptions().textbox_result_txtColor,
    padding : {
      lr: 50, //left-right padding
      tb: 25 //top-bottom padding
    },
    fontSize: MTLG.getOptions().textbox_result_fontSize,
    lineWidth: this.width-50
  });
  this._titleView.setText((result.successful) ? l('result_success') + result.points+ " " + l('points')+ "!": l('result_fail'));
  this._titleView.x = 50;
  this._titleView.y = 50;

  this._textView = createText(result.text,{
    txtColor: "#ffffff",
    padding : {
      lr: 50, //left-right padding
      tb: 25 //top-bottom padding
    },
    lineWidth: this.width
  });
  //this._textView.setText((result.successful) ? "Congratulations!": "Sorry, this was wrong.");
  this._textView.x = 50;
  this._textView.y = 150;

  this._container.addChild(this._boxView);
  this._container.addChild(this._titleView);
  this._container.addChild(this._textView);

  // Function to remove overlay if pressing continue when successful is false
  let removeSelf = function(){
    _logger.log("Retrying level.");
    this.stage.removeChild(this._container);
    this.stage.removeChild(this._overlay);
    if (MTLG.getOptions().useCountdown) {
      startCountdown();
    } 
  }.bind(this);

  let loadLastTutorial = function(){
    _logger.log("Repeating the last tutorial.");
    _globalController.loadLastTutorial();
  }

  if(result.successful){
    this._continueButton = button(this.width - 250, this.height - 100, l('button_Continue'), callback, this._container); //TODO: Button coords hardcoded
  }else{
    this._continueButton = button(this.width - 250, this.height - 100, l('button_tryAgain'), removeSelf, this._container); //TODO
    this._lastTutButton = cbutton(this.width - 600, this.height - 100, l('button_lastTut'), loadLastTutorial, "red", this._container);
  }

  this.stage.addChild(this._container);
}
