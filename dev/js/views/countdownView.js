
// Function to show the ticking countdown
function countdownView(countdownTime, x, y, stage){
  this.x = x;
  this.y = y;
  this.stage = stage;

  var countdownText = stage.getChildByName("countdownText");
  if(countdownText === null){
    countdownText = MTLG.utils.gfx.getText(l('countdown_timer_text') + countdownTime + "s", "32px Arial", MTLG.getOptions().countdown_color);
    countdownText.name = "countdownText";
    countdownText.textAlign = 'center';
    countdownText.x = this.x;
    countdownText.y = this.y;
    stage.addChild(countdownText);
  } else {
    countdownText.text = l('countdown_timer_text') + countdownTime + "s";
  }
}

// display expiration view
function expirationView(x, y, width, height, stage) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;

  // semi-tranparent overlay to "block" events
  this._overlay = new createjs.Container();
  let gray = new createjs.Shape();
  gray.graphics.beginFill('rgba(68, 68, 68, 0.4)').drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height, 10);
  this._overlay.addChild(gray);
  this._overlay.addEventListener('pressup', function(evt){
    ;
  });
  this.stage.addChild(this._overlay);

  // Deactivate keyboard input
  if(_globalController.inputAreaView){
    _globalController.inputAreaView.deactivateInputBar();
  }

  this._container = new createjs.Container();
  this._container.x = this.x;
  this._container.y = this.y;
  this._container.width = this.width;
  this._container.height = this.height;

  this._boxView = createBox(0, 0, this.width, this.height, {bgColor: MTLG.getOptions().textbox_countdown_bgColor, alpha:0.95});

  this._titleView = createText(l('countdown_expiration_headline'),{
    txtColor: MTLG.getOptions().textbox_countdown_txtColor,
    padding : {
      lr: 50, //left-right padding
      tb: 25 //top-bottom padding
    },
    fontSize:MTLG.getOptions().textbox_countdown_fontSize,
    lineWidth: this.width-50
  });
  this._titleView.x = 50;
  this._titleView.y = 50;

  this._textView = createText(l('countdown_expiration_text'),{
    txtColor: MTLG.getOptions().textbox_countdown_txtColor,
    padding : {
      lr: 50, //left-right padding
      tb: 25 //top-bottom padding
    },
    lineWidth: this.width
  });
  this._textView.x = 50;
  this._textView.y = 150;

  this._container.addChild(this._boxView);
  this._container.addChild(this._titleView);
  this._container.addChild(this._textView);

  // Function to remove overlay if pressing continue when successful is false
  let removeSelf = function(){
    this.stage.removeChild(this._container);
    this.stage.removeChild(this._overlay);
    startCountdown();
  }.bind(this);

  let loadLastTutorial = function(){
    _globalController.loadLastTutorial();
  }

  this._continueButton = button(this.width - 250, this.height - 100, l('button_tryAgain'), removeSelf, this._container);
  this._lastTutButton = cbutton(this.width - 600, this.height - 100, l('button_lastTut'), loadLastTutorial, "red", this._container);


  this.stage.addChild(this._container);
}
