function urlPartAreaView(x, y, width, height, stage) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.stage = stage;

  this._container = new createjs.Container();
  this._container.x = x;
  this._container.y = y;
  this._container.width = width;
  this._container.height = height;
  this._container.stage = stage;

  this.urlPartViews = [];

  //this._rect = createBox(0, 0, width, height, {bgColor: "#343434"});

  this._container.addChild(this._rect);

  this.getRandomCoords = (urlPartView) => {
    let width = urlPartView.width;
    let height = urlPartView.height;
    let x = getRandomNumber(this.x, this.x + this.width - width);
    let y = getRandomNumber(this.y, this.y + this.height - height);
    return {x, y};
  }

  this.addUrlPartViews = (urlParts) => {
    let index = 0;

    for (part of urlParts) {
      let newUrlPartView = new urlPartView(part, index, 0, 0, this.stage);
      let coords = null;
      let i = 0;
      do {
        coords = this.getRandomCoords(newUrlPartView);
        newUrlPartView.setCoords(coords);
        i++;
      } while (hasOverlap(newUrlPartView, this.urlPartViews) && i<100);
      this.urlPartViews.push(newUrlPartView);
      index++;
    }
  }

  stage.addChild(this._container);

}