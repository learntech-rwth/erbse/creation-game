function menuButtons(gameState, stage, callback = null) {
  this.gameState = gameState;
  this.stage = stage;

  this.returnHome = () => {
    // callback first
    if(callback){
      callback();
    }
    //MTLG.lc.levelFinished({'cheat':true, 'nextLevel':'level1', 'level':8, 'preset':0, 'points': 1000000});
    if(_globalController && _globalController.countdown != undefined){
      clearInterval(_globalController.countdown);
    }
    this.gameState['nextLevel'] = "selection";
    MTLG.lc.levelFinished(this.gameState);
  }

  this._homeButtonView = new button(50, 50, l("button_Menu"), this.returnHome, this.stage, {bgColor: MTLG.getOptions().menubutton_bgColor});
}
