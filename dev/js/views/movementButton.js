// movement URL parts Buttons
function movementButton (stage){

  this.stage = stage;

  this._movementButtonView = new cbutton(50, 900, l('button_movement'), changeMovement, "red", this.stage);



  function changeMovement(){
    if(_globalController.movementUrlParts === "drag"){
      _globalController.movementUrlParts = "click";
    } else{
      _globalController.movementUrlParts = "drag";
    }
  }
}
