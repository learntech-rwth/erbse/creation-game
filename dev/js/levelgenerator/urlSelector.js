/**
 * Module for returning predifined phishing URLs for comparability in studies.
 * Expected functionality: Has to have a function generateUrl(type, url) like specified below akin to the urlGenerator module.
 * @module urlSelector
 */

 let urlSelector = (function(){
    // CURRENTLY UNUSED: DOES NOT MAKE A LOT OF SENSE WITH THE CURRENT IMPLEMENTATION
    let selector_minSelectedPersonalized = 10; // minimum amount of different services to be selected 

    let selector_inGameState = [
        {"setName" : "unknownServices", "setChance" : 0.2},
        {"setName" : "knownServices"},
    ]
    let selector_inLP = [
        {"setName" : "defaultServices", "setChance" : 0},
        {"setName" : "browserServices", "setChance" : 0},
        {"setName" : "probablyUnknown", "setChance" : 0},
    ]
    let selector_usePredefined = false;

    let predefinedUrls = [
        {'name': "Amazon", 'url': "https://www.amazon.de/"}, 
       //{'name': "Youtube", 'url': "https://accounts.google.com/signin/"}, 
       {'name': "Google", 'url': "https://accounts.google.com/signin/"}, 
       {'name': "Wikipedia", 'url': "https://en.wikipedia.org/w/index.php"}, 
       {'name': "Ebay", 'url': "https://signin.ebay.de/"}, 
       {'name': "Ebay-Kleinanzeigen", 'url': "https://www.ebay-kleinanzeigen.de/m-einloggen.html"}, 
       {'name': "Reddit", 'url': "https://www.reddit.com/login/"}, 
       //{'name': "Reddit", 'url': "https://www.reddit.com/"}, 
       {'name': "Apple", 'url': "https://apple.com/"}, 
       //{'name': "Facebook", 'url': "https://www.facebook.com/"}, 
       {'name': "Facebook", 'url': "https://www.facebook.com/login/"}, 
       {'name': "Twitch", 'url': "https://www.twitch.tv/"}, 
       {'name': "Gmx", 'url': "https://www.gmx.net/"}, 
       {'name': "Web", 'url': "https://web.de/"}, 
       //{'name': "Vk", 'url': "https://vk.com/"}, 
       {'name': "Vk", 'url': "https://vk.com/login"}, 
       {'name': "Yahoo", 'url': "https://login.yahoo.com/"}, 
       {'name': "Netflix", 'url': "https://www.netflix.com/de/login"}, 
       //{'name': "T-online", 'url': "https://accounts.login.idm.telekom.com/"}, 
       {'name': "Live", 'url': "https://login.live.com/"}, 
       //{'name': "Bild", 'url': "https://secure.mypass.de/"}, 
       {'name': "Spiegel", 'url': "https://gruppenkonto.spiegel.de/anmelden.html"}, 
       //{'name': "Microsoft", 'url': "https://login.live.com/"}, 
       {'name': "Otto", 'url': "https://www.otto.de/user/login"}, 
       //{'name': "Wetter", 'url': "https://interaction.7pass.de/"}, 
       {'name': "Mobile", 'url': "https://login.mobile.de/"}, 
       {'name': "Immobilienscout24", 'url': "https://sso.immobilienscout24.de/sso/login"}, 
       {'name': "ZDF", 'url': "https://www.zdf.de/"}, 
       {'name': "Fandom", 'url': "https://www.fandom.com/signin"}, 
       //{'name': "Idealo", 'url': "https://www.idealo.de/"}, 
       {'name': "Idealo", 'url': "https://account.idealo.de/login"}, 
       {'name': "Focus", 'url': "https://www.focus.de/ajax/login/"}, 
       {'name': "Zeit", 'url': "https://meine.zeit.de/anmelden"}, 
       {'name': "RuneScape", 'url': "https://secure.runescape.com/m=weblogin/loginform"}, 
       //{'name': "Steam", 'url': "https://store.steampowered.com/login/"}, 
       {'name': "Deutsche-Bank", 'url': "https://meine.deutsche-bank.de/trxm/db/"}, 
       //{'name': "Commerzbank", 'url': "https://kunden.commerzbank.de/lp/login"}, 
       {'name': "Commerzbank", 'url': "https://www.commerzbank.de/"}, 
       //{'name': "icloud", 'url': "https://idmsa.apple.com/appleauth/auth/authorize/signin"}, 
       {'name': "icloud", 'url': "https://www.icloud.com/"}, 
       {'name': "PayPal", 'url': "https://www.paypal.com"}, 
       //{'name': "Onedrive", 'url': "https://onedrive.live.com/about/en-us/signin"}, 
       {'name': "Dropbox", 'url': "https://www.dropbox.com"}, 
    ];

    function getUrl(){
        let possibleObjects = [];

        if(usePersonalizedData()){    
            for(entry of selector_inLP){
                let setList = _learnerProfile.baseSets;
                for(_set of setList){
                    if(_set.setName == entry.setName && Math.random() < entry.setChance && (possibleObjects == null || possibleObjects.length == 0)){
                        possibleObjects = _set.entries;
                    }
                }
            }
            if(possibleObjects.length == 0){
                for(entry of selector_inGameState){
                    if(entry.setName != "personalizedUrls" && entry.setName in gameState && Math.random() < entry.setChance && (possibleObjects == null || possibleObjects.length == 0)){
                        possibleObjects = gameState[entry.setName]
                    }
                }
            }
            if(possibleObjects == null || possibleObjects.length == 0){
                possibleObjects = gameState.knownServices
            }
        }

        if (possibleObjects == null || possibleObjects.length == 0) {
            console.log("Getting from predefined")
            possibleObjects = predefinedUrls;
        }
 
        return possibleObjects[utils.generateRandomIntBetween(0, possibleObjects.length-1)];
    }

    function usePersonalizedData(){
        return !selector_usePredefined && window.hasOwnProperty("gameState") && MTLG.getOptions().pi_enabled;
    }

    return {
        getUrl : getUrl
    }
})();

