/**
 * Revealing module containing utility functions for the level generators.
 * @module utils
 * 
 * @returns {Object} - Used to reveal all functions inside of the module.
 */

let utils = (function(){

    // Generation functions

    /**
     * Generates a random integer between min and max (inclusive).
     * 
     * @param {Integer} min - The maximum value the generated value can have.
     * @param {Integer} max - The minimum value the generated value can have.
     * @returns {Integer} - The generated integer. 
     * @memberof module:utils
     */
    function generateRandomIntBetween(min, max){
        let res = Math.random() *  (max-min);
        res += min;
        res = Math.round(res);
        return res;
    }

    /*
        Function, returns a char sequence consisting of the chars specified in characters
    */

    /**
     * Returns a char sequence consisting of the chars specified in characters.
     * 
     * @param {Integer} [minLength = 4] - The minimum length of the char sequence.
     * @param {Integer} [maxLength = 15] - The maximum length of the char sequence. 
     * @returns {String} - The generated char sequence.
     * @memberof module:utils
     */
    function generateRandomCharSequence(minLength = 4, maxLength = 15){
        var charSequence = "";
        var characters = "abcdefghijklmnopqrstuvwxyz1234567890";

        for(var i = 0; i < generateRandomIntBetween(minLength,maxLength); i++){
            charSequence += characters.charAt(generateRandomIntBetween(0,characters.length-1));
        }

        return charSequence;
    }

    // Type testing functions

    /*
        Function, returns true if the value is a string
    */

    /**
     * Tests if val is a string.
     * 
     * @param {*} val - The value that is tested.
     * @returns {Boolean} - True if val was a string.
     * @memberof module:utils
     */
    function isString(val){
        return (typeof val === 'string' || val instanceof String)
    }

    /**
     * Tests if val is an integer greater than min.
     * 
     * @param {*} val - The value that is tested.
     * @param {Number} min - The comparison value, val has to be greater than min.
     * @returns {Boolean} - True if val was an integer greater than min.
     * @memberof module:utils
     */
    function isIntOverMin(val, min){
        return (Number.isInteger(val) && val > min);
    }

    /**
     * Tests if val is an integer greater than min and lower than max.
     * 
     * @param {*} val - The value that is tested.
     * @param {Number} min - The comparison value, val has to be greater than min.
     * @param {Number} max - The comparison value, val has to be lower than max.
     * @returns {Boolean} - True if val was an integer greater than min and lower than max.
     * @memberof module:utils
     */
    function isIntBetween(val, min, max){
        return (Number.isInteger(val) && val > min && val < max);
    }

   /**
     * Tests if val is an integer value is an integer greater than or equal to min and less than or equal to max.
     * 
     * @param {*} val - The value that is tested.
     * @param {Number} min - The comparison value, val has to be greater than or equal to min.
     * @param {Number} max - The comparison value, val has to be lower than or equal to max.
     * @returns {Boolean} - True if val was an integer greater than min and lower than max.
     * @memberof module:utils
     */
    function isIntBetweenInclusive(val, min, max){
        return (Number.isInteger(val) && val >= min && val <= max);
    }

    /**
     * Tests if val is a boolean.
     * 
     * @param {*} val - The value that is tested.
     * @returns {Boolean} - True if val was a boolean.
     * @memberof module:utils
     */
    function isBool(val){
        return (val != null && (val === true || val === false));
    }

    /**
     * Tests if the value is neiterh null, [], {} nor "".
     * 
     * @param {*} val - The value to be tested.
     * @returns {Boolean} - True if val was neither null, [], {} nor "".
     * @memberof module:utils
     */
    function isNotEmpty(val){
        return (val != null && val != [] && Object.keys(val).length != 0 && val != "");
    }

    // Object funtions

    /**
     * Traverses the base object along the given path.
     * 
     * @param {Object} base - The base object. 
     * @param {String} path - The path to traverse in the object in dot notation. Example: "example.path" tries to return base.example.path.
     * @returns {Object} - The object after traversal.
     * @memberof module:utils
     */
    function getByPath(base, path){
        let obj = base;
        if(!(path == null || path == "")){
            let pathSplit = path.split(".");  
            for(part of pathSplit){
                if(part in obj){
                    obj = obj[part];
                }
                else return "Error: invalid path";
            }
        }
        return obj;
    }
    
    // Array testing functions

    /*
        Function, returns true if the array contains only values from the specified domain array
    */

    /**
     * Tests if the array contains only values from the domain array.
     * 
     * @param {Array} array - The array to be tested.
     * @param {Array} domain - The domain. 
     * @returns {Boolean} - True if the contains only values from the domain array, false otherwise.
     * @memberof module:utils
     */
    function containsOnlyValuesInDomain(array, domain){
        if(!Array.isArray(array)){
            return false;
        }
        for(entry of array){
            if(!isValueInDomain(entry, domain)){
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if all values in the array pass the test defined by the check function.
     * 
     * @param {Array} array - The array in which all elements are checked using the check function.
     * @param {Function} check - Function that returns true / false, has the property value as first input
     * @param  {...any} [checkArgs] - Additional parameters that are passed into the check function.
     * @returns {Boolean} - True if the contains only entries that pass the test.
     * @memberof module:utils 
     */
    function containsOnlyCorrectElements(array, check, ...checkArgs){
        if(!Array.isArray(array)){
            return false;
        }
        for(entry of array){
            if(!check(entry, ...checkArgs)){
                return false;
            }
        }
        return true;
    }

    /**
     * Tests if a value is contained in a domain.
     * 
     * @param {*} val - The value to be tested.
     * @param {Array} domain - The domain val has to be in.
     * @returns {Boolean} - True if the value is contained in the domain. 
     * @memberof module:utils 
     */
    function isValueInDomain(val, domain){
        if(typeof val === "object"){
            let objAsString = JSON.stringify(val);
            let domainAsStrings = domain.map(x => JSON.stringify(x));
            return domainAsStrings.includes(objAsString);
        }
        return (domain.includes(val));
    }

    return {
        generateRandomIntBetween : generateRandomIntBetween,
        generateRandomCharSequence : generateRandomCharSequence,
        isString : isString,
        isIntOverMin : isIntOverMin,
        isIntBetween : isIntBetween,
        isValueInDomain : isValueInDomain,
        getByPath : getByPath,
        isBool : isBool,
        containsOnlyValuesInDomain : containsOnlyValuesInDomain,
        containsOnlyCorrectElements : containsOnlyCorrectElements,
        isIntBetweenInclusive : isIntBetweenInclusive,
        isNotEmpty : isNotEmpty
    }
})();

if (typeof exports !== 'undefined') {
    module.exports = utils;
}