/**
 * Module for generating phishing URLs.
 * Expected functionality: Has to have a function generateUrl(type, url) like specified below.
 * @module urlManager
 */

 /**
 * An object containing URL data. May have additional properties.
 * 
 * @typedef {Object} urlObject
 * 
 * @property {String} name - Name of the service associated with the URL.
 * @property {String} url - URL under which the service can be accessed.
 */

let urlManager = (function(){
    // Variables and settings for the Creation Game
    let defaultUrls = [
       {'name': "Amazon", 'url': "https://www.amazon.de/"}, 
       //{'name': "Youtube", 'url': "https://accounts.google.com/signin/"}, 
       {'name': "Google", 'url': "https://accounts.google.com/signin/"}, 
       {'name': "Wikipedia", 'url': "https://en.wikipedia.org/w/index.php"}, 
       {'name': "Ebay", 'url': "https://signin.ebay.de/"}, 
       {'name': "Ebay-Kleinanzeigen", 'url': "https://www.ebay-kleinanzeigen.de/m-einloggen.html"}, 
       {'name': "Reddit", 'url': "https://www.reddit.com/login/"}, 
       //{'name': "Reddit", 'url': "https://www.reddit.com/"}, 
       {'name': "Apple", 'url': "https://apple.com/"}, 
       //{'name': "Facebook", 'url': "https://www.facebook.com/"}, 
       {'name': "Facebook", 'url': "https://www.facebook.com/login/"}, 
       {'name': "Twitch", 'url': "https://www.twitch.tv/"}, 
       {'name': "Gmx", 'url': "https://www.gmx.net/"}, 
       {'name': "Web", 'url': "https://web.de/"}, 
       //{'name': "Vk", 'url': "https://vk.com/"}, 
       {'name': "Vk", 'url': "https://vk.com/login"}, 
       {'name': "Yahoo", 'url': "https://login.yahoo.com/"}, 
       {'name': "Netflix", 'url': "https://www.netflix.com/de/login"}, 
       //{'name': "T-online", 'url': "https://accounts.login.idm.telekom.com/"}, 
       {'name': "Live", 'url': "https://login.live.com/"}, 
       //{'name': "Bild", 'url': "https://secure.mypass.de/"}, 
       {'name': "Spiegel", 'url': "https://gruppenkonto.spiegel.de/anmelden.html"}, 
       //{'name': "Microsoft", 'url': "https://login.live.com/"}, 
       {'name': "Otto", 'url': "https://www.otto.de/user/login"}, 
       //{'name': "Wetter", 'url': "https://interaction.7pass.de/"}, 
       {'name': "Mobile", 'url': "https://login.mobile.de/"}, 
       {'name': "Immobilienscout24", 'url': "https://sso.immobilienscout24.de/sso/login"}, 
       {'name': "ZDF", 'url': "https://www.zdf.de/"}, 
       {'name': "Fandom", 'url': "https://www.fandom.com/signin"}, 
       //{'name': "Idealo", 'url': "https://www.idealo.de/"}, 
       {'name': "Idealo", 'url': "https://account.idealo.de/login"}, 
       {'name': "Focus", 'url': "https://www.focus.de/ajax/login/"}, 
       {'name': "Zeit", 'url': "https://meine.zeit.de/anmelden"}, 
       {'name': "RuneScape", 'url': "https://secure.runescape.com/m=weblogin/loginform"}, 
       //{'name': "Steam", 'url': "https://store.steampowered.com/login/"}, 
       {'name': "Deutsche-Bank", 'url': "https://meine.deutsche-bank.de/trxm/db/"}, 
       //{'name': "Commerzbank", 'url': "https://kunden.commerzbank.de/lp/login"}, 
       {'name': "Commerzbank", 'url': "https://www.commerzbank.de/"}, 
       //{'name': "icloud", 'url': "https://idmsa.apple.com/appleauth/auth/authorize/signin"}, 
       {'name': "icloud", 'url': "https://www.icloud.com/"}, 
       {'name': "PayPal", 'url': "https://www.paypal.com"}, 
       //{'name': "Onedrive", 'url': "https://onedrive.live.com/about/en-us/signin"}, 
       {'name': "Dropbox", 'url': "https://www.dropbox.com"}, 
   ];
   let personalizedUrls = [];

   function resetUrls(){
       personalizedUrls = [];
   }

   // Warnungen / Fehler zurückgeben 
   // Function to set the array containing the personalized URLs
   // Resets the array and rewrites it to contain the new data (if fitting information is contained the passed userInfo-object)
   // return value: true if the array was rewritten to now contain the new data
   function personalizeUrls(websiteData){
       var personalized = false;
       if(Array.isArray(websiteData) && websiteData.length > 0) {
           for(let page of userInfo.websites){
               // check if the contained element has all the required parameters
               if('name' in page && 'url' in page){
                   // if fitting data is found, set return value to true and empty the array 
                   if(!personalized){
                       personalized = true;
                       resetUrls();
                   }
                   // add the found data object to the array
                   personalizedUrls.push(page);
               }
           }
       }
   }

   function wasPersonalized(){
       return personalizedUrls.length > 0;
   }

   /**
    * Randomly selects a service from the learner profile / the default values and returns it. 
    * 
    * @memberof module:urlManager
    * @returns {urlObject} - The selected service. 
    */
   function getUrl(){
       if(wasPersonalized()){
           return personalizedUrls[utils.generateRandomIntBetween(0,personalizedUrls.length-1)];
       }
       else {
           return defaultUrls[utils.generateRandomIntBetween(0,defaultUrls.length-1)];
       }
   }

   return {
       getUrl : getUrl,
       personalizeUrls : personalizeUrls,
       resetUrls : resetUrls
   }
})();

if (typeof exports !== 'undefined') {
    module.exports = urlManager;
}
