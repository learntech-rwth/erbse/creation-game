/**
 * Revealing module for generating levels for the Creation Game.
 * @module levelGeneratorCG
 * 
 * @returns {Object} - Contains the revealed functions: {@link module:levelGeneratorCG.initGenerator}, {@link module:levelGeneratorCG.setManager}, {@link module:levelGeneratorFG.createLevel} and {@link module:levelGeneratorFG.resetDefaults}
 */

/**
 * A module that manages the learner profile of the user.
 * 
 * @typedef {Module} urlManager
 * 
 * @see {@link module:urlManager}
 */

/**
 * Contains the settings for level creation.
 * 
 * @typedef {Object} LevelSettings
 *
 * @property {taskOptions} [taskOptions] - Contains the options for type creation. 
 * @property {urlOptions} [urlOptions] - Contains the options for url selection. 
 * @property {Integer} [numberOfPresets] - Number of presets to be generated.
 * @property {String[]} [labels] - Label groups that should be available in the level.
 * @property {Object} [options] - Contains additional settings.
 * @property {Boolean} [options.creationModeEnabled] - Whether the creation mode should be enabled or not.
 */

/**
 * The level template returned to the game. 
 * 
 * @typedef {Object} LevelTemplate
 * 
 * @property {Boolean} creationMode - Whether the creation mode should be enabled or not.
 * @property {Object} config - Contains additional settings.
 * @property {Boolean} config.creationModeEnabled - Whether the creation mode should be enabled or not.
 * @property {Preset[]} presets - Contains the presets used for level creation.
 */

/**
 * 
 * @typedef {Object} Preset
 * 
 * @property {String} domainsType - Preset or generated.
 * @property {String} targetsType - Preset or generated.
 * @property {Boolean} addDomainsLabels - Whether labels should be created for the string in domains or not.
 * @property {Boolean} addTargetsLabels - Whether labels should be created for the object in targets or not.
 * @property {Object} check - Specifies which tests are made and how many points are awarded.
 * @property {Integer} passingPoints - Specifies how many points are needed to complete the level.
 * @property {String} taskDescription - Contains the description of the task.
 * @property {String[]} domains - Array containing the possible domain URLs.
 * @property {urlObject[]} targets - Array containing the possible target URLs.
 */

/**
 * 
 * @typedef {Object} Task
 * 
 * @property {String} taskDescription - Contains the description of the task.
 * @property {Object} check - Specifies which tests are made and how many points are awarded.
 * @property {Integer} passingPoints - Specifies how many points are needed to complete the level.
 */

/**
 * @typedef {Object} taskOptions
 * 
 * @property {Task} [customTasks] - A List of specifically defined task, if contained a randomly selected tasks will be used in all generated presets.
 * @property {0, 1} [difficulty] - A difficulty setting, either 0: tutorial levels or 1: other game levels.
 * @property {Integer[]} [indexList] - Array of indices from which a corresponding task will be chosen randomly if no customTask is specified.
 */

 /**
 * @typedef {Object} urlOptions
 * 
 * @property {urlObject[]} targetList - Array of urlObjects from which a corresponding target will be chosen randomly.
 * @property {String[]} regDomainList - Array of strings from which a corresponding regDomain will be chosen randomly.
 */

/**
 * An object containing URL data. May have additional properties.
 * 
 * @typedef {Object} urlObject
 * 
 * @property {String} name - Name of the service associated with the URL.
 * @property {String} url - URL under which the service can be accessed.
 */

let levelGeneratorCG = (function(){
    // global variable to store references to the used URL-manager
    let manager;
    // default list of existing tasks, labels and regDomains
    let defaultTasks = [
        // tutorial levels
        {
            'taskDescription' : "task-lvl-1-pre-0",
            'check' : {'extendedSyntax': 100, 'example1': 50, 'https': 50},
            'passingPoints' : 200,
        },
        {
            'taskDescription' : "task-lvl-1-pre-1",
            'check' : {'extendedSyntax': 100, 'domain_suffix': 50, 'https' : 50},
            'passingPoints' : 200,
        },
        {
            'taskDescription' : "task-lvl-1-pre-3",
            'check' : {'checkIPUrl': 150, 'https' : 50},
            'passingPoints' : 200,
        },
        /*{
            'taskDescription' : "task-lvl-1-pre-2",
            'check' : {'extendedSyntax': 200},
            'passingPoints' : 200,
        },*/

        // default levels
        {
            'taskDescription' : "task-lvl-3-pre-0",
            'check' : {'extendedSyntax': 50, 'targetInRegDomain': 100, 'sameTldAsTarget': 50},
            'passingPoints' : 200,
        },
        {
            'taskDescription' : "task-lvl-3-pre-1",
            'check' : {'extendedSyntax': 50, 'targetInRegDomain': 100, 'changedTld': 50},
            'passingPoints' : 200,
        }, 
        {
            'taskDescription' : "task-lvl-3-pre-2",
            'check' : {'extendedSyntax': 50, 'checktypoSquattedUrl': 150},
            'passingPoints' : 200,
        },
        {
            'check' : {'extendedSyntax': 50, 'targetInSubdomain': 100, 'domain': 50},
            'passingPoints' : 200, 
            'taskDescription' : "task-lvl-4-pre-0",
        },
        {
            'check' : {'extendedSyntax': 50, 'targetInSubdomainFull': 100, 'domain': 50},
            'passingPoints' : 200,
            'taskDescription' : "task-lvl-4-pre-1",
        },
        {
            'check' : {'extendedSyntax': 50, 'targetInPath': 100, 'domain': 50},
            'passingPoints' : 200,
            'taskDescription' : "task-lvl-7-pre-0",
        }, 
        {
            'check' : {'extendedSyntax': 50, 'targetInPath': 100, 'domain': 50},
            'passingPoints' : 200,
            'taskDescription' : "task-lvl-7-pre-1",
        }, 
        {
            'check' : {'extendedSyntax': 50, 'targetInRegDomain': 100, 'sameTldAsTarget': 50},
            'passingPoints' : 200,
            'taskDescription' : "task-lvl-9-pre-0",
        }, 
        {
            'check' : {'extendedSyntax': 50, 'targetInSubdomain': 100, 'domain': 50},
            'passingPoints' : 200,
            'taskDescription' : "task-lvl-9-pre-1",
        }, 
        {
            'check' : {'extendedSyntax': 50, 'targetInSubdomainFull': 100, 'domain': 50},
            'passingPoints' : 200,
            'taskDescription' : "task-lvl-9-pre-2",
        }, 
        {
            'check' : {'extendedSyntax': 50, 'targetInPath': 100, 'domain': 50},
            'passingPoints' : 200,
            'taskDescription' : "task-lvl-9-pre-3",
        }, 
    ];
    let existingLabels = ['baseLabels', 'lightLabels', 'extendedLabels', 'commonTlds', 'randomWords', 'secureWords', 'ipLabel'];
    let defaultRegDomains = [ "compromised.de", "hacked.com", "own-server.com", "random.de", "test.website", "hack.co.uk", "some-website.io" ];
    let levelSettings = {}; // global variable to store the settings   

    // fallback values if values are not specified, can be overwritten using overwriteDefaults
    let defaultValues = {
        'numberOfPresets' : 1,
        'labels' : ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
        'options' : {
            'creationModeEnabled' : true,   
        },
        'urlOptions' : {},
        'taskOptions' : {}
    }
    let overwriteDefaults = false; // global variable, stores if entries in levelSettings overwrite entries in defaultValues
    let newValues = {}; // stores values temporarily if overwriteDefaults is true; used in the end to overwrite defaultValues 
    let difficultyMap = [[0,1,2],[3,4,5,6,7,8,9,10,11,12,13]]  // stores the associated difficulties for each input (0 or 1 currently for 0 = tutorial levels 1-3 and 1 = game content)

    /**
     * Used to initialize the level generator.
     * 
     * @param {urlManager} urlManager  - The URL-Manager to be used.
     * @memberof module:levelGeneratorCG
     */ 
    function initGenerator(urlManager) {
        setUrlManager(urlManager);
    }

   /**
    * Used to set the URL manager.
    * 
    * @param {urlManager} urlManager - The URL-Manager to be used.
    * @memberof module:levelGeneratorCG
    *  
    */
    function setUrlManager(urlManager) {
        manager = urlManager;
    }

    /**
     * Resets defaultValues to its default value.
     * 
     * @memberof module:levelGeneratorCG
     */
    function resetDefaults(){
        defaultValues = {
            'numberOfPresets' : 1,
            'labels' : ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
            'options' : {
                'creationModeEnabled' : true,   
            },
            'urlOptions' : {},
            'taskOptions' : {}
        }
    }

    /**
     * Used to create a level for the Filtering Game.
     * 
     * @param {LevelSettings} settings - The settings for the level creation.
     * @returns {LevelTemplate} - The filled template for level creation.
     * @memberof module:levelGeneratorCG
     */
     function createLevel(settings){
        // default base level object
        let level = {
            'labels' : ['extendedLabels'],
            'check' : {'syntax': 100},
            'passingPoints' : 100,
            'domainsType' : 'preset',
            'targetsType' : 'preset',
            'taskDescription' : '',
            'presets' : [
    
            ],
        }
        // prevent levelSettings from being null / undefined
        levelSettings = (settings == null) ? {} : settings;

        // temporary
        if(manager == null) manager = urlSelector;

        // set overwrite defaults if it is contained
        if('overwriteDefaults' in levelSettings && levelSettings.overwriteDefaults === true){
            overwriteDefaults = true;
            newValues = JSON.parse(JSON.stringify(defaultValues));
        }

        // set the base properties : labels, numberOfPresets + creationModeEnabled
        let numberOfPresets = setProperty('numberOfPresets', "", utils.isIntOverMin, 0);

        // get the urlOptions and taskOptions, uses the default settings if the contained ones are empty
        let urlOptions = setProperty('urlOptions', "", utils.isNotEmpty);
        let taskOptions = setProperty('taskOptions', "", utils.isNotEmpty);
        
        if('inOrder' in levelSettings && levelSettings.inOrder == true){
            if('taskOptions' in levelSettings && levelSettings.taskOptions != null){
                if('customTasks' in taskOptions && taskOptions.customTasks != null && Array.isArray(taskOptions.customTasks)){
                    numberOfPresets = taskOptions.customTasks.length;
                }
                else if ('indexList' in taskOptions && taskOptions.indexList != null && Array.isArray(taskOptions.indexList)) {
                    numberOfPresets = taskOptions.indexList.length;
                }   
            }
        }

        let labels = ['baseLabels', 'randomWords', 'commonTlds', 'secureWords']
        if('labels' in settings && Array.isArray(settings['labels'][0])){
            labels = []
            for (let i = 0; i < numberOfPresets; i++){
                if(utils.containsOnlyValuesInDomain(settings['labels'][i], existingLabels)){
                    labels.push(settings['labels'][i])
                } 
                else {
                    labels.push(['baseLabels', 'randomWords', 'commonTlds', 'secureWords'])
                }
            }
        }
        else {
            labels = setProperty('labels', "", utils.containsOnlyValuesInDomain, existingLabels);
        }

        // generates levels up to the number of presets
        for(var i = 0; i < numberOfPresets; i++){
            // generate the URLs
            let task = generateTask(taskOptions, i);
            let target = generateTarget(urlOptions, task);
            let regDomain = generateRegDomain(urlOptions, task);
            // generate the tasks
            // adds the preset to the level
            if(Array.isArray(labels)){
                if(Array.isArray(labels[0])){
                    level.presets[i] = generatePreset(target, regDomain, task, labels[i]);
                }
                else {
                    level.presets[i] = generatePreset(target, regDomain, task, labels);
                }
            }
        }
        // Set the creation mode
        setCreationMode(level);

        // Overwrite the values if overwriteDefaults was true
        if(overwriteDefaults){
            defaultValues = newValues;
            overwriteDefaults = false;
        }

        // return the generated level
        return level;
    }

    /**
     * Enables or disables the creation mode based on the input and the default values.
     * 
     * @param {LevelTemplate} level - The level template in which the property is changed.
     * @memberof module:levelGeneratorCG
     */
    function setCreationMode(level){
        // Get if the creation mode is supposed to be enabled        
        let creationModeEnabled = defaultValues.options.creationModeEnabled;
        if('options' in levelSettings && levelSettings.options != null){
            creationModeEnabled = setProperty('creationModeEnabled', 'options', utils.isBool);
        }
        // Set the creationMode values for the base level        
        level.creationMode = creationModeEnabled;
        level.config = {
            'creationMode' : creationModeEnabled
        }
    }

    /**
     * Function, returns the value of the property at the given path from the level settings or, if not contained, the default values. 
     * 
     * @param {String} property - The property name as string.
     * @param {String} path - String in dot notation that denotes the route to the parent of the property, e.g. "example.value" to access levelSettings.example.value
     * @param {Function} check - Function that returns true / false, has the property value as first input
     * @param  {...any} [checkArgs] - Additional parameters that are passed into the check function.
     * @returns {*} - The value of the property at the given path from the level settings or, if not contained, the default values
     * @memberof module:levelGeneratorCG
     */
    function setProperty(property, path, check, ...checkArgs){
        let settings = utils.getByPath(levelSettings, path);
        let defaultValue = utils.getByPath(defaultValues, path);
        let newVal = utils.getByPath(newValues, path);
        if(property in settings && check(settings[property], ...checkArgs)){
            if(overwriteDefaults){
                newVal[property] = settings[property];
            } 
            return settings[property];
        }
        return defaultValue[property];
    }

    /**
     * Function to initialize the values for a preset by creating a base preset and adding the url, task and label values.
     * 
     * @param {urlObject} target - The target used in the preset.
     * @param {String} domain - The domain used in the preset.
     * @param {Task} task - The task the preset is based on.
     * @param {String[]} labels - The labels to be used in the level preset. 
     * @memberof module:levelGeneratorCG
     */
    function generatePreset(target, domain, task, labels){
        let preset = {
            'domainsType' : 'preset',
            'targetsType' : 'preset',
            'addDomainsLabels' : true,
            'addTargetsLabels' : true,
            'labels' : labels,
            'targets' : [target],
            'domains' : [domain],
            'check' : task.check,
            'passingPoints' : task.passingPoints,
            'taskDescription' : task.taskDescription,
        };

        if(Array.isArray(domain)){
            preset.domains = domain;
        }
        if(Array.isArray(target)){
            preset.targets = target;
        }

        if(target == null){
            preset.targets = [];
        }
        if(domain == null){
            preset.domains = [];
        }

        return preset;
    }

   /**
    * Selects a task object for a preset. If customTask is contained in taskOptions, return it. If indexList is contained, return task at randomly selected index. Otherwise, return a random default task.
    * @summary Selects a task object for a preset based on levelSettings.taskOptions and the default tasks.
    * 
    * @return {Task} - The task to be used in the preset.
    * @memberof module:levelGeneratorCG 
    */
    function generateTask(taskOptions, presetIndex){
        let task = defaultTasks[utils.generateRandomIntBetween(0,defaultTasks.length - 1)];

        if(utils.isNotEmpty(taskOptions)){
            if('customTasks' in taskOptions && Array.isArray(taskOptions.customTasks) && taskOptions.customTasks.length > 0) {
                if(utils.containsOnlyCorrectElements(taskOptions.customTasks, isCorrectTaskObject) ) {
                    if('inOrder' in levelSettings && levelSettings.inOrder == true){
                        task = taskOptions.customTasks[presetIndex]; 
                    }
                    else {
                        task = taskOptions.customTasks[utils.generateRandomIntBetween(0, taskOptions.customTasks.length-1)];    
                    }
                }
            }
            else if('indexList' in taskOptions && Array.isArray(taskOptions.indexList) && taskOptions.indexList.length > 0)
            {
                if(utils.containsOnlyCorrectElements(taskOptions.indexList, utils.isIntBetweenInclusive, 0, defaultTasks.length-1)){
                    let index = taskOptions.indexList[utils.generateRandomIntBetween(0,taskOptions.indexList.length - 1)];
                    if('inOrder' in levelSettings && levelSettings.inOrder == true){
                        index = taskOptions.indexList[presetIndex]; 
                    }
                    task = defaultTasks[index];
                }
            }
            else if ('difficulty' in taskOptions && Number.isInteger(taskOptions.difficulty) && (taskOptions.difficulty == 1 || taskOptions.difficulty == 0)){
                let difficultyArray = difficultyMap[taskOptions.difficulty];
                let index = difficultyArray[utils.generateRandomIntBetween(0,difficultyArray.length - 1)];
                task = defaultTasks[index];
            }
        }

        return task;
    }

    /**
     * Tests if an object is a correct urlObject containing name and url strings.
     * @param {Object} testedObject - The object that is tested. 
     * 
     * @returns {Boolean} - True if all necessary properties of a {@link module:levelGeneratorCG~urlObject} are contained, false otherwise.
     * @memberof module:levelGeneratorCG 
     */
    function isCorrectUrlObject(testedObject) {
        return testedObject != null && (('name' in testedObject && utils.isString(testedObject.name)) 
            && ('url' in testedObject && utils.isString(testedObject.url)))
    }

    /**
     * Tests if an object is a correct taskObject containing taskDescription, passingPoints and checks of the right type.
     * @param {Object} testObject - The object that is tested. 
     * 
     * @returns {Boolean} - True if all necessary properties of a {@link module:levelGeneratorCG~Task} are contained, false otherwise.
     * @memberof module:levelGeneratorCG 
     */
    function isCorrectTaskObject(testedObject) {
        return testedObject != null 
            && ('taskDescription' in testedObject && utils.isString(testedObject.taskDescription)
            && 'passingPoints' in testedObject && utils.isIntOverMin(testedObject.passingPoints, 0)
            && 'check' in testedObject && utils.isNotEmpty(testedObject.check));
    }

    /**
     * Selects a regDomain for a preset. If regDomain is contained in urlOptions, return it. If regDomainList is contained, return a regDomain at a randomly selected index. Otherwise, return a random default regDomain.
     * @summary Selects a regDomain for a preset based on levelSettings.urlOptions and the default regDomain.
     * 
     * @returns {String} - The selected regdomain.
     * @memberof module:levelGeneratorCG
     */
    function generateRegDomain(urlOptions, task){
        let regDomain = defaultRegDomains[utils.generateRandomIntBetween(0,defaultRegDomains.length-1)];

        if(utils.isNotEmpty(urlOptions)){
            // check if regDomainList exists
            if('regDomainList' in urlOptions && Array.isArray(urlOptions.regDomainList) && urlOptions.regDomainList.length > 0) {
                if(utils.containsOnlyCorrectElements(urlOptions.regDomainList, utils.isString)){
                    regDomain = urlOptions.regDomainList[utils.generateRandomIntBetween(0, urlOptions.regDomainList.length-1)];   
                }
            }
        }

        if(task.taskDescription == 'task-lvl-1-pre-0'){
            //regDomain = ['example.de', 'example.com', 'example.co', 'example.uk', 'example.io', 'example.edu'];
            regDomain = null;
        }
        else if(task.taskDescription == 'task-lvl-1-pre-1'){
            regDomain = "com";
        }
        else if(task.taskDescription == 'task-lvl-1-pre-2'){
            regDomain = null;
        }

        return regDomain;
    }

    /**
     * Selects a target for a preset. If target is contained in urlOptions, return it. If targetList is contained, return a target at a randomly selected index. Otherwise, return a random target from the learner profile.
     * @summary Selects a regDomain for a preset based on levelSettings.urlOptions and the learner profile.
     * 
     * @returns {String} - The selected regdomain.
     * @memberof module:levelGeneratorCG
     */
    function generateTarget(urlOptions, task){        
        let target = manager.getUrl();

        if(utils.isNotEmpty(urlOptions)){
            // check if targetList exists
            if('targetList' in urlOptions && Array.isArray(urlOptions.targetList) && urlOptions.targetList.length > 0) {
                if(utils.containsOnlyCorrectElements(urlOptions.targetList, isCorrectUrlObject) ) {
                    target = urlOptions.targetList[utils.generateRandomIntBetween(0, urlOptions.targetList.length-1)];    
                }
            }
        }

        if(task.taskDescription == 'task-lvl-1-pre-0'){
            target = [{'url' : 'example'}];
        }
        else if(task.taskDescription == 'task-lvl-1-pre-1'){
            target = [];
        }
        else if(task.taskDescription == 'task-lvl-1-pre-2'){
            target = [];
        }

        return target;
    }

    return {
        createLevel : createLevel,
        setUrlManager : setUrlManager,
        resetDefaults : resetDefaults,
        initGenerator : initGenerator
    }
})();

if (typeof exports !== 'undefined') {
    module.exports = levelGeneratorCG;
}
