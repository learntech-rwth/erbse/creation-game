function learnerProfile() {
    this.browserServices = [];
    this.personalizedServices = [];
    this.defaultServices = [
        {
            "name": "Google.com",
            "url": "https://www.google.com/"
        },
        {
            "name": "Youtube.com",
            "url": "https://www.youtube.com/"
        },
        {
            "name": "Google.de",
            "url": "https://www.google.de/"
        },
        {
            "name": "Amazon.de",
            "url": "https://www.amazon.de/"
        },
        {
            "name": "Wikipedia",
            "url": "https://en.wikipedia.org/"
        },
        {
            "name": "eBay Kleinanzeigen",
            "url": "https://www.ebay-kleinanzeigen.de/"
        },
        {
            "name": "Facebook.com",
            "url": "https://www.facebook.com/"
        },
        {
            "name": "Twitch.tv",
            "url": "https://www.twitch.tv/"
        },
        {
            "name": "Gmx.net",
            "url": "https://www.gmx.net/"
        },
        {
            "name": "Web.net",
            "url": "https://web.de/"
        },
        {
            "name": "Vk.com",
            "url": "https://www.vk.com/"
        },
        {
            "name": "Yahoo.com",
            "url": "https://www.yahoo.com/"
        },
        {
            "name": "Netflix.com",
            "url": "https://www.netflix.com/"
        },
        {
            "name": "T-online.de",
            "url": "https://www.telekom.com/"
        },
        {
            "name": "Live.com",
            "url": "https://login.live.com/"
        },
        {
            "name": "Bild.de",
            "url": "https://secure.mypass.de/"
        },
        {
            "name": "Bild.de",
            "url": "https://bild.de/"
        },
        {
            "name": "Spiegel.de",
            "url": "https://www.spiegel.de/"
        },
        {
            "name": "Microsoft.com",
            "url": "https://www.microsoft.com/"
        },
        {
            "name": "Otto.de",
            "url": "https://www.otto.de/"
        },
        {
            "name": "Wetter.com",
            "url": "https://www.wetter.com/"
        },
        {
            "name": "Wetter.com",
            "url": "https://interaction.7pass.de/"
        },
        {
            "name": "Mobile.de",
            "url": "https://login.mobile.de/"
        },
        {
            "name": "Zdf.de",
            "url": "https://www.zdf.de/"
        },
        {
            "name": "Fandom.com",
            "url": "https://www.fandom.com/"
        },
        {
            "name": "Amazon.com",
            "url": "https://www.amazon.com/"
        },
        {
            "name": "Idealo.de",
            "url": "https://www.idealo.de/"
        },
        {
            "name": "Focus.de",
            "url": "https://www.focus.de/"
        },
        {
            "name": "Zeit.de",
            "url": "https://meine.zeit.de/"
        },
        {
            "name": "PayPal",
            "url": "https://www.paypal.com/"
        },
        {
            "name": "runescape 3979",
            "url": "https://secure.runescape.com/"
        },
        {
            "name": "steam",
            "url": "https://store.steampowered.com/"
        },
        {
            "name": "deutsche-bank.de",
            "url": "https://meine.deutsche-bank.de/"
        },
        {
            "name": "commerzbank.de",
            "url": "https://www.commerzbank.de/"
        },
        {
            "name": "dropbox.com",
            "url": "https://www.dropbox.com/"
        },
        {
            "name": "icloud.com",
            "url": "https://www.icloud.com/"
        },
        {
            "name": "onedrive",
            "url": "https://onedrive.live.com/"
        }
    ];
    this.probablyUnknown = [
        
    ];
    
    this.baseSets = [
        {"setName": "defaultServices", "entries": this.defaultServices}, 
        {"setName": "browserServices", "entries": this.browserServices},
        {"setName": "probablyUnknown", "entries": this.probablyUnknown}
    ]   

    this.personalizedEntries = [
        {"setName": "knownServices", "entries": [], "flags": ["selected", "used"]},
        {"setName": "unknownServices", "entries": [], "flags": ["rejected"]}
    ]
    
    this.generatePersonalizedUrls = () => {
        levelGeneratorCG.setUrlManager(urlSelector);
        addEntryToGamestate("personalizedServices", this.personalizedServices);
        for(entry of this.personalizedEntries){
            entry.entries = this.generatePersonalizedUrlsForFlag(entry.flags, entry.setName)
        }

        if(window.hasOwnProperty("_saver")){
            _saver.save();
        }
    }

    this.restoreLearnerProfile = () => {
        this.personalizedServices = getEntryFromGameState("personalizedServices");
        for(entry of this.personalizedEntries){
            entry.entries = getEntryFromGameState(entry.setName);
        }
    }

    function getEntryFromGameState(entryName){
        if(entryName in gameState){
            return gameState[entryName];
        }
    }

    this.restoreGameState = () => {
        if(!window.hasOwnProperty("gameState")){
            gameState = {}
        }

        addEntryToGamestate("personalizedServices", this.personalizedServices);
        for(entry of this.personalizedEntries){
            addEntryToGamestate(entry.setName, entry.entries);
        }
    }

    /**
     * Sets the personalized services to the passed services.
     * @param {service[]} services 
     */
    this.setServices = (services) => {
        this.personalizedServices = services;
        this.generatePersonalizedUrls();
    }    

    /**
     * Adds an entry to the list of personalized services.
     * @param {service} service 
     */
    this.addService = (service) => {
        this.personalizedServices.push(service);
        this.generatePersonalizedUrls();
    }

    /**
     * Resets the list of personalized services.
     */
    this.resetServices = () => {
        this.personalizedServices = [];
    }

    /**
     * Check if the learner profile contains personalized data. 
     * @returns {boolean} - True if personalized services were entered.
     */
    this.wasPersonalized = () => {
        return (this.personalizedServices != null && this.personalizedServices.length > 0);
    }

    /**
    * Randomly selects a service from the learner profile / the default values and returns it. 
    * @returns {service} - The selected service. 
    */
    this.getSingleService = () => {
        if(wasPersonalized()){
            return this.personalizedServices[randomIntBetween(0, this.personalizedServices.length-1)];
        }
        else {
            return this.defaultServices[randomIntBetween(0, this.defaultServices.length-1)];
        }
    }

    /**
     * Used to get the stored personalized services.
     * @returns {service[]} - The personalized services.
     */
    this.getPersonalizedServices = () => {
        return this.personalizedServices;
    }

    /**
     * Used to get personalized services with a flag set to true.
     * @param {string} flag - The flag which is checked.
     * @returns {service[]} - List of personalized services with the flag. 
     */
    this.getServicesWithFlag = (flag) => {
        let result = this.personalizedServices.filter(service => (service[flag] == true));
        return result;
    }

    /**
     * Used to get personalized services with a flag set to value.
     * @param {string} flag - The flag which is checked.
     * @param {*} value - The value the flag has to have.
     * @returns {service[]} - List of personalized services with the flag set to value.
     */
    this.getServicesWithFlagValue = (flag, value) => {
        let result = this.personalizedServices.filter(service => (service[flag] == value));
        return result;
    }

    function randomIntBetween(min, max){
        let res = Math.random() * (max-min);
        res += min;
        res = Math.round(res);
        return res;
    }

    function addEntryToGamestate(key, value){
        if(!window.hasOwnProperty("gameState")){
            gameState = {}
            gameState[key] = value;
        }
        else {
            gameState[key] = value;
        }
    }

    this.generatePersonalizedUrlsForFlag = (flags, setName) => {
        let filteredServices = []
        for(flag of flags){
            let flagServices = this.getServicesWithFlag(flag);
            if(filteredServices == []){
                filteredServices = flagServices;
            }
            else {
                for(service of flagServices){
                    if(!filteredServices.includes(service)){
                        filteredServices.push(service);
                    }
                }
            }
        }
        addEntryToGamestate(setName, filteredServices);
        return filteredServices;
    }
};
