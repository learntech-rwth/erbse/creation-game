function drawMainMenu(gameState){
  MTLG.setBackgroundImage("start_bg.jpg");

  // log in demo player if not yet present
  //if (false && MTLG.getPlayerNumber()) {
      //console.log("Logging in");
      //MTLG.lc.goToLogin(); //Leave Main Menu and go to login
  //}

  // get stage to add objects
  var stage = MTLG.getStage();
  var elementContainer = new createjs.Container();
  stage.addChild(elementContainer);

  /* let levelButton = MTLG.utils.uiElements.addButton({
      text: MTLG.lang.getString("start_game"),
      sizeX: MTLG.getOptions().width * 0.1,
      sizeY: 70,
    },
    function() {
      MTLG.lc.levelFinished({
        nextLevel: "level1"
      });
    }
  );
  levelButton.x = 1300;
  levelButton.y = 700;*/

  const levelCallback = function() {

    gameState = _saver.load();
    _learnerProfile.restoreLearnerProfile();

    if(!MTLG.getOptions().pi_enabled || _learnerProfile.wasPersonalized()){
      _logger.log(`Player started creation game.`);
      
      gameState = { nextLevel: "level1" };
      _learnerProfile.restoreGameState();

      MTLG.lc.levelFinished(gameState);
      _learnerProfile.restoreGameState();
    }
    else {
      PersonalizationInterface.init(elementContainer, true);
    }
  }

  const loadLevelCallback = function() {
    _logger.log(`Player loaded gameState and continued creation game.`);
    gameState = _saver.load();
    _learnerProfile.restoreLearnerProfile();

    if(!MTLG.getOptions().pi_enabled || _learnerProfile.wasPersonalized()){
      console.log(gameState);
      MTLG.lc.levelFinished(gameState);
    }
    else {
      PersonalizationInterface.init(elementContainer, true);
    }
  }

  let startGameText = l('start_game')
  if(_saver.hasSavedGameState()){
    startGameText = l('start_new_game')
    let continueButton = new cbutton(1150, 780, l('continue_game'), loadLevelCallback, "red", elementContainer);
  }
  let levelButton = new cbutton(1150, 700, startGameText, levelCallback, "red", elementContainer);


  let title = createText("A Phisher's Bag of Tricks",{txtColor: MTLG.getOptions().title_txtColor,fontSize: MTLG.getOptions().title_fontSize});
  //title.text.shadow = new createjs.Shadow('#000000', 0, 0, 50);
  title.x = 1100;//(MTLG.getOptions().width-title.getBounds().width)/2+300;
  title.y = 500;

  new uuidView(_logger.getUUID());

  // Settings
  var settingsButton = new cbutton(50, 1000, l('settings_button'), function(){
    _logger.log("Opened settings.");
    settingsButton._view.visible = false; 
    gameSettings(settingsButton._view);
  }, "red", elementContainer);

  // add objects to stage
  //stage.addChild(levelButton);
  elementContainer.addChild(title);
}
