function startCountdown (time = null){
  this.countdownTime = _globalController.countdownTime
  if (time !== null) this.countdownTime = time;  
  _globalController.countdown = setInterval(function() {

    // Find the distance between now and the count down date
    this.countdownTime -= 1;
    countdownView(this.countdownTime, 120, 140, _globalController.stage);

    // If the count down is finished, write some text
    if (this.countdownTime <= 0) {
      clearInterval(_globalController.countdown);
      expirationView(MTLG.getOptions().width/2-400, MTLG.getOptions().height/2-300, 800, 600, _globalController.stage);
    }
  }, 1000);
}
