
// global variable for shortening language calls
var l = MTLG.lang.getString;

// Handle the language switching
function langHandler(){

  var languageContainer = new createjs.Container();

  // language flags
  var germanFlag = MTLG.assets.getBitmap("img/german_flag.png");
  germanFlag.x = 0;
  germanFlag.y = 0;
  germanFlag.scale = .075;

  var ukFlag = MTLG.assets.getBitmap("img/uk_flag.jpg");
  ukFlag.x = 200;
  ukFlag.y = 0;
  ukFlag.scale = .075;

  // get actual language
  if(MTLG.lang.getLanguage() == "de"){
    germanFlag.alpha = 1;
    ukFlag.alpha = 0.4;
  } else if(MTLG.lang.getLanguage() == "en"){
      ukFlag.alpha = 1;
      germanFlag.alpha = 0.4;
  }

  // add listeners
  germanFlag.addEventListener("pressup", function(){
    MTLG.lang.setLanguage("de");
    _logger.log("Set language to german, restarting game.");
    console.log(tutSections1);
    // restart menu
    if (_globalController){
      let tmpGameState = _globalController.gameState;
      tmpGameState['nextLevel'] = "selection";
      MTLG.lc.levelFinished(tmpGameState);
    } else {
      MTLG.lc.goToMenu();
    }
  });

  ukFlag.addEventListener("pressup", function(){
    MTLG.lang.setLanguage("en");
    _logger.log("Set language to english, restarting game.");
    // restart menu
    if (_globalController){
      let tmpGameState = _globalController.gameState;
      tmpGameState['nextLevel'] = "selection";
      MTLG.lc.levelFinished(tmpGameState);
    } else {
      MTLG.lc.goToMenu();
    }
  });

  languageContainer.addChild(germanFlag, ukFlag)

  return languageContainer;
}
