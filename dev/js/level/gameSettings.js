function gameSettings(settingsButton){

  this.settingsButton = settingsButton;

  // general stuff - container, headline, done button
  var generalContainer = createBox(210, 130, (MTLG.getOptions().devMode)?1500:800, (MTLG.getOptions().devMode)?820:540, {bgColor: MTLG.getOptions().textboxTutorial2_bgColor});
  this.gC = generalContainer;
  var settingsHeadline = createText(l('settings_headline'), {txtColor: MTLG.getOptions().textbox_txtColor, lineWidth: 700});
  settingsHeadline.x = 20;
  settingsHeadline.y = 20;
  var doneButton = new cbutton(20 + MTLG.getOptions().text_padding.lr, (MTLG.getOptions().devMode)?750:440, l('settings_done'), function() {
    this.gC.visible = false;
    this.settingsButton.visible = true;
    _logger.log("Closed settings.");
  }.bind(this), "red", generalContainer);

  function changeSetting(buttonActivated, buttonsDeactivated, newSettings = null){
    MTLG.loadOptions(newSettings);
    buttonActivated._view.alpha = 1;
    for(let btn of buttonsDeactivated){
      btn._view.alpha = 0.5;
    }
  }

  generalContainer.addChild(settingsHeadline);
  MTLG.getStageContainer().addChild(generalContainer);

  // lang handling
  var langHeadline = createText(l('lang_headline'), {txtColor: MTLG.getOptions().textbox_txtColor, lineWidth: 850});
  langHeadline.x = 20;
  langHeadline.y = 100;
  var langhandling = langHandler();
  langhandling.x = 20 + MTLG.getOptions().text_padding.lr;
  langhandling.y = 150;

  generalContainer.addChild(langHeadline, langhandling);

  if(MTLG.getOptions().pi_showPersonalizationOptions){
    // Personalization
    var personalizationHeadline = createText(l('pi_headline'), {txtColor: MTLG.getOptions().textbox_txtColor, lineWidth: 700});
    personalizationHeadline.x = 20;
    personalizationHeadline.y = 300;
    var piOnButton = new cbutton(20 + MTLG.getOptions().text_padding.lr, 350, l('pi_enabled'), function(){changeSetting(piOnButton, [piOffButton], {"pi_enabled": true});}, "red", generalContainer);
    var piOffButton = new cbutton(250, 350, l('pi_disabled'), function(){changeSetting(piOffButton, [piOnButton], {"pi_enabled": false});}, "red", generalContainer);

    // initial alpha
    if(MTLG.getOptions().pi_enabled === true){
      changeSetting(piOnButton, [piOffButton]);
    } else{
      changeSetting(piOffButton, [piOnButton]);
    }
    
    generalContainer.addChild(personalizationHeadline);
  }
  
  if(MTLG.getOptions().devMode){
    // URL movement - click, drag or click + drag
    var movementHeadline = createText(l('movement_headline'), {txtColor: MTLG.getOptions().textbox_txtColor, lineWidth: 700});
    movementHeadline.x = 20;
    movementHeadline.y = 450;
    var clickButton = new cbutton(20 + MTLG.getOptions().text_padding.lr, 500, l('movement_click'), function(){changeSetting(clickButton, [dragButton, clickdragButton], {"movement": "click"});}, "red", generalContainer);
    var dragButton = new cbutton(225, 500, l('movement_drag'), function(){changeSetting(dragButton, [clickButton, clickdragButton], {"movement": "drag"});}, "red", generalContainer);
    var clickdragButton = new cbutton(405, 500, l('movement_clickdrag'), function(){changeSetting(clickdragButton, [dragButton, clickButton], {"movement": "clickdrag"});}, "red", generalContainer);

    // initial alpha
    if(MTLG.getOptions().movement === "click"){
      changeSettings(clickButton, [dragButton, clickdragButton]);
    } else if(MTLG.getOptions().movement === "drag"){
      changeSetting(dragButton, [clickButton, clickdragButton]);
    } else{
      changeSetting(clickdragButton, [dragButton, clickButton]);
    }
    
    generalContainer.addChild(movementHeadline);
    
    // cheat mode
    var cheatHeadline = createText(l('cheat_headline'), {txtColor: MTLG.getOptions().textbox_txtColor, lineWidth: 700});
    cheatHeadline.x = 20;
    cheatHeadline.y = 600;
    var cheatOnButton = new cbutton(20 + MTLG.getOptions().text_padding.lr, 650, l('cheat_on'), function(){changeSetting(cheatOnButton, [cheatOffButton], {"cheatMode": true});}, "red", generalContainer);
    var cheatOffButton = new cbutton(250, 650, l('cheat_off'), function(){changeSetting(cheatOffButton, [cheatOnButton], {"cheatMode": false});}, "red", generalContainer);

    // initial alpha
    if(MTLG.getOptions().cheatMode === true){
      changeSetting(cheatOnButton, [cheatOffButton]);
    } else{
      changeSetting(cheatOffButton, [cheatOnButton]);
    }
    
    generalContainer.addChild(cheatHeadline);
    
    // countdown
    var countdownHeadline = createText(l('countdown_headline'), {txtColor: MTLG.getOptions().textbox_txtColor, lineWidth: 700});
    countdownHeadline.x = 20;
    countdownHeadline.y = 750;
    var countdownOnButton = new cbutton(20 + MTLG.getOptions().text_padding.lr, 800, l('countdown_on'), function(){changeSetting(countdownOnButton, [countdownOffButton], {"useCountdown": true});}, "red", generalContainer);
    var countdownOffButton = new cbutton(350, 800, l('countdown_off'), function(){changeSetting(countdownOffButton, [countdownOnButton], {"useCountdown": false});}, "red", generalContainer);

    // initial alpha
    if(MTLG.getOptions().useCountdown === true){
      changeSetting(countdownOnButton , [countdownOffButton]);
    } else{
      changeSetting(countdownOffButton, [countdownOnButton]);
    }

    generalContainer.addChild(countdownHeadline);
  }
}
