let urlUtils = (function(){

  /**
   * Dummy function to replace existing calls
   */
  this.getUrl = function(pUrl){
    return pUrl;
  }
  /**
   * Return the current URL as parsed by the URL class and constructor.
   * This is not necessarily equal to the URL given by concatenating
   * the urlParts.
   */
  this.getUrlParsed = function(pUrl){
    if(!this.testUrl(pUrl)){
      return "";
    }
    let currentUrlString = pUrl
    let currentUrl = new URL(currentUrlString);
    return currentUrl.href;
  }

  /**
   * Syntax check for URLs.
   * Checks for correct URL syntax and if protocol (scheme) is https or http
   * Note that the URL parser is very lenient, and transforms some errors automatically.
   * For example https:///www.example.com///test is a valid URL.
   * As is https:www.example.com
   */
  this.testUrl = function(pUrl){
    let currentUrlString = pUrl;
    let currentHostname = "";
    let currentUrl = null;
    // First check if syntax is correct
    // by calling URL constructor
    try{
      //currentUrl = url.parse(currentUrlString);
      currentUrl = new URL(currentUrlString);
    }catch (e){
      // console.log(e) //TODO: DEBUG
      return false;
    }
    // Next, check if scheme is http or https
    if(!["http:", "https:"].includes(currentUrl.protocol.toLocaleLowerCase())){
      return false;
    }
    // Next, check if // follows after protocol (bug: URL accepts strings like 'http:/www.example.de')
    if (!(pUrl.startsWith(currentUrl.protocol+"//"))){
      return false;
    }
    // Check if host is not empty
    if(!currentUrl.hostname){
      return false;
    }

    return true;

  }

  /**
   * Extended Syntax check of URL. Checks if parsed URL
   * is equal to given URL.
   * TODO: Check if reg domain is empty?
   * Exception: Given URL does not have to end in a slash if path is empty.
   */
  this.extendedSyntaxUrl = function(pUrl){
    if(!this.testUrl(pUrl)){
      return false;
    }
    // Uppercase schemes should be interpreted as lowercase
    let schemeReg = /^[a-zA-Z]*:\/\//;
    let scheme = schemeReg.exec(pUrl)[0]; // Have to find something, since testUrl passed
    pUrl = pUrl.replace(scheme, scheme.toLocaleLowerCase());

    // We need to find the exact hostname (not parsed) to convert to lower case
    let noSchemeUrl = pUrl.replace(schemeReg, "");
    let hostReg = /^[A-Za-z0-9._-]*/;
    let pHostname = hostReg.exec(noSchemeUrl);
    if(pHostname && pHostname[0]){
      pHostname = pHostname[0];
    } else{
      return false;
    }
    //let pHostname = this.getUrlHostname(pUrl);
    if(pHostname.toLocaleLowerCase() !== pHostname){
      pUrl = pUrl.replace(pHostname, pHostname.toLocaleLowerCase());
    }
    let parsedUrl = this.getUrlParsed(pUrl);
    if(pUrl === parsedUrl){
      return true;
    }
    if(parsedUrl.endsWith("/") && pUrl === parsedUrl.substring(0, parsedUrl.length -1)){
      return true;
    }
    return false;
  }

  /**
   * Returns the hostname (host without port) of the
   * current URL.
   */
  this.getUrlHostname = function(pUrl){
    if(!this.testUrl(pUrl)){
      return "";
    }
    let currentUrlString = pUrl;
    let currentUrl = new URL(currentUrlString);
    return currentUrl.hostname;
  }

  /**
   * Returns the path of the current URL.
   * Path does not include query or fractions.
   */
  this.getUrlPathname = function(pUrl){
    if(!this.testUrl(pUrl)){
      return "";
    }
    let currentUrlString = this.getUrl(pUrl);
    let currentUrl = new URL(currentUrlString);
    return currentUrl.pathname;
  }

  /**
   * Returns the query part of the URL.
   */
  this.getUrlQuery = function(pUrl){
    if(!this.testUrl(pUrl)){
      return "";
    }
    let currentUrlString = this.getUrl(pUrl);
    let currentUrl = new URL(currentUrlString);
    return currentUrl.search;
  }

  /**
   * Returns the scheme/protocol part of the URL.
   */
  this.getUrlScheme = function(pUrl){
    if(!this.testUrl(pUrl)){
      return "";
    }
    let currentUrlString = this.getUrl(pUrl);
    let currentUrl = new URL(currentUrlString);
    return currentUrl.protocol;
  }


  /**
   * Returns the registrable domain of the current URL.
   * The registrable domain consists of the publix suffix (eTLD),
   * as well as the first domain label in front of the suffix.
   * Currently performs some additional checks:
   *   - Hostname starts with '.'
   *   - Unknown TLD
   *   - Hostname equals eTLD
   *   - hostname ends in eTLD but character before eTLD is not '.' (TODO)
   */
  this.getRegistrableDomain = function(pUrl){
    let currentHostname = this.getUrlHostname(pUrl);
    if(currentHostname[0] == '.'){
      return ""; //TODO
    }
    let candidates = [];
    for(prefix of pubPrefixList){
      if(currentHostname.endsWith(prefix)){
        candidates.push(prefix);
      }
    }
    if(candidates.length == 0){
      return "";
    }
    let longestMatch = "";
    for(candidate of candidates){
      if(candidate.length > longestMatch.length){
        // The candidate is a longer prefix, check if it actually is the eTLD
        // First check if the hostname is not empty
        if(currentHostname == candidate){
          return ""; // TODO
        }
        // Next, check if there is a "." in front of it.
        if(currentHostname[currentHostname.length - candidate.length - 1] == "."){
          longestMatch = candidate;
        } //TODO: other cases? Are there valid cases for this?
      }
    }
    let pattString = "[a-zA-Z0-9-]*\\." + longestMatch + "$";
    let patt = new RegExp(pattString,"i");
    let res = patt.exec(currentHostname);
    return res?res[0]:""; // Return found match or "" if none were found
  }

  this.getETld = function(pUrl){
    let currentRD = this.getRegistrableDomain(pUrl);
    let eTLD = currentRD.split(".");
    eTLD = eTLD.splice(1);
    eTLD = eTLD.join(".");
    return eTLD;
  }

  /**
   * Returns the subdomains of the current URL as string.
   * Subdomains in this context mean all domains below the registrable domain.
   * This is achieved by simply returning everything in front of the registrable domain
   * (which includes dots).
   */
  this.getSubdomains = function(pUrl){
    let currentHostname = this.getUrlHostname(pUrl);
    let currentRegName = this.getRegistrableDomain(pUrl);
    // Return empty string if not a valid URL
    if(currentRegName.length == 0){
      return "";
    }
    return currentHostname.substring(0, currentHostname.length - currentRegName.length);
  }
  return {
    getUrl: getUrl,
    getUrlParsed: getUrlParsed,
    testUrl: testUrl,
    extendedSyntaxUrl: extendedSyntaxUrl,
    getUrlHostname: getUrlHostname,
    getUrlPathname: getUrlPathname,
    getUrlQuery: getUrlQuery,
    getUrlScheme: getUrlScheme,
    getRegistrableDomain: getRegistrableDomain,
    getETld: getETld,
    getSubdomains : getSubdomains
  }
})()

/**
 * Creates an object to calculate the points of a given objective and URL.
 */
function objectiveChecker(objective){

  this.objective = objective;

  this.check = function(pUrl){
    let totalPoints = 0;
    let ret = {'passed' : false,
      'points' : 0,
      'tests' : [],
    }
    for(currentTest in this.objective['check']){
      // There are a lot of test functions, the selection is outsourced
      let testFun = this.getTestFun(currentTest);
      let testResult = (testFun.bind(this))(pUrl);

      let currentPoints = this.objective['check'][currentTest];
      let passResult = testResult['passed'];
      let isReq = true; // TODO
      let currentFeedback = testResult['feedback'];

      if(testResult['passed']){
        totalPoints += currentPoints;
      }else{
        currentPoints = 0;
      }

      ret['tests'].push({'name' : currentTest,
        'passed' : passResult,
        'points' : currentPoints,
        'required' : isReq,
        'desc' : currentFeedback,
      });

      ret['points'] = totalPoints;
      ret['passed'] = totalPoints >= objective['passingPoints'];

      // check if 'extendedSyntax' failed; if so return just this as feedback.
      if (currentTest == 'extendedSyntax' && !testResult['passed']){
        ret['tests'] = [{'name' : currentTest,
          'passed' : passResult,
          'points' : currentPoints,
          'required' : isReq,
          'desc' : currentFeedback,
        }];
        return ret;
      }
    }
    return ret;
  }

  this.getTestFun = function(currentTest){
    switch(currentTest){
      case 'domain':
        return this.checkRegDomainInDomains;
      case 'domain_suffix':
        return this.checkRegDomainEndsInDomain;
      case 'target':
        return this.checkUrlContainsTargets;
      case 'syntax':
        return this.checkSyntax;
      case 'extendedSyntax':
        return this.checkExtendedSyntax;
      case 'https':
        return this.checkSchemeIsHttps;
      case 'targetInRegDomain':
        return this.checkTargetInRegDomain;
      case 'targetInSubdomain':
        return this.checkTargetInSubdomain;
      case 'targetInSubdomainFull':
        return this.checkTargetInSubdomainFull;
      case 'targetInPath':
        return this.checkTargetInPath;
      case 'sameTldAsTarget':
        return this.checkSameTldAsTarget;
      case 'changedTld':
        return this.checkChangedTld;
      case 'example1':
        return this.checkExample1;
      case 'checktypoSquattedUrl':
        return this.checkTypoSquattedUrl;
      case 'checkIPUrl':
        return this.checkIPUrl;
    }
  }
  /**
   * Checks if the serialized URL in the URL bar contains one ore more of
   * the targets defined in the objective.
   * Returns 0 if there are no targets in the URL
   * Returns 1 if there is exactly one target in the URL
   * Returns 2 if there are more than one targets in the URL
   */
  this.checkUrlContainsTargets = function(pUrl){
    let foundTarget = null;
    for(target of this.objective['targets']){
      if(urlUtils.getUrlParsed(pUrl).toLocaleLowerCase().includes(target.toLocaleLowerCase())){
        if(foundTarget){ // if foundTarget is set we already found a target!
          return {'passed' : true, // TODO
            'feedback' : l('fb-check-target-more-true'),
          }
        }
        foundTarget = target;
      }
    }
    if(foundTarget){ // We did not return in the loop, so we found exactly one target
      return {'passed' : true,
        'feedback' : l('fb-check-target-contain-true'),
      }
    }else{
      return {'passed' : false,
        'feedback' : l('fb-check-target-contain-false'),
      }
    }
  }

  this.checkRegDomainInDomains = function(pUrl){
    if(this.objective['domains'].includes(urlUtils.getRegistrableDomain(pUrl))){
      return {'passed' : true,
        'feedback' : l('fb-check-rd-incorrect-true'),
      }
    }
      return {'passed' : false,
        'feedback' : l('fb-check-rd-incorrect-false'),
      }
  }

  this.checkRegDomainEndsInDomain= function(pUrl){
    let regDomain = urlUtils.getRegistrableDomain(pUrl);
    for(domain of this.objective['domains']){
      if(regDomain.endsWith(domain)){
        return {'passed' : true,
          'feedback' : l('fb-check-rd-end-true'),
        }
      }
    }
    return {'passed' : false,
      'feedback' : l('fb-check-rd-end-false'),
    }
  }

  this.checkSchemeIsHttps = function(pUrl){
    let scheme = urlUtils.getUrlScheme(pUrl);
    if(scheme == "https:"){
      return{'passed' : true,
        'feedback' : l('fb-check-scheme-true'),
      }
    }else{
      return{'passed' : false,
        'feedback' : l('fb-check-scheme-false'),
      }
    }
  }

  this.replaceContainedTld = function(pUrl){
    let eTld = '.' + urlUtils.getETld(pUrl.toLocaleLowerCase());
    let tmpName = urlUtils.getRegistrableDomain(pUrl).toLocaleLowerCase();
    return tmpName.substring(0, tmpName.length - eTld.length);
  }

  this.checkTargetInRegDomain = function(pUrl){
    let regDomain = urlUtils.getRegistrableDomain(pUrl);
    for(target of this.objective['targets']){
      let targetReg = urlUtils.getRegistrableDomain(target['url']);
      if(target['name'].toLocaleLowerCase() == targetReg){
        // if(regDomain.includes(this.replaceContainedTld(target['url']).toLocaleLowerCase()) && this.replaceContainedTld(pUrl) != this.replaceContainedTld(target['url'])){
        if(regDomain.includes(this.replaceContainedTld(target['url']).toLocaleLowerCase())){
          console.log("regDomain includes targetName w/o tld and domain is not just name w/o tld");
          return{'passed' : true,
            'feedback' : l('fb-check-rd-target-true'),
          }
        }
        // else if(this.replaceContainedTld(pUrl) != this.replaceContainedTld(target['url'])){
        //   return{'passed' : false,
        //     'feedback' : l('fb-check-rd-belong-false'),
        //   }
        // }
      }
      if(regDomain == targetReg){
        return{'passed' : false,
          'feedback' : l('fb-check-rd-belong-false'),
        }
      }
      if(regDomain.includes(target['name'].toLocaleLowerCase())){
        return{'passed' : true,
          'feedback' : l('fb-check-rd-target-true'),
        }
      }
    }
    return{'passed' : false,
      'feedback' : l('fb-check-rd-target-false'),
    }
  }

  this.checkTargetInSubdomain = function(pUrl){
    let currentSubdomains = urlUtils.getSubdomains(pUrl);
    for(target of this.objective['targets']){
      if(currentSubdomains.includes(target['name'].toLocaleLowerCase())){
        return{'passed' : true,
          'feedback' : l('fb-check-subdomain-target-true'),
        }
      }
    }
    return{'passed' : false,
      'feedback' : l('fb-check-subdomain-target-false'),
    }
  }

  this.checkTargetInSubdomainFull = function(pUrl){
    let currentSubdomains = urlUtils.getSubdomains(pUrl);
    for(target of this.objective['targets']){
      let targetRD = urlUtils.getRegistrableDomain(target['url']);
      if(currentSubdomains.includes(targetRD)){
        return{'passed' : true,
          'feedback' : l('fb-check-subdomain-full-true'),
        }
      }
    }
    return{'passed' : false,
      'feedback' : l('fb-check-subdomain-full-false'),
    }
  }

  this.checkTargetInPath = function(pUrl){
    // Transform to lowercase, as path is case sensitive
    let currentPath = urlUtils.getUrlPathname(pUrl).toLocaleLowerCase();
    for(target of this.objective['targets']){
      let targetName = target['name'].toLocaleLowerCase();
      if(currentPath.includes(targetName)){
        return{'passed' : true,
          'feedback' : l('fb-check-target-path-true'),
        }
      }
    }
    return{'passed' : false,
      'feedback' : l('fb-check-target-path-false'),
    }
  }

  /**
   * Checks if pUrl has a common TLD with at least one target
   */
  this.checkSameTldAsTarget = function(pUrl){
    let targets = this.objective['targets'];
    for(target of targets){
      if(urlUtils.getETld(pUrl).endsWith(urlUtils.getETld(target['url']))){
        return{'passed' : true,
          'feedback' : l('fb-check-tld-target-true'),
        }
      }
    }
    return{'passed' : false,
      'feedback' : l('fb-check-tld-target-false'),
    }
  }

  /**
   * Checks if pUrl was created by changing the TLD of a target
   */
  this.checkChangedTld = function(pUrl){
    let regDomain = urlUtils.getRegistrableDomain(pUrl);
    let targets = this.objective['targets'];
    for(target of targets){
      let targetReg = urlUtils.getRegistrableDomain(target['url']);
      let name = target['name'].toLocaleLowerCase();;
      if(target['name'].toLocaleLowerCase() == targetReg){
        name = this.replaceContainedTld(pUrl);
      }
      if(regDomain.includes(name)){
        let targetUrl = target['url'];
        let targetRD = urlUtils.getRegistrableDomain(targetUrl);
        if(regDomain == targetRD){
          continue;
        }
        if(regDomain.split(".")[0] == targetRD.split(".")[0]){
          return{'passed' : true,
            'feedback' : l('fb-check-tld-true'),
          }
        }
      }
    }
    return{'passed' : false,
      'feedback' : l('fb-check-tld-false'),
    }
  }

  /**
   * Function to calculate the demerau levenshtein distance (levenshtein distance but swapping is a cost of 1)
   * Taken from: https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance
   * 
   * @param {string} a first string to compare
   * @param {string} b second string to compare
   * @returns the demerau levenshtein distance
   */
  this.damerauLevenshtein = function(a,b) {
    let d = [];
  
    if (a.length == 0 || b.length == 0) {
      return a.length + b.length;
    }
  
    for (let i = 0; i <= a.length; i++) {
      d[i] = [];
      d[i][0] = i;
    }
  
    for (let j = 0; j <= b.length; j++) {
      d[0][j] = j;
    }
  
    for (let i = 1; i <= a.length; i++) {
      for (let j = 1; j <= b.length; j++) {
        let cost = (a[i-1] == b[j-1]) ? 0:1;
  
        d[i][j] = Math.min(d[i-1][j] + 1, d[i][j-1] + 1, d[i-1][j-1] + cost);
        
        if (i > 1 && j > 1 && a[i-1]== b[j-2] && a[i-2] == b[j-1]) {
            d[i][j] = Math.min(d[i][j], d[i-2][j-2] + cost)
        }
      }
    }
  
    return d[a.length][b.length];
  }

  /**
   * Checks if pUrl was created by typo squatting the target url
   * 
   * @param {string} pUrl 
   */
  this.checkTypoSquattedUrl = function(pUrl) {
    let hostname = urlUtils.getUrlHostname(pUrl);
    for (let target of this.objective['targets']) {
      // get registrable domain because subdomain is not shown in the game!
      let targetHostname = urlUtils.getRegistrableDomain(target["url"]);

      let dist = this.damerauLevenshtein(hostname, targetHostname);

      if (dist == 1) {
        return {'passed': true, 
          'feedback': l('fb-check-typosquatting-true'),
        }
      }
    }

    return {'passed': false, 
      'feedback': l('fb-check-typosquatting-false'),
    }
  }

  /**
   * Checks if pUrl is a ip address instead of a domain
   * 
   * @param {string} pUrl 
   */
  this.checkIPUrl = function(pUrl) {
    let hostname = urlUtils.getUrlHostname(pUrl);
    let re = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/gm;

    // ip v4 url found
    if (re.test(hostname)) {
      return {'passed': true, 
        'feedback': l('fb-check-ip-true'),
      }
    }

    return {'passed': false, 
      'feedback': l('fb-check-ip-false'),
    }
  }

  this.checkExtendedSyntax = function(pUrl){
    if(urlUtils.extendedSyntaxUrl(pUrl)){
      return{'passed' : true,
        'feedback' : l('fb-check-syntax-true'),
      }
    }else{
      return{'passed' : false,
        'feedback' : l('fb-check-syntax-false'),
      }
    }
  }

  this.checkSyntax = function(pUrl){
    if(urlUtils.test(pUrl)){
      return{'passed' : true,
        'feedback' : l('fb-check-syntax-true'),
      }
    }else{
      return{'passed' : false,
        'feedback' : l('fb-check-syntax-false'),
      }
    }
  }

  this.checkExample1 = function(pUrl){
    let regDomain = urlUtils.getRegistrableDomain(pUrl);
    if(regDomain.startsWith("example.")){
      if(regDomain != "example.com"){
        return{'passed' : true,
          'feedback' : l('fb-check-exp-tld-true'),
        }
      }else{
        return{'passed' : false,
          'feedback' : l('fb-check-exp-tld-false'),
        }
      }
    }else{
      return{'passed' : false,
        'feedback' : l('fb-check-exp-rd'),
      }
    }
  }

}
