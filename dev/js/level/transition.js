function transition(gameState){
  // initialize level structure

  // Set background color to deep blue
  MTLG.setBackgroundColor(MTLG.getOptions().bgColor);

  this.gameState = gameState;
  this.stage = MTLG.getStageContainer();

  this._menuButtonsView = new menuButtons(this.gameState, this.stage);

  if(levels[gameState['level']]['config'] == "finish"){
    _logger.log("Showing the finish view, reached the end of game.");
    this._finishView = new finishView(this.gameState, this.stage);
    _logger.sendLogs();
  } else {
    let continueButton = button(500, 500, "Next Level",function(){
      MTLG.lc.levelFinished(gameState);
    },MTLG.getStageContainer());
    let transitionText = createText("Put your transition here!");
    transitionText.x = 500;
    transitionText.y = 300;
    MTLG.getStageContainer().addChild(transitionText);
  }
}
