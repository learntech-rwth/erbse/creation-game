function tutorial(gameState){
  // initialize level structure

  // Set background color to deep blue
  MTLG.setBackgroundColor(MTLG.getOptions().bgColor);

  this.stage = MTLG.getStageContainer();
  this.gameState = gameState;
  this.tutorialIndex = 1;

  this.lastShownHoverViews = [];
  this.explanationShownLastTick = false;

  //gameState['level'] = 4;

  switch(gameState['level']){
    case 0:
      this.sections = tutSections1;
      break;
    case 2:
      this.sections = tutSections2;
      this.tutorialIndex = 2;
      break;
    case 4:
      this.sections = tutSections3;
      this.tutorialIndex = 3;
      break;
    case 6:
      this.sections = tutSections4;
      this.tutorialIndex = 4;
      break;
    case 8:
      this.sections = tutSections5;
      this.tutorialIndex = 5;
      break;
    default:
      this.sections = tutSections1;
      console.log("Unexpected Tutorial! Showing first tutorial instead."); //TODO: DEBUG
      console.log(gameState);
  }
  _logger.log("Entered tutorial " + this.tutorialIndex);

  this.state = 0;
  this.maxTuts = this.sections.length - 1;
  let callback = function(){
    MTLG.getStageContainer().removeAllChildren();
    if(this.state >= this.maxTuts){
      return MTLG.lc.levelFinished(this.gameState);
    }else{
      this.state++;
      return tutorialLayout2(this, this.sections[this.state], callback, this.gameState, this.stage, urlPart);
    }
  }.bind(this);



  this.next = () => {
    this.stage.removeAllChildren();
    _logger.log("Continue button clicked in tutorial " + this.tutorialIndex +".");
    // remove possible listenes on tick
    removeListener();
    if (this.state >= this.maxTuts) {
      _logger.log("Tutorial " + this.tutorialIndex +" finished.");
      return MTLG.lc.levelFinished(this.gameState);
    } else {
      this.state++;
      _logger.log("Going to next tutorial section. State " + state + " reached in tutorial " + this.tutorialIndex +".");
      return tutorialLayout2(this, this.sections[this.state], callback, this.gameState, this.stage, urlPart);
    }
  }

  this.previous = () => {
    this.stage.removeAllChildren();
    // remove possible listenes on tick
    _logger.log("Previous clicked in tutorial " + this.tutorialIndex +".");
    removeListener();
    if (this.state <= 0) {
      _logger.log("Tutorial state index negative, going to selection.");
      this.gameState['nextLevel'] = "selection";
      return MTLG.lc.levelFinished(this.gameState);
    } else {
      this.state--;
      _logger.log("Going to previous tutorial section. State " + state + " reached in tutorial " + this.tutorialIndex +".");
      return tutorialLayout2(this, this.sections[this.state], callback, this.gameState, this.stage, urlPart);
    }
  }

  this.menu = () => {
    // remove possible listenes on tick
    removeListener();
  }

  // add array to save wrappers of eventlisteners on createjs.Ticker
  this.tickerArray = [];
  // function to empty ticker array
  function removeListener(){
    for(let wrapper of this.tickerArray){
      createjs.Ticker.off("tick", wrapper);
    }
    this.tickerArray = [];
  }
  // call tutorial view
  tutorialLayout2(this, this.sections[0], callback, this.gameState, this.stage, urlPart);
}

function tutorialLayout1(tutorial, section, callback, gameState, stage){
  this.tutorial = tutorial;
  this.gameState = gameState;
  this.callback = callback;
  this.stage = stage;

  text = section['text'];


  this._boxView = createBox(100, 150, 600, MTLG.getOptions().height-2*150, {bgColor:"#304b78"});
  this.stage.addChild(this._boxView);

  this._tutText = createText(text, {lineWidth: 600, txtColor: "#efefef", padding:{lr:20, tb:20}});
  this._tutText.x = 100;
  this._tutText.y = 150;
  this.stage.addChild(this._tutText);

  if (section['picture']) {
    let pic = MTLG.assets.getBitmapAbsoluteScale(section['picture'], .9, .9);
    pic.x = 850+(MTLG.getOptions().width-800-pic.getBounds().width)/2;
    pic.y = (MTLG.getOptions().height-pic.getBounds().height)/2;
    this.stage.addChild(pic);
  }

  this._menuButtons = new menuButtons(this.gameState, this.stage);
  this._continueButton = new button(MTLG.getOptions().width-220, MTLG.getOptions().height-100, "Continue", this.tutorial.next, this.stage);
  this._backButton = new button(50, MTLG.getOptions().height-100, "Back", this.tutorial.previous, this.stage);

}

function tutorialLayout2(tutorial, section, callback, gameState, stage, urlPart){
  this.tutorial = tutorial;
  this.gameState = gameState;
  this.callback = callback;
  this.stage = stage;

  // Section Checks
  var continueAllowed = false;
  //////////////////////////////////

  // navigation buttons
  this._menuButtons = new menuButtons(this.gameState, this.stage, this.tutorial.menu);
  this._backButton = new button(50, MTLG.getOptions().height-100, l("button_Back"), this.tutorial.previous, this.stage);
  this._continueButton = new button(MTLG.getOptions().width-220, MTLG.getOptions().height-100, l("button_Continue"), function(){/*dummy*/ } /*this.tutorial.next*/, this.stage);
  this._continueButton._view.removeAllEventListeners();
  this._continueButton._view.alpha = 0.5;

  function allowContinuation(){
    this._continueButton._view.addEventListener('pressup', this.tutorial.next);
    this._continueButton._view.alpha = 1;
  }

  // combine section informations and section mode explanation
  let combinedText;
  if(section['exp-text']){
    combinedText = l(section['tut-text']) + l(section['exp-text']);
  } else{
    combinedText = l(section['tut-text']);
  }
  //tutText = section['tut-text'];

  // check for presentationMode
  switch (section['presentationMode']){
    case 1:
      presentationModeOne();
      break;
    case 2:
      presentationModeTwo();
      break;
    case 3:
      presentationModeThree();
      break;
    default:
      console.log("No presentationMode passed. Please set presentationMode!");
  }

  // Mode One: Introductions, only text without any URL or grahpic
  function presentationModeOne(){
    // Section Checks - continue is directly allowed, because there is nothing to explore
    continueAllowed = true;
    allowContinuation();
    //////////////////////////////////
    
    let character = new characterView(this.stage);

    this._boxView = createBox(310, 50, 1300, MTLG.getOptions().height * 0.45, {bgColor: MTLG.getOptions().textboxTutorial2_bgColor});
    this.stage.addChild(this._boxView);

    this._tutText = createText(combinedText, {lineWidth: 1300, txtColor: MTLG.getOptions().textboxTutorial2_txtColor, padding:{lr:20, tb:20}});
    this._tutText.x = 310;
    this._tutText.y = 50;
    this.stage.addChild(this._tutText);
  }

  // Mode Two: URL with keys and explanation to show while hover
  function presentationModeTwo(){

    // Section Checks
    // direct allow, if there is no explanation to display
    if(section['url_explanation_on']){
      continueAllowed = false;
    } else{
      continueAllowed = true;
      allowContinuation();
    }
    //////////////////////////////////

    this._boxView = createBox(310, 50, 1300, MTLG.getOptions().height * 0.175, {bgColor:MTLG.getOptions().textboxTutorial2_bgColor});
    this.stage.addChild(this._boxView);

    this._tutText = createText(combinedText, {lineWidth: 1300, txtColor: MTLG.getOptions().textboxTutorial2_txtColor, padding:{lr:20, tb:20}});
    this._tutText.x = 310;
    this._tutText.y = 50;
    this.stage.addChild(this._tutText);

    let character = new characterView(this.stage);

    // check if there is an provided URL
    if(section['url']){

      // logic URL parts
      this._urlParts =  [];
      this._urlPartsView = [];

      // container for URL-block, URL-keys and explanation
      this._urlPartsViewContainer = new createjs.Container();
      this._urlPartsViewKeyContainer = new createjs.Container();
      this._urlPartsViewExplanationContainer = new createjs.Container();
      this._urlPartsViewExplanationContainer.visible = false;

      // save the length-sum of the URL-parts
      let urlLength = 0;

      // logic url parts
      for(let key in section['url']){
        let newUrlPart = new urlPart(section['url'][key]);
        newUrlPart.key = key;
        this._urlParts.push(newUrlPart);

        // check for tld and later eltd - new slash is assosiacted to path and no longer a single sign
        /*if(key == 'tld' || key == 'etld' || key == 'host'){
          let newUrlPartSlash = new urlPart("/");
          newUrlPartSlash.key = key  + '-slash';
          this._urlParts.push(newUrlPartSlash);
        }*/

        // Check for domain to add a dot
        if(key == 'domain' || key == 'subdomain'){
          let newUrlPartDot = new urlPart(".");
          newUrlPartDot.key = key + '-dot';
          this._urlParts.push(newUrlPartDot);
        }
      }

      // view of url parts
      for(let urlPart of this._urlParts){
        let newUrlPartView;
        newUrlPartViewTutorial = new urlPartViewTutorial(urlPart, urlPart.key, urlLength, 0);
        urlLength += newUrlPartViewTutorial.width;

        // push and add
        this._urlPartsView.push(newUrlPartViewTutorial);
        this._urlPartsViewContainer.addChild(newUrlPartViewTutorial._view);

      }

      // position all container after the URL-view generated
      var positionContainer = {x: (MTLG.getOptions().width - urlLength) / 2, y: 320};
      this._urlPartsViewContainer.x = positionContainer.x;
      this._urlPartsViewContainer.y = positionContainer.y;
      this._urlPartsViewKeyContainer.x = positionContainer.x;
      this._urlPartsViewKeyContainer.y = positionContainer.y - 80;
      this._urlPartsViewExplanationContainer.x = 0;
      this._urlPartsViewExplanationContainer.y = 0;


      // create the keys shown on hover
      if(section['url_key_view']){
        // first look for keys shownAbove more than one URL-part and how often for every single URL-Part
        var multipleShow = [];
        var singleShow = [];
        var keyCount = {};

        // get URL-parts displayed
        for(let urlPart of this._urlPartsView){
          keyCount[urlPart.key] = 0;
        }

        for(let urlPart of this._urlPartsView){
          // check for hover keys
          if(section['url_key_view'][urlPart.key]){
            urlPart.shownName = l(section['url_key_view'][urlPart.key]['shownName']);
            urlPart.shownOn = section['url_key_view'][urlPart.key]['shownOn'];
            urlPart.shownAbove = section['url_key_view'][urlPart.key]['shownAbove'];

            // sort ULR-Parts after key ist shown on multiple parts or not
            if(urlPart.shownAbove.length >= 2){
              multipleShow.push(urlPart);
            }
            else{
              singleShow.push(urlPart);
            }

            // sum up
            for(let key of urlPart.shownOn){ // old: shownAbove
              keyCount[key] += 1;
            }

          }
        }

        // check for elements more than one key is displayed above
        var maxShownOn = 0;
        var maxMultipleShow = 0;
        for(let key in keyCount){
          if(keyCount[key] > maxShownOn){
            maxShownOn = keyCount[key];
          }
        }
        for(let urlPart of multipleShow){
          if(urlPart.shownAbove.length > maxMultipleShow){
            maxMultipleShow = urlPart.shownAbove.length;
          }
        }

        // check for valid show
        var validConfig = false;
        if(maxShownOn <= 2 && maxMultipleShow <= 2){
          validConfig = true;
        }

        // switch for displaying mode of keys
        switch(validConfig){
          case true:
            keyView();
            break;
          default:
            console.log("Keine zugelassene Konfiguration."+
            "Zu einem URL-Part können maximal 2 Keys gleichzeitig angezeigt werden. Momentanes Maximum: " + maxShownOn + "\n" +
            "Maximal zulässige Anzahl shownAbove sind 2 Keys. Momentanes Maximum: " + maxMultipleShow);
        }

        // generate key view
        function keyView(){
          // keys with shownAbove > 2 go upper URL
          // generate upper keys first
          for(let urlPart of multipleShow){
            // compute position of label and line
            // find first second URL-part for multipleShow - distinct element after validConfig
            let secondPart = this._urlPartsView.find(part => urlPart.shownAbove.indexOf(part.key) != -1 && part.key != urlPart.key);

            let startPartX = 0;
            let distanceParts = 0;

            if(urlPart._view.x <= secondPart._view.x){
              startPartX = urlPart._view.x;
              distanceParts = (secondPart._view.x + secondPart.width) - urlPart._view.x;
            }else{
              startPartX = secondPart._view.x;
              distanceParts = (urlPart._view.x + urlPart.width) - secondPart._view.x;
            }

            let keyView = new urlPartViewKey(urlPart, startPartX, distanceParts, 'top');

            // add to container
            this._urlPartsViewKeyContainer.addChild(keyView._view);
          }


          // now single downside keys
          for(let urlPart of singleShow){
            let keyView = new urlPartViewKey(urlPart, urlPart._view.x, urlPart.width, 'bottom');

            // add to container
            this._urlPartsViewKeyContainer.addChild(keyView._view);
          }

        }


      }

      // Add explanation if one is provided
      if(section['url_explanation_on']){
        this._boxUrlText = createBox(310, 500/*220*/, 1300, MTLG.getOptions().height * 0.45, {bgColor:MTLG.getOptions().textbox_urlPart_explanation_bgColor});
        this._urlPartsViewExplanationContainer.addChild(this._boxUrlText);
        this._urlText = createText(l(section['url-text']), {lineWidth: 1300, txtColor: MTLG.getOptions().textbox_urlPart_explanation_txtColor, padding:{lr:20, tb:20}});
        this._urlText.x = 310;
        this._urlText.y = 500/*220*/;
        this._urlPartsViewExplanationContainer.addChild(this._urlText);

      }


      // Ticker for Hit with URL-parts
      if(this._urlPartsViewKeyContainer.children.length > 0){
        var tickerModeTwo = createjs.Ticker.on("tick", tickMouse.bind(this));
        this.tutorial.tickerArray.push(tickerModeTwo);
      }

      function tickMouse(event){
        var showExplanation = false;

        var shownExplanation = false;
        var shownTexts = [];

        // first reset visibility of all URL-parts and explanation
        for(let urlKeyView of this._urlPartsViewKeyContainer.children){
          urlKeyView.visible = false;
        }
        this._urlPartsViewExplanationContainer.visible = false;


        // check for hits
        // has to let, because of constructor with the same name
        for(let urlPartView of this._urlPartsView){
          let point = urlPartView._view.globalToLocal(MTLG.getStage().mouseX, MTLG.getStage().mouseY);

          if(urlPartView._view.hitTest(point.x, point.y) && urlPartView.shownOn){
            let associatedParts = this._urlPartsView.filter(part => urlPartView.shownOn.indexOf(part.key) != -1);

            // show the key
            for(let part of associatedParts){
              let associatedContainer = this._urlPartsViewKeyContainer.children.filter(keyView => keyView.key == part.key);
              for(let container of associatedContainer){
                container.visible = true;
                if(!this.lastShownHoverViews.includes(part.key)){
                  _logger.log("Hovering the " + part.key + " of the URL view.");
                }
                shownTexts.push(part.key);
              }
            }

            // show explanation if necessary
            if(section['url_explanation_on']){
              if(showExplanation == false){
                if(section['url_explanation_on'].indexOf(urlPartView.key) != -1){
                  this._urlPartsViewExplanationContainer.visible = true;
                  showExplanation = true;

                  shownExplanation = true;
                  if(this.explanationShownLastTick == false){
                    console.log(this.explanationShownLastTick);
                    _logger.log("Explanation shown.");
                  }

                  // Explanation has been shown - continue is now allowed
                  if(continueAllowed === false){
                    continueAllowed = true;
                    _logger.log("Continuing to the next level is allowed now.");
                    allowContinuation();
                  }
                }
              }
            }
          }
        }
        
        this.explanationShownLastTick = shownExplanation;
        if(shownTexts.length < this.lastShownHoverViews.length){
          for(var i = 0; i < this.lastShownHoverViews.length; i++){
            if(!shownTexts.includes(this.lastShownHoverViews[i])){
              _logger.log("Stopped hovering " + this.lastShownHoverViews[i] + ".");
            }
          }
        }
        if(shownTexts.length == 0 && this.lastShownHoverViews.length != 0){
          _logger.log("Stopped hovering.");
        }
        this.lastShownHoverViews = [...shownTexts];
      }

      // add container to stage
      this.stage.addChild(this._urlPartsViewContainer);
      this.stage.addChild(this._urlPartsViewKeyContainer);
      this.stage.addChild(this._urlPartsViewExplanationContainer);
    }
    else{
      console.log("No URL to display!");
    }
  }


  // Mode Three: Two URLs with an arrow inbetween
  function presentationModeThree(){

    // Section Checks
    continueAllowed = false;

    //////////////////////////////////

    this._boxView = createBox(310, 50, 1300, MTLG.getOptions().height * 0.15, {bgColor:MTLG.getOptions().textboxTutorial2_bgColor});
    this.stage.addChild(this._boxView);
    this._tutText = createText(combinedText, {lineWidth: 1300, txtColor: MTLG.getOptions().textboxTutorial2_txtColor, padding:{lr:20, tb:20}});
    this._tutText.x = 310;
    this._tutText.y = 50;
    this.stage.addChild(this._tutText);
    
    let character = new characterView(this.stage);
 
    // check if there is an provided URL
    if(section['url']){

      // logic URL parts
      this._urlParts =  [];
      this._urlPartsView = [];

      // container for URL-block, URL-keys and explanation
      this._urlPartsViewContainer = new createjs.Container();
      this._urlPartsViewExplanationContainer = new createjs.Container();
      // this._urlPartsViewExplanationContainer.alpha = 0;
      this._urlPartsViewExplanationContainer.visible = false;

      // add text of part which should replace the part in the first URL
      this._urlReplace = createText(section['replace_with'], {fontSize: "46px", lineWidth: 500, txtColor: "#efefef", padding:{lr:2, tb:6}});

      // save the length-sum of the URL-parts
      let urlLength = 0;

      // logic url parts
      for(let key in section['url']){
        let newUrlPart = new urlPart(section['url'][key]);
        newUrlPart.key = key;
        this._urlParts.push(newUrlPart);

        // check for tld and later eltd - new slash is assosiacted to path and no longer a single sign
        /*if(key == 'tld' || key == 'etld' || key == 'host'){
          let newUrlPartSlash = new urlPart("/");
          newUrlPartSlash.key = key  + '-slash';
          this._urlParts.push(newUrlPartSlash);
        }*/

        // Check for domain to add a dot
        if(key == 'domain' || key == 'subdomain'){
          let newUrlPartDot = new urlPart(".");
          newUrlPartDot.key = key + '-dot';
          this._urlParts.push(newUrlPartDot);
        }
      }

      // view of url parts
      for(let urlPart of this._urlParts){
        let newUrlPartView;
        newUrlPartViewTutorial = new urlPartViewTutorial(urlPart, urlPart.key, urlLength, 0);

        // new for tween
        if(urlPart.key == section['replace_url_part']){
          if(this._urlReplace.getBounds().width > newUrlPartViewTutorial.width){
            urlLength += this._urlReplace.getBounds().width - 10;
          }else{
            urlLength += newUrlPartViewTutorial.width;
          }
        }else{
          urlLength += newUrlPartViewTutorial.width;
        }

        // push and add
        this._urlPartsView.push(newUrlPartViewTutorial);
        this._urlPartsViewContainer.addChild(newUrlPartViewTutorial._view);

      }

      // position all container after the URL-view generated
      var positionContainer = {x: (MTLG.getOptions().width - urlLength) / 2, y: 320};
      this._urlPartsViewContainer.x = positionContainer.x;
      this._urlPartsViewContainer.y = positionContainer.y;
      this._urlPartsViewExplanationContainer.x = 0;
      this._urlPartsViewExplanationContainer.y = 0;

      // add explanation-text
      if(section['url-text']){
        this._boxUrlText = createBox(310, 530, 1300, MTLG.getOptions().height * 0.45, {bgColor:MTLG.getOptions().textbox_urlPart_explanation_bgColor});
        this._urlText = createText(l(section['url-text']), {lineWidth: 1300, txtColor: MTLG.getOptions().textbox_urlPart_explanation_txtColor, padding:{lr:20, tb:20}});
        this._urlText.x = 310;
        this._urlText.y = 530;
        this._urlPartsViewExplanationContainer.addChild(this._boxUrlText, this._urlText);
      }

      // which part should be replaced
      let replacedPart = this._urlParts.find(part => part.key == section['replace_url_part']);
      let replacedPartView = this._urlPartsView.find(part => part.key == section['replace_url_part']);

      // define position of _urlReplace after adding all url parts
      let swapPoint ={x: /*positionContainer.x*/ + replacedPartView._view.x, y:60}; //380};
      this._urlReplace.x = swapPoint.x;
      this._urlReplace.y = swapPoint.y;
      this._urlReplace.alpha = 0.4;
      //this.stage.addChild(this._urlReplace);
      this._urlPartsViewContainer.addChild(this._urlReplace);

      // add swap-button
      this._swapButton = new button(0, 450, l('button_Swap'), swapParts, this.stage);
      this._swapButton._view.x = (MTLG.getOptions().width - this._swapButton._view.getBounds().width) / 2;


      let originalLabel = replacedPartView.currentText();
      let replaceLabel = section['replace_with'];

      // swap flag
      let swapped = false;

      // callback on swap-button
      function swapParts(replaced, swap){
        // Explanation has been shown - continue is now allowed
        if(continueAllowed === false){
          continueAllowed = true;
          allowContinuation();
        }

        replaced = replacedPartView._view;
        swap = this._urlReplace;
        if(swapped){
          createjs.Tween.get(replaced)
            .wait(150)
            .to({
              y: 0, alpha:1
            }, 2000, createjs.Ease.cubicOut)

          createjs.Tween.get(swap)
            .wait(150)
            .to({
              y: 60, alpha:0.4
            }, 2000, createjs.Ease.cubicOut)

          /*createjs.Tween.get(this._urlPartsViewExplanationContainer)
            .wait(750)
            .to({
              alpha:0
            }, 2000)*/

            swapped =false;
            _logger.log("Swapping parts. Going back to the default state.");
        }else{
          // this._urlPartsViewExplanationContainer.alpha = 1;
          this._urlPartsViewExplanationContainer.visible = true;
          createjs.Tween.get(replaced)
            .wait(150)
            .to({
              y: -60, alpha:0.4,
            }, 2000, createjs.Ease.cubicOut)

          createjs.Tween.get(swap)
            .wait(150)
            .to({
              y: 0, alpha:1
            }, 2000, createjs.Ease.cubicOut)

          //createjs.Tween.get(this._urlPartsViewExplanationContainer)
          
            // .wait(500)
            // .to({
            //   alpha:1
            // }, 1500)

            swapped = true;
            _logger.log("Swapping parts. Going to the swapped state.");
        }
      }
      // add container to stage
      this.stage.addChild(this._urlPartsViewContainer, this._urlPartsViewExplanationContainer);

    }
  }

  //this._continueButton = new button(MTLG.getOptions().width-250, MTLG.getOptions().height-100, l("button_Continue"), function(){/*dummy*/ } /*this.tutorial.next*/, this.stage);
  //this._continueButton._view.removeAllEventListeners();
  //this._continueButton._view.alpha = 0.75;

}

// ToDo: A method for individual URLs has to be implemented in the future

var tutSections1 = [
  {'presentationMode': 1,
    'tut-text': "tut-section-1-1",
  },
  {'presentationMode': 1,
    'tut-text': "tut-section-1-2",
  },
  {'presentationMode': 1,
    'tut-text': "tut-section-1-3",
  },
  {'presentationMode': 2,
    'tut-text': "tut-section-1-4",
    'picture': "img/url_basic.jpg",
    'url': {'scheme': "https://", 'subdomain':"sub.domain", 'domain': "registrable-domain", 'tld':"tld", 'path':"/path-way/more"},
    'url_key_view': {'scheme':{'shownName':'url-questionMark', 'shownOn':['scheme'],'shownAbove':['scheme'] }, 'subdomain':{'shownName':'url-questionMark', 'shownOn':['subdomain'],'shownAbove':['subdomain']}, 'domain':{'shownName':'url-questionMark', 'shownOn':['domain'],'shownAbove':['domain'] }, 'tld':{'shownName':'url-questionMark', 'shownOn':['tld'],'shownAbove': ['tld']}, 'path':{'shownName':'url-questionMark', 'shownOn':['path'],'shownAbove':['path'] }, 'query':{'shownName':'url-questionMark', 'shownOn':['query'],'shownAbove':['query'] }, 'fragment':{'shownName':'url-questionMark', 'shownOn':['fragment'],'shownAbove':['fragment']}},
  },
  {'presentationMode': 2,
    'tut-text': "tut-section-1",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-1-5",
    'picture': "img/url_structure_scheme.jpg",
    'url': {'scheme': "https://", 'domain': "registrable-domain", 'tld':"tld"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']}},
    'url_explanation_on': ['scheme'],
  },
  {'presentationMode': 2,
    'tut-text': "tut-section-1",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-1-6",
    'picture': "img/url_structure_host.jpg",
    'url': {'scheme': "https://", 'host': "sub.domain.registrable-domain.tld"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']},'host':{'shownName':'url-host', 'shownOn':['host'],'shownAbove':['host']}},
    'url_explanation_on': ['host'],
  },
  {
    'presentationMode': 2,
    'tut-text': "tut-section-1",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-1-6-2",
    'picture': "img/url_structure_host.jpg",
    'url': {'scheme': "https://", 'host': "172.217.0.9"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']},'host':{'shownName':'url-host', 'shownOn':['host'],'shownAbove':['host']}},
    'url_explanation_on': ['host'],
  },
  {'presentationMode': 2,
    'tut-text': "tut-section-1",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-1-7",
    'picture': "img/url_structure_tld.jpg",
    'url': {'scheme': "https://", 'domain': "registrable-domain",'tld': "tld"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']},'tld':{'shownName':'url-tld', 'shownOn':['tld'],'shownAbove':['tld']}},
    'url_explanation_on': ['tld'],
  },
  /*
  {'presentationMode': 2,
    'tut-text': "tut-section-1",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-1-8",
    'picture': "img/url_structure_etld.jpg",
    'url': {'scheme': "https://", 'domain': "registrable-domain",'etld': "co.uk"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']},'etld':{'shownName':'url-etld', 'shownOn':['etld'],'shownAbove':['etld']}},
    'url_explanation_on': ['etld'],
  },
  */
  {'presentationMode': 2,
    'tut-text': "tut-section-1",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-1-9",
    'picture': "img/url_structure_reg_domain.jpg",
    'url': {'scheme': "https://", 'subdomain': "login", 'domain': "amazon",'tld': "de"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']}, 'domain':{'shownName':'url-rd', 'shownOn':['domain'], 'shownAbove':['domain','tld']},'tld':{'shownName':'url-tld', 'shownOn':['tld'],'shownAbove':['tld']}},
    'url_explanation_on': ['domain'],
  },
  {'presentationMode': 2,
    'tut-text': "tut-section-1",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-1-10",
    'picture': "img/url_structure_reg_domain_etld.jpg",
    'url': {'scheme': "https://", 'domain': "registrable-domain",'tld': "com"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']}, 'domain':{'shownName':'url-rd', 'shownOn':['domain'], 'shownAbove':['domain','tld']},'tld':{'shownName':'url-tld', 'shownOn':['tld'],'shownAbove':['tld']}},
    'url_explanation_on': ['domain'],
  },
  {'presentationMode': 1,
    'tut-text': "tut-section-1-11",
  },
]

let tutSections2 = [
  {'presentationMode': 3,
    'tut-text': "tut-section-2",
    'exp-text': "tut-explanationModeThree",
    'url-text': "tut-section-2-1",
    'picture': "img/url_structure_evil_reg.jpg",
    'url': {'scheme': "https://", 'domain': "example",'tld': "com"},
    'replace_url_part': 'domain',
    'replace_with': 'example-secure',
  },
  {'presentationMode': 3,
    'tut-text': "tut-section-2",
    'exp-text': "tut-explanationModeThree",
    'url-text': "tut-section-2-1-2",
    'picture': "img/url_structure_evil_reg.jpg",
    'url': {'scheme': "https://", 'domain': "amazon",'tld': "com"},
    'replace_url_part': 'domain',
    'replace_with': 'amarzon',
  },
  {'presentationMode': 3,
    'tut-text': "tut-section-2",
    'exp-text': "tut-explanationModeThree",
    'url-text': "tut-section-2-2",
    'picture': "img/url_structure_evil_tld.jpg",
    'url': {'scheme': "https://", 'domain': "example",'tld': "com"},
    'replace_url_part': 'tld',
    'replace_with': 'ml',
  },
  {
    'presentationMode': 1,
    'tut-text': "tut-section-2-3"
  }
  /*
  {'text': "Try to create your first \"real\" Phishing URLs in the next level!",
  },
  */
]

let tutSections3 = [
  {'presentationMode': 2,
    'tut-text': "tut-section-3",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-3-1",
    'picture': "img/url_structure_subdomain.jpg",
    'url': {'scheme': "https://", 'subdomain':"sub.domain",'domain': "registrable-domain",'tld': "tld"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']}, 'subdomain':{'shownName':"Subdomains", 'shownOn':['subdomain'],'shownAbove':['subdomain']}, 'domain':{'shownName':'url-rd', 'shownOn':['domain'], 'shownAbove':['domain','tld']},'tld':{'shownName':'url-tld', 'shownOn':['tld'],'shownAbove':['tld']}},
    'url_explanation_on': ['subdomain'],
  },
  {'presentationMode': 2,
    'tut-text': "tut-section-3",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-3-2",
    'picture': "img/url_structure_subdomain_evil.jpg",
    'url': {'scheme': "https://", 'subdomain':"amazon",'domain': "com-evil",'tld': "ml"},
    'url_key_view': {'subdomain':{'shownName':'url-subdomain', 'shownOn':['subdomain'],'shownAbove':['subdomain']}, 'domain':{'shownName':'url-rd', 'shownOn':['domain'], 'shownAbove':['domain','tld']}},
    'url_explanation_on': ['subdomain'],
  },
  {'presentationMode': 1,
    'tut-text': "tut-section-3-3",
  }
]

let tutSections4 = [
  {'presentationMode': 2,
    'tut-text': "tut-section-4",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-4-1",
    'picture': "img/url_structure_path.jpg",
    'url': {'scheme': "https://", 'host': "domain.tld",'path': "/path-item/more%22path%22"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']}, 'host':{'shownName':'url-host', 'shownOn':['host'], 'shownAbove':['host']},'path':{'shownName':'url-path', 'shownOn':['path'],'shownAbove':['path']}},
    'url_explanation_on': ['path'],
  },
  // TODO True? @Vincent
  /*
  {'presentationMode': 2,
    'tut-text': "tut-section-4",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-4-2",
    'picture': "img/url_structure_path_evil.jpg",
    'url': {'scheme': "https://", 'host': "evil.com",'path': "/benign/login/secure"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']}, 'host':{'shownName':'url-host', 'shownOn':['host'], 'shownAbove':['host']},'path':{'shownName':'url-path', 'shownOn':['path'],'shownAbove':['path']}},
    'url_explanation_on': ['path'],
  },
  */
  {'presentationMode': 2,
    'tut-text': "tut-section-4",
    'exp-text': "tut-explanationModeTwo",
    'url-text': "tut-section-4-3",
    'picture': "img/url_structure_path_complete.jpg",
    'url': {'scheme': "https://", 'host': "domain.tld",'path': "/amazon/login?param=true#fragment"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']}, 'host':{'shownName':'url-host', 'shownOn':['host'], 'shownAbove':['host']},'path':{'shownName':'url-path', 'shownOn':['path'],'shownAbove':['path']}},
    'url_explanation_on': ['path'],
  }
]

let tutSections5 = [
  {'presentationMode': 1,
    'tut-text': "tut-section-5-1",
  }
]
