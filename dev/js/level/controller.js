/**
 * basicController
 */
function basicController(objective, gameState, stage){

  this.stage = stage;
  this.gameState = gameState;

  // Model
  this.urlParts = [];
  this.urlBar = new urlBar();
  this.inputBar = new inputBar();

  // View
  this.urlPartViews = [];

  // Click or drag of URL parts
  this.movementUrlParts = MTLG.getOptions().movement;
  //this.movementButton = new movementButton(this.stage);

  // Countdown for each preset
  this.countdownTime = 20;
  this.countdown;

  // Config
  this.objective = objective;

  // Create objectiveChecker
  this.checker = new objectiveChecker(this.objective);

  // Init urlParts
  for(label of this.objective['labels']){
    this.urlParts.push(new urlPart(label));
  }

  this.taskDescriptionView = new taskDescriptionView(this.objective, 350, 0, MTLG.getOptions().width-500, 220, this.stage);

  this.urlBarAreaView = new urlBarAreaView(this.urlBar, 250, MTLG.getOptions().height-450, 1300, 50, this.stage);

  if(objective['config'] && objective['config']['creationMode']){
    this.inputAreaView = new inputAreaView(this.inputBar, 100, MTLG.getOptions().height-250, 800, 100, this.stage);
  }

  this.urlPartAreaView = new urlPartAreaView(50, 350, MTLG.getOptions().width-200, 250, this.stage);

  this.statusAreaView = new statusAreaView(this.gameState, 1400, MTLG.getOptions().height-250, 400, 200, this.stage);

  this.menuButtons = new menuButtons(this.gameState, this.stage);

  // Init urlPartViews
  this.urlPartAreaView.addUrlPartViews(this.urlParts);
  this.urlPartViews = this.urlPartAreaView.urlPartViews;

  /**
   * Returns UrlBarView
   */
  this.getUrlBarView = () => {
    return this.urlBarAreaView.getUrlBarView();
  }

  /** Returns UrlBarAreaView */
  this.getUrlBarAreaView = () => {
    return this.urlBarAreaView;
  }

  /**
   * Inserts UrlPart to UrlBar at position
   */
  this.insertPart = (viewId, position) => {
    let bar = this.urlBar;
    let barView = this.getUrlBarView();

    // add url part (model) to list of url parts
    let newPart = this.urlParts[viewId];
    bar.addPart(newPart, position);
    barView.addUrlPartView(this.urlPartViews[viewId],position);
    barView.realignUrlPartViews(position);

    // logging event
    _logger.log(`Player inserted UrlPart "${newPart.getLabel()}" to UrlBar at position ${position}.`);
    _logger.log(`The UrlBar now contains the text ` + this.urlBar.getUrl() + `.`);
  }

  /**
   * Appends/Adds UrlPart to UrlBar
   */
  this.addPart = (viewId) => {
    let bar = this.urlBar;
    let barView = this.getUrlBarView();
    if(barView.addedViews.length > 0){
      lastView = barView.addedViews[barView.addedViews.length -1];
      this.urlPartViews[viewId].align(lastView._view.x + lastView.width);
    }else{
      this.urlPartViews[viewId].align(0);
    }
    bar.addPart(this.urlParts[viewId]);
    barView.addUrlPartView(this.urlPartViews[viewId]);
    // logging event
    _logger.log(`Player appended UrlPart "${this.urlParts[viewId].getLabel()}" to UrlBar.`);
    _logger.log(`The UrlBar now contains the text \"` + this.urlBar.getUrl() + `\".`);
  }

  /**
   * Removes UrlPart from UrlBar
   */
  this.removePart = (viewId) => {
    let bar = this.urlBar;
    let barView = this.getUrlBarView();

    bar.removePart(this.urlParts[viewId]);
    curIndex = barView.addedViews.indexOf(this.urlPartViews[viewId]);

    // Remove view
    barView.addedViews.splice(curIndex, 1);

    barView.realignUrlPartViews(curIndex);
    
    // logging event
    _logger.log(`Player removed UrlPart "${this.urlParts[viewId].getLabel()}" from UrlBar.`);
    _logger.log(`The UrlBar now contains the text \"` + this.urlBar.getUrl() + `\".`);
  }


  // TODO: the wrapper should also update the view and model to reflect check results
  // e.g. increase points, clean up, ..., ?
  // TODO: Hoisted so that the function can be passed as callback to verification button.
  this.verifyUrl = () => {
    // logging event
    _logger.log(`Player clicked Verify button.`);
    _logger.log(`The UrlBar currently contains the text ` + this.urlBar.getUrl() + `.`);

    // stop countdown
    clearInterval(this.countdown);

    let url = this.urlBar.getUrl()
    let checkResult = this.checker.check(url);
    let feedbackText = "";
    for(res of checkResult['tests']){
      if(res['passed']){
        feedbackText += "✔\t" + res['desc'] + "\n";
        _logger.log(`Player passed test ` + res["name"] +`.`);
      }else{
        feedbackText += "✘\t" + res['desc'] + "\n";
        _logger.log(`Player failed test ` + res["name"] +`.`);
      }
    }
    let points = checkResult['points'];
    _logger.log(`Player received ` + points +` points.`);
    _logger.log(`Player required ` + this.objective['passingPoints'] +` points to pass.`);
    let result = {};
    if(points >= this.objective['passingPoints']){
      _logger.log(`Player passed the level.`);
      // Add level to finished levels
      this.gameState['finishedLevels'] = this.gameState['finishedLevels'] || {};
      let levelString = 'l' + this.gameState['level'] + 'p' + this.gameState['preset'];
      let currentPoints = this.gameState['finishedLevels'][levelString];
      if(isNaN(currentPoints) || currentPoints < points){
        this.gameState['finishedLevels'][levelString] = points;
      }
      // Add points in gamestate
      let allPoints = 0;
      for(let finished of Object.keys(this.gameState['finishedLevels'])){
        allPoints += this.gameState['finishedLevels'][finished];
      }
      this.gameState['points'] = allPoints;
      // Create result object for result screen
      result = {
        text: feedbackText,
        //text: "Congratulations, you passed!\nYou got " + points + " points" + "\n" + feedbackText,
        points: points,
        successful: true
      };
    }else{
      _logger.log(`Player failed the level.`);
      // Create results object for result screen
      result = {
        text: feedbackText,
        //text: "Try again" + "\n" + feedbackText,
        points: points,
        successful: false
      };
    }
    this._resultView = new resultView(
      result,
      MTLG.getOptions().width/2-400,
      MTLG.getOptions().height/2-300,
      800,
      600,
      finishedCallback.bind(this),
      this.stage
    );
  }

  this.loadLastTutorial = () =>{
    _logger.log(`Player reached last tutorial screen before a level.`);
    // tutorial before test is the level before
    this.gameState['level'] = this.gameState['level'] - 1;
    this.gameState['preset'] = 0;
    this.gameState['presetOrder'] = [];
    this.gameState['presetIndex'] = 0;
    this.gameState['cheat'] = true;
    this.cleanUp();
    MTLG.lc.levelFinished(this.gameState);
  }

  function finishedCallback(){
    //TODO: From here on
    this.cleanUp();
    MTLG.lc.levelFinished(this.gameState);
  }

  this.cleanUp = function(){
    //TODO
    this.stage.removeAllChildren();
  }

  /**
   * Returns next available id for UrlPart
   */
  this.getNextId = () => {
    return this.urlPartViews.length;
  }

  /**
   * Generates Entered URL part
   */
  this.addGeneratedUrlPart = (generatedUrlPart = null, generatedUrlPartView = null) => {
    this.urlParts.push(generatedUrlPart);
    this.urlPartViews.push(generatedUrlPartView);

    // logging event
    _logger.log(`Player generated custom UrlPart "${generatedUrlPart.getLabel()}".`);
  }

  this.getUrlPart = (viewId) => {
    return this.urlParts[viewId];
  }

  this.getUrlPartView = (viewId) => {
    return this.urlPartViews[viewId];
  }



}

//var global_bar;
var _globalController;
