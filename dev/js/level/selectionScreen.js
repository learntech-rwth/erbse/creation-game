function selectionScreen(gameState){
  console.log("selectionScreen");

  let stage = MTLG.getStageContainer();

  // Set background color to deep blue
  MTLG.setBackgroundColor(MTLG.getOptions().bgColor);
  var settingsButton = new cbutton(50, 1000, l('settings_button'), function(){settingsButton._view.visible = false; gameSettings(settingsButton._view);}, "red", stage);

  new uuidView(_logger.getUUID());

  this.gameState = gameState;
  this.gameState['nextLevel'] = "level1"; //TODO: Is this relevant?
  this.gameState['cheat'] = true;
  this.gameState['presetIndex'] = null;
  this.gameState['presetOrder'] = null;

  let buttonOffsetY = 0;
  let i = 0;
  let tutorialCount = 0;
  let previousFinished = true;
  while(i < levels.length){
    let currentLevel = i;
    let currentType = levels[i]['type'];
    if(currentType == "tutorial" || currentType == "transition"){
      //TODO: From here
      if(previousFinished){
        addButton("Tutorial " + (tutorialCount+1), i, 0, 200, 100 + buttonOffsetY, this.gameState);
        tutorialCount++;
      }else{
        break;
      }
      i++;
      let buttonOffsetX = 0;
      while(levels[i] && (levels[i]['type'] == "level" || levels[i]['type'] == null)){
        if(previousFinished){

          for(let j = 0; j < levels[i]['presets'].length; j++){
            if(j == 0 || (this.gameState['finishedLevels'] && this.gameState['finishedLevels']['l'+i+'p'+(j-1)])){
              addButton("Level " + i + " - " + (j+1), i, j, MTLG.getOptions().width / 2 - 50 + buttonOffsetX, 100 + buttonOffsetY, this.gameState);
            }
            buttonOffsetX += MTLG.getOptions().width *.1 + 50;
          }

          if(this.gameState['finishedLevels'] && this.gameState['finishedLevels']['l'+i+'p' + (levels[i]['presets'].length -1)]){
            // TODO
          }else{
            //cheat mode - block flag, if cheatmode activated
            if(MTLG.getOptions().cheatMode != true){
              previousFinished = false;
            }
          }
        }else{
          break;
        }
        i++;
      }
      buttonOffsetY += 100;
    }else{
      i++;
      //TODO
    }
  }

  /**
   * Creates button with custom function to start correct level/tutorial
   */
  function addButton(label, pLevel, pPreset, pX, pY, pGameState){
    let level = pLevel;
    let preset = pPreset;
    let x = pX;
    let y = pY;
    this.gameState = pGameState;
    let levelButton = button(pX, pY, label,
      function(){
        _logger.log("Selection screen: clicked on button for " + label);
        //this.gameState['level'] = parseInt(currentLevel.slice(1));
        gameState['level'] = parseInt(level);
        gameState['preset'] = preset;
        console.log("Used gamestate: " + JSON.stringify(gameState));
        MTLG.lc.levelFinished(this.gameState);
      }.bind(this),
      MTLG.getStageContainer()
    );
  }
}