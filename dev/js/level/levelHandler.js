function levelHandler(gameState){
  // If gameState['level'] does not exists, the function was called for the first time.
  // Set everything up.
  if(gameState['level'] === null || gameState['level'] === undefined){
    gameState['level'] = 0;
    gameState['preset'] = 0;
    gameState['cheat'] = true; // Do not skip first level!
  }


  // If selection is set, go to selection screen
  if(gameState['nextLevel'] == "selection"){
    _logger.log("Entering selection menu");
    return selectionScreen(gameState);
  }

  // Make it possible to quickly jump to defined level using "cheat"
  // In this case, the calculation for the next level is skipped.
  if(gameState['cheat'] == true){
    gameState['cheat'] = false;
    // If we jump from an ongoing level it was probably not cleaned up
    if(_globalController){
      _globalController.cleanUp();
    }
    return goToLevel(gameState);
  }

  // Now we are in the "normal" case, so we increase the level.
  // We check if it is necessary to go to the next level, or only increase the preset
  if((gameState['presetOrder'] && gameState['presetIndex'] >= gameState['presetOrder'].length-1)
    || (!gameState['presetOrder'] && levels[gameState['level']]['presets'].length -1 <= gameState['preset'])){
    _logger.log("All presets done, entering next level");
    gameState['level']++;
    setupPreset(gameState);
  }else{
    _logger.log("Preset done, starting next preset");
    if(gameState['presetOrder']){
      gameState['preset'] = gameState['presetOrder'][++gameState['presetIndex']];
    }else{
      gameState['preset']++;
    }
  }
  return goToLevel(gameState);




  function setupPreset(gameState){
    let presetOrder = [];
    for(let i = 0; i < levels[gameState['level']]['presets'].length; i++){
      presetOrder.push(i);
    }
    if(levels[gameState['level']]['randomize']){ //TODO: only shuffle specified presets?
      shuffleArray(presetOrder);
    }
    gameState['presetOrder'] = presetOrder;
    gameState['presetIndex'] = 0;

    gameState['preset'] = presetOrder[0] || 0;
    // Check if this level exists
    if(levels[gameState['level']] === null || levels[gameState['level']] === undefined){ //null or undefined
      return selectionScreen(gameState);
    }
  }

  function goToLevel(gameState){
    if(window.hasOwnProperty("_saver")){
      _saver.save();
    }
    // We can add transitions to tutorial levels, story elements, transitional menus, ... here
    let currentLevel = levels[gameState['level']];
    switch(currentLevel['type']){
      case "tutorial":
        _logger.log("Entering tutorial");
        tutorial(gameState);
        break;
      case "transition":
        _logger.log("Entering transition");
        transition(gameState);
        break;
      case "story":
        break;
      default:
        _logger.log("Entering level");
        firstlevel_init(gameState);
        break;
    }
  }
}

function checkLevelHandler(gameState){
  //Handles all level transitions
  if(gameState){
    return 1;
  }else{
    return 0;
  }
}
