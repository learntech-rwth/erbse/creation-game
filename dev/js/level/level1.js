function firstlevel_init(gameState){
  // initialize level structure

  // Set background color to deep blue
  MTLG.setBackgroundColor(MTLG.getOptions().bgColor);
  
  _learnerProfile.restoreGameState();


  /*
   * TODO: Put level config into own file.
   * Seperate objects: e.g. base labels into array,
   * Targets from personalization interface (TODO: Extend by URL(s)?)
   *  Personalization object: at least target name + url
   * Config (includes label type, ...), checks etc. should be level dependent.
   * Level should be called with only a gamestate and should decide:
   * Is this the first time? -> Setup (determine preset order (e.g. if we want to have one preset first), ...?)
   * Else: Find out which level (and set up level config) and which preset
   */

  console.log("____________________________________________________");
  console.log(JSON.stringify(levels[gameState['level']], null, 4));
  console.log(JSON.stringify(levelData[gameState['level']], null, 4));
  levels[gameState['level']] = levelGeneratorCG.createLevel(levelData[gameState['level']]);
  console.log(JSON.stringify(levels[gameState['level']], null, 4));

  /**
   * Helper function for loading options
   */
  let loadOptions = function(level, preset){
    _logger.log("Loading options for level " + level + " and preset " + preset + ".");
    // console.log("Loading options for level " + level + " and preset " + preset + ".");
    let defaultOptions = {
      'labels' : ['baseLabels'],
      'targets': [],
      'targetsType': "generated",
      'domains': [],
      'domainsType' : "generated",
      'config' : {'creationMode': false},
      'taskDescription': "Default",
      'check': {'domain':50, 'target':50, 'syntax':100},
      'passingPoints': 100,
    };



    let levelOptions = levels[level];
    let presetOptions = levels[level]['presets'][preset];
    let retOptions = Object.assign(defaultOptions, levelOptions, presetOptions);
    return retOptions;
  }

  let posX = 350;
  let posY = 250;

  let helpButton = button(posX, posY, l('help'),
    function(){
      _logger.log("Used help button, showing URL view.");
      showUrlView(posY, posX+200);
    }.bind(this),
    MTLG.getStageContainer()
  );
  
  let hideButton = function() {
    helpButton.hide();
  }

  // Load options (presetOptions > levelOptions > defaultOptions)
  let currentOptions = loadOptions(gameState['level'], gameState['preset']);
  let currentObjective = {};

  // Check if we need to generate targets
  if(currentOptions['targetsType'] === "generated"){
    currentObjective['targets'] = [ generateTargets() ];
  }else{
    currentObjective['targets'] = currentOptions['targets'];
  }

  // Check if we need to generate domains
  if(currentOptions['domainsType'] === "generated"){
    currentObjective['domains'] = [ generateDomains() ];
  }else{
    currentObjective['domains'] = currentOptions['domains'];
  }

  Object.assign(currentOptions, currentObjective); // TODO: Hack!

  let currentLabels = [];
  for(label of currentOptions['labels']){
    currentLabels = currentLabels.concat(labels[label]);
  }
  if(currentOptions['addTargetsLabels']){
    for(target of currentOptions['targets']){
      currentLabels = currentLabels.concat(getDomainLabels(target['url']));
    }
  }
  if(currentOptions['addDomainsLabels']){
    for(domain of currentOptions['domains']){
      currentLabels = currentLabels.concat(getDomainLabels(domain));
    }
  }

  function getDomainLabels(url){
    let ret = urlUtils.getUrlHostname(url);
    if(ret != null && ret != ""){
      ret = ret.split(".");
    }else{
      ret = url.split(".");
    }
    return ret;
  }

  currentObjective['labels'] = currentLabels;

  Object.assign(currentOptions, currentObjective); // TODO: Hack!

  createTaskDescription(currentOptions);

  // The following line allows an automatic download of a logdata.json file.
  //_logger.exportLogData();

  _globalController = new basicController(currentOptions, gameState, MTLG.getStageContainer());

  // Test Movement
  console.log(_globalController.movementUrlParts);

  // start countdown in preset
  // Note 28.07: Why is the countdown needed?
  if (MTLG.getOptions().useCountdown) {
    startCountdown();
  }

  function generateTargets(){
    // TODO
    let services = [{'name': "Amazon", 'url': "https://www.amazon.de/"}, {'name': "Apple", 'url': "https://apple.com/"}, {'name': "PayPal", 'url': "https://www.paypal.com"}, {'name': "Microsoft", 'url': "https://microsoft.com"}, {'name': "Google", 'url': "https://google.com"}, {'name': "Facebook", 'url': "https://www.facebook.com"}, {'name': "Dropbox", 'url': "https://www.dropbox.com"}, {'name': "Ebay", 'url': "https://www.ebay.com"}, {'name': "Booking.com", 'url':"https://www.booking.com"}];
    let elem = services[Math.floor(Math.random() * services.length)];
    return elem;
  }

  function generateDomains(){
    // TODO
    let services = ["compromised.de", "hacked.com", "own-server.com", "random.de", "test.website", "hack.co.uk", "some-website.io"];
    let elem = services[Math.floor(Math.random() * services.length)];
    return elem;
  }

  /**
   * This function replaces specific keywords in the task description string of the options.
   * Sometimes, we want to include the target name, given domain, ... in the task description.
   * Since these are generated automatically, they have to be replaced at "runtime".
   * For this purpose, it is possible to use specific placeholders in the levelConfig.
   * TODO: Currently replaces placeholder with first instance of targets and array of domains.
   */
  function createTaskDescription(options){
    // load lang string first!
    options['taskDescription'] = l(options['taskDescription']);
    if(options['targets'] && options['targets'].length > 0){
      if('displayName' in options['targets'][0]){
        options['taskDescription'] = options['taskDescription'].replace("{{TARGET_NAME}}", options['targets'][0]['displayName'] + " (" + options['targets'][0]['name'] + ")");
        let knowledge = "notSet"
        for(serviceStatus of MTLG.getOptions().pi_serviceStatuses){
         if(serviceStatus in options['targets'][0] && options['targets'][0][serviceStatus] == true){
            knowledge = serviceStatus
          }
        }
        _logger.log("Displayed service in task: " + options['targets'][0]['displayName'] + " (" + options['targets'][0]['name'] + ") with knowledge: " + knowledge)
      }
      else {
        options['taskDescription'] = options['taskDescription'].replace("{{TARGET_NAME}}", options['targets'][0]['name']);
        let knowledge = "notSet"
        for(serviceStatus of MTLG.getOptions().pi_serviceStatuses){
         if(serviceStatus in options['targets'][0] && options['targets'][0][serviceStatus] == true){
            knowledge = serviceStatus
          }
        }
        _logger.log("Displayed service in task: " + options['targets'][0]['name'] + " with knowledge: " + knowledge)
      }
      options['taskDescription'] = options['taskDescription'].replace("{{TARGET_URL}}", options['targets'][0]['url']);
      options['taskDescription'] = options['taskDescription'].replace("{{TARGET_TLD}}", urlUtils.getETld(options['targets'][0]['url']));
    }
    if(options['domains'] && options['domains'].length > 0){
      options['taskDescription'] = options['taskDescription'].replace("{{DOMAINS}}", options['domains']);
    }
    return options;
  }

}

// check wether level 1 is choosen or not
function checkLevel1(gameState){
  if(gameState && gameState.nextLevel && gameState.nextLevel == "level1"){
    return 1;
  }
  return 0;
}

function showUrlView(posY, posX = null){

  
  this.lastShownHoverViews = [];
  this.explanationShownLastTick = false;


  let section = {
    'url': {'scheme': "https://", 'subdomain':"sub.domain",'domain': "registrable-domain",'tld': "tld", 'path':"/path"},
    'url_key_view': {'scheme':{'shownName':'url-scheme', 'shownOn':['scheme'],'shownAbove':['scheme']}, 'subdomain':{'shownName':"Subdomains", 'shownOn':['subdomain'],'shownAbove':['subdomain']}, 'domain':{'shownName':'url-rd', 'shownOn':['domain'], 'shownAbove':['domain','tld']},'tld':{'shownName':'url-tld', 'shownOn':['tld'],'shownAbove':['tld']}, 'path':{'shownName':'url-path', 'shownOn':['path'],'shownAbove':['path']}},
  }
  if(section['url']){

    // logic URL parts
    this._urlParts =  [];
    this._urlPartsView = [];

    // container for URL-block, URL-keys and explanation
    this._urlPartsViewContainer = new createjs.Container();
    this._urlPartsViewKeyContainer = new createjs.Container();
    this._urlPartsViewExplanationContainer = new createjs.Container();
    this._urlPartsViewExplanationContainer.visible = false;

    // save the length-sum of the URL-parts
    let urlLength = 0;

    // logic url parts
    for(let key in section['url']){
      let newUrlPart = new urlPart(section['url'][key]);
      newUrlPart.key = key;
      this._urlParts.push(newUrlPart);

      // check for tld and later eltd - new slash is assosiacted to path and no longer a single sign
      /*if(key == 'tld' || key == 'etld' || key == 'host'){
        let newUrlPartSlash = new urlPart("/");
        newUrlPartSlash.key = key  + '-slash';
        this._urlParts.push(newUrlPartSlash);
      }*/

      // Check for domain to add a dot
      if(key == 'domain' || key == 'subdomain'){
        let newUrlPartDot = new urlPart(".");
        newUrlPartDot.key = key + '-dot';
        this._urlParts.push(newUrlPartDot);
      }
    }

    // view of url parts
    for(let urlPart of this._urlParts){
      let newUrlPartView;
      newUrlPartViewTutorial = new urlPartViewTutorial(urlPart, urlPart.key, urlLength, 0);
      urlLength += newUrlPartViewTutorial.width;

      // push and add
      this._urlPartsView.push(newUrlPartViewTutorial);
      this._urlPartsViewContainer.addChild(newUrlPartViewTutorial._view);

    }

    // position all container after the URL-view generated
    var positionContainer = {x: (posX || (((MTLG.getOptions().width - urlLength) / 2) - 200)), y: posY};
    this._urlPartsViewContainer.x = positionContainer.x;
    this._urlPartsViewContainer.y = positionContainer.y;
    this._urlPartsViewKeyContainer.x = positionContainer.x;
    this._urlPartsViewKeyContainer.y = positionContainer.y - 80;
    this._urlPartsViewExplanationContainer.x = 0;
    this._urlPartsViewExplanationContainer.y = 0;


    // create the keys shown on hover
    if(section['url_key_view']){
      // first look for keys shownAbove more than one URL-part and how often for every single URL-Part
      var multipleShow = [];
      var singleShow = [];
      var keyCount = {};

      // get URL-parts displayed
      for(let urlPart of this._urlPartsView){
        keyCount[urlPart.key] = 0;
      }

      for(let urlPart of this._urlPartsView){
        // check for hover keys
        if(section['url_key_view'][urlPart.key]){
          urlPart.shownName = l(section['url_key_view'][urlPart.key]['shownName']);
          urlPart.shownOn = section['url_key_view'][urlPart.key]['shownOn'];
          urlPart.shownAbove = section['url_key_view'][urlPart.key]['shownAbove'];

          // sort ULR-Parts after key ist shown on multiple parts or not
          if(urlPart.shownAbove.length >= 2){
            multipleShow.push(urlPart);
          }
          else{
            singleShow.push(urlPart);
          }

          // sum up
          for(let key of urlPart.shownOn){ // old: shownAbove
            keyCount[key] += 1;
          }

        }
      }

      // check for elements more than one key is displayed above
      var maxShownOn = 0;
      var maxMultipleShow = 0;
      for(let key in keyCount){
        if(keyCount[key] > maxShownOn){
          maxShownOn = keyCount[key];
        }
      }
      for(let urlPart of multipleShow){
        if(urlPart.shownAbove.length > maxMultipleShow){
          maxMultipleShow = urlPart.shownAbove.length;
        }
      }

      // check for valid show
      var validConfig = false;
      if(maxShownOn <= 2 && maxMultipleShow <= 2){
        validConfig = true;
      }

      // switch for displaying mode of keys
      switch(validConfig){
        case true:
          keyView();
          break;
        default:
          console.log("Keine zugelassene Konfiguration."+
          "Zu einem URL-Part können maximal 2 Keys gleichzeitig angezeigt werden. Momentanes Maximum: " + maxShownOn + "\n" +
          "Maximal zulässige Anzahl shownAbove sind 2 Keys. Momentanes Maximum: " + maxMultipleShow);
      }

      // generate key view
      function keyView(){
        // keys with shownAbove > 2 go upper URL
        // generate upper keys first
        for(let urlPart of multipleShow){
          // compute position of label and line
          // find first second URL-part for multipleShow - distinct element after validConfig
          let secondPart = this._urlPartsView.find(part => urlPart.shownAbove.indexOf(part.key) != -1 && part.key != urlPart.key);

          let startPartX = 0;
          let distanceParts = 0;

          if(urlPart._view.x <= secondPart._view.x){
            startPartX = urlPart._view.x;
            distanceParts = (secondPart._view.x + secondPart.width) - urlPart._view.x;
          }else{
            startPartX = secondPart._view.x;
            distanceParts = (urlPart._view.x + urlPart.width) - secondPart._view.x;
          }

          let keyView = new urlPartViewKey(urlPart, startPartX, distanceParts, 'top');

          // add to container
          this._urlPartsViewKeyContainer.addChild(keyView._view);
        }


        // now single downside keys
        for(let urlPart of singleShow){
          let keyView = new urlPartViewKey(urlPart, urlPart._view.x, urlPart.width, 'bottom');

          // add to container
          this._urlPartsViewKeyContainer.addChild(keyView._view);
        }

      }


    }

    // Add explanation if one is provided
    if(section['url_explanation_on']){
      this._boxUrlText = createBox(310, 500/*220*/, 1300, MTLG.getOptions().height * 0.45, {bgColor:MTLG.getOptions().textbox_urlPart_explanation_bgColor});
      this._urlPartsViewExplanationContainer.addChild(this._boxUrlText);
      this._urlText = createText(l(section['url-text']), {lineWidth: 1300, txtColor: MTLG.getOptions().textbox_urlPart_explanation_txtColor, padding:{lr:20, tb:20}});
      this._urlText.x = 310;
      this._urlText.y = 500/*220*/;
      this._urlPartsViewExplanationContainer.addChild(this._urlText);

    }


    // Ticker for Hit with URL-parts
    if(this._urlPartsViewKeyContainer.children.length > 0){
      var tickerModeTwo = createjs.Ticker.on("tick", tickMouse.bind(this));
      this.tutorial.tickerArray.push(tickerModeTwo);
    }

    function tickMouse(event){
      var showExplanation = false;

        var shownExplanation = false;
        var shownTexts = [];

        // first reset visibility of all URL-parts and explanation
        for(let urlKeyView of this._urlPartsViewKeyContainer.children){
          urlKeyView.visible = false;
        }
        this._urlPartsViewExplanationContainer.visible = false;


        // check for hits
        // has to let, because of constructor with the same name
        for(let urlPartView of this._urlPartsView){
          let point = urlPartView._view.globalToLocal(MTLG.getStage().mouseX, MTLG.getStage().mouseY);

          if(urlPartView._view.hitTest(point.x, point.y) && urlPartView.shownOn){
            let associatedParts = this._urlPartsView.filter(part => urlPartView.shownOn.indexOf(part.key) != -1);

            // show the key
            for(let part of associatedParts){
              let associatedContainer = this._urlPartsViewKeyContainer.children.filter(keyView => keyView.key == part.key);
              for(let container of associatedContainer){
                container.visible = true;
                if(!this.lastShownHoverViews.includes(part.key)){
                  _logger.log("Hovering the " + part.key + " of the URL view.");
                }
                shownTexts.push(part.key);
              }
            }

            // show explanation if necessary
            if(section['url_explanation_on']){
              if(showExplanation == false){
                if(section['url_explanation_on'].indexOf(urlPartView.key) != -1){
                  this._urlPartsViewExplanationContainer.visible = true;
                  showExplanation = true;
                  if(this.explanationShownLastTick == false){
                    _logger.log("Explanation shown.");
                  }
                  shownExplanation = true;


                  // Explanation has been shown - continue is now allowed
                  if(continueAllowed === false){
                    continueAllowed = true;
                    _logger.log("Continuing to the next level is allowed now.");
                    allowContinuation();
                  }
                }
              }
            }
          }
        }
        
        this.explanationShownLastTick = shownExplanation;
        
        // console.log(shownTexts);
        // console.log(this.lastShownHoverView);
        // console.log(shownTexts.length == 0 && this.lastShownHoverView.length != 0);
        
        if(shownTexts.length == 0 && this.lastShownHoverViews.length != 0){
          _logger.log("Stopped hovering.");
        }
        this.lastShownHoverViews = [...shownTexts];
      }

      // add container to stage
      this.stage.addChild(this._urlPartsViewContainer);
      this.stage.addChild(this._urlPartsViewKeyContainer);
      this.stage.addChild(this._urlPartsViewExplanationContainer);
    }
    else{
      console.log("No URL to display!");
    }
}
