let randomNumbers = [];
while (randomNumbers.length != 8) {
  let rNumber = (Math.floor(Math.random() * 256)).toString();

  if (!randomNumbers.includes(rNumber)) {
    randomNumbers.push(rNumber);
  }
}

while (randomNumbers.length != 11) {
  let rNumber = (Math.floor(Math.random() * 744) + 255).toString();

  if (!randomNumbers.includes(rNumber)) {
    randomNumbers.push(rNumber);
  }
}

let labels = {
  'baseLabels' : ["https", "http", ":", "//", "/", "www", ".", ".", ".", "/", "/", "?", "#", "=", "-", "~"],
  'lightLabels' : ["https", ":", "/", "/", ".", "."],
  'extendedLabels' : ["https", "http", ":", "//", "/", "www", ".", ".", ".", "/", "/", "path", "?", "#", "abc", "isSet", "true", "false", "=", "-", "~", "*", "\\", "\\", "\\", "\\\\"],
  'commonTlds' : ["com", "de", "co", "uk", "io", "edu"],
  'randomWords': ["abc", "def", "ghi"],
  'secureWords': ["verify", "password", "secure", "login", "verification"],
  'ipLabel': randomNumbers
}

let levelData = [
  {

  },
  {
    'inOrder' : true,
    'labels' : [['baseLabels', 'randomWords', 'commonTlds', 'secureWords'], ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'], ['baseLabels', 'randomWords', 'commonTlds', 'secureWords', 'ipLabel']],
    'options' : {
        'creationModeEnabled' : false,   
    },
    'taskOptions' : {
      'indexList' : [0,1,2]
    }
  },
  {

  },
  {
    'inOrder' : true,
    'labels' : ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
    'options' : {
        'creationModeEnabled' : true,   
    },
    'taskOptions' : {
      'indexList' : [3,4,5]
    }
  },
  {

  },
  {
    'inOrder' : true,
    'labels' : ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
    'options' : {
        'creationModeEnabled' : true,   
    },
    'taskOptions' : {
      'indexList' : [6,7]
    }
  },
  {

  },
  {
    'inOrder' : true,
    'labels' : ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
    'options' : {
        'creationModeEnabled' : true,   
    },
    'taskOptions' : {
      'indexList' : [8]
    }
  },
  {

  },
  {
    'inOrder' : true,
    'labels' : ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
    'options' : {
        'creationModeEnabled' : true,   
    },
    'taskOptions' : {
      'indexList' : [9,10,12]
    }
  }
]

let levels = [
  //Level 0: Tutorial
  {
    'type' : 'tutorial',
    'presets': [],
  },
  //Level 1: Test basic syntax of Reg Domain and TLD
  {
    'labels' : ['extendedLabels'],
    'creationMode' : false,
    'check' : {'syntax': 100},
    'passingPoints' : 100,
    'domainsType' : "preset",
    'targetsType' : "preset",
    'taskDescription' : "",
    'config': {'creationMode' : false, },
    'presets' : [
      { // Preset 0: Change TLD
        'check' : {'extendedSyntax': 100, 'example1': 50, 'https': 50},
        'passingPoints' : 200,
        'domainsType': "preset",
        'domains': [
          'example.de', 'example.com', 'example.co', 'example.uk', 'example.io', 'example.edu'],
        'targetsType': "preset",
        'targets': [{'url' : 'example'}],
        'labels': ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
        'addDomainsLabels': false,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-1-pre-0',
      },
      { // Preset 1: Change Registrable domain
        'check' : {'extendedSyntax': 100, 'domain_suffix': 50, 'https' : 50},
        'passingPoints' : 200,
        'domainsType': "preset",
        'domains': ['com'],
        'targetsType': "preset",
        'targets': [],
        'labels': ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
        'addDomainsLabels': true,
        'addTargetsLabels': false,
        'taskDescription' : 'task-lvl-1-pre-1',
      },
      { // Preset 3: Change Host to IP
        'check' : {'checkIPUrl': 100, 'https' : 50, 'extendedSyntax': 50},
        'passingPoints' : 200,
        'domainsType': "preset",
        'domains': ['com'],
        'targetsType': "preset",
        'targets': [],
        'labels': ['baseLabels', 'randomWords', 'commonTlds', 'secureWords', 'ipLabel'],
        'addDomainsLabels': true,
        'addTargetsLabels': false,
        'taskDescription' : 'task-lvl-1-pre-3',
      },
      /*
      { // Preset 2: Create valid URL
        'check' : {'extendedSyntax': 200},
        'passingPoints' : 200,
        'domainsType': "preset",
        'domains': [],
        'targetsType': "preset",
        'targets': [],
        'labels': ['extendedLabels', 'randomWords', 'commonTlds', 'secureWords'],
        'addDomainsLabels': false,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-1-pre-2',
      },
      {
        'check' : {'domain' : 25, 'syntax': 100},
        'passingPoints' : 125,
        'domainsType': "generated",
        'domains': [],
        'targetsType': "preset",
        'targets': [],
        'labels': ['baseLabels'],
        'addDomainsLabels': true,
        'addTargetsLabels': false,
      },
      */
      /*
      {
        'check' : {'domain' : 50, 'target': 50, 'syntax': 100},
        'passingPoints' : 200,
      },
      */
    ],
    // 'strictPresets': [0,2] //TODO: Random order for presets?
  },
  //Level 2: "Transition" and Tutorial: Create malicious reg domain
  {
    'type' : 'tutorial',
    'presets': [],
  },
  //Level 3: Test malicious Reg Domain
  {
    'labels' : ['extendedLabels'],
    'creationMode' : true,
    'check' : {'syntax': 100},
    'passingPoints' : 100,
    'domainsType' : "preset",
    'targetsType' : "preset",
    'taskDescription' : "",
    'config': {'creationMode' : true, },
    'presets' : [
      { // Preset 0: Create malicious reg domain, keep TLD etc
        'check' : {'extendedSyntax': 50, 'targetInRegDomain': 100, 'sameTldAsTarget': 50},
        'passingPoints' : 200,
        'domainsType': "preset",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
        'addDomainsLabels': false,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-3-pre-0',
      },
      { // Preset 1: Create malicious reg domain by changing TLD
        'check' : {'extendedSyntax': 50, 'targetInRegDomain': 100, 'changedTld': 50},
        'passingPoints' : 200,
        'domainsType': "preset",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
        'addDomainsLabels': false,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-3-pre-1',
      },
      { // Preset 1: Create malicious reg domain by changing TLD
        'check' : {'extendedSyntax': 50, 'checktypoSquattedUrl': 150},
        'passingPoints' : 200,
        'domainsType': "preset",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
        'addDomainsLabels': false,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-3-pre-2',
      },
    ],
  },
  //Level 4: "Transition" and Tutorial: Subdomains
  {
    'type' : 'tutorial',
    'presets': [],
  },
  //Level 5: Test Subdomains
  {
    'labels' : ['extendedLabels'],
    'creationMode' : true,
    'check' : {'syntax': 100},
    'passingPoints' : 100,
    'domainsType' : "preset",
    'targetsType' : "preset",
    'taskDescription' : "",
    'config': {'creationMode' : true, },
    'presets' : [
      { // Preset 0: Create malicious sub domain
        'check' : {'extendedSyntax': 50, 'targetInSubdomain': 100, 'domain': 50},
        'passingPoints' : 200,
        'domainsType': "generated",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels'],
        'addDomainsLabels': true,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-4-pre-0',
      },
      { // Preset 1: Create malicious sub domain with full embedding
        'check' : {'extendedSyntax': 50, 'targetInSubdomainFull': 100, 'domain': 50},
        'passingPoints' : 200,
        'domainsType': "generated",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels'],
        'addDomainsLabels': true,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-4-pre-1',
      },
    ],
  },
  //Level 6: "Transition" and Tutorial: Path
  {
    'type' : 'tutorial',
    'presets': [],
  },
  //Level 7: Test Path
  {
    'labels' : ['extendedLabels'],
    'creationMode' : true,
    'check' : {'syntax': 100},
    'passingPoints' : 100,
    'domainsType' : "preset",
    'targetsType' : "preset",
    'taskDescription' : "",
    'config': {'creationMode' : true, },
    'presets' : [
      { // Preset 0: Create malicious path
        'check' : {'extendedSyntax': 50, 'targetInPath': 100, 'domain': 50},
        'passingPoints' : 200,
        'domainsType': "generated",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels'],
        'addDomainsLabels': true,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-7-pre-0',
      },
      /*
      { // Preset 1: Create malicious path
        'check' : {'extendedSyntax': 50, 'targetInPath': 100, 'domain': 50},
        'passingPoints' : 200,
        'domainsType': "generated",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels'],
        'addDomainsLabels': true,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-7-pre-1',
      },
      */
    ],
  },
  //Level 8: Transition (Final Level)
  {
    'type': 'tutorial',
    'config': 'finalLevel',
    'presets': [],
  },
  //Level 9: Final Test
  {
    'labels' : ['extendedLabels'],
    'creationMode' : true,
    'check' : {'syntax': 100},
    'passingPoints' : 100,
    'domainsType' : "generated",
    'targetsType' : "generated",
    'taskDescription' : "",
    'config': {'creationMode' : true, },
    'randomize' : [0,1,2],
    'presets' : [
      { // Preset 0: Create malicious reg domain, keep TLD etc
        'check' : {'extendedSyntax': 50, 'targetInRegDomain': 100, 'sameTldAsTarget': 50},
        'passingPoints' : 200,
        'domainsType': "preset",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels', 'randomWords', 'commonTlds', 'secureWords'],
        'addDomainsLabels': false,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-9-pre-0',
      },
      { // Preset 1: Create malicious sub domain
        'check' : {'extendedSyntax': 50, 'targetInSubdomain': 100, 'domain': 50},
        'passingPoints' : 200,
        'domainsType': "generated",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels'],
        'addDomainsLabels': true,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-9-pre-1',
      },
      /*
      { // Preset 2: Create malicious sub domain with full embedding
        'check' : {'extendedSyntax': 50, 'targetInSubdomainFull': 100, 'domain': 50},
        'passingPoints' : 200,
        'domainsType': "generated",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels'],
        'addDomainsLabels': true,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-9-pre-2',
      },
      */
      { // Preset 3: Create malicious path
        'check' : {'extendedSyntax': 50, 'targetInPath': 100, 'domain': 50},
        'passingPoints' : 200,
        'domainsType': "generated",
        'domains': [],
        'targetsType': "generated",
        'targets': [],
        'labels': ['baseLabels'],
        'addDomainsLabels': true,
        'addTargetsLabels': true,
        'taskDescription' : 'task-lvl-9-pre-3',
      },
    ],
  },
  //Level 10: Transition (Finish)
  {
    'type': 'transition',
    'config': 'finish',
    'presets': [],
  },
  /*
  {
    'type': 'transition',
    'config': 'malReg',
    'presets': [],
  },
  */
]
