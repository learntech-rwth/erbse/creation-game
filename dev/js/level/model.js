/**
 * Model for parts of a URL.
 */
function urlPart (pLabel, generated = false) {
  this.label = pLabel || "";
  this.generated = generated;
  this.category = "plain"; // Saves which part of the URL this label belongs to

  this.getLabel = function(){
    return this.label;
  }

}

/**
 * Model for a URL Bar. Contains several URL parts in a given
 * order. The parts can be concatenated to get the current URL
 * of the urlBar.
 */
function urlBar () {
  this.parts = [];

  this.addPart = function(newPart, index){
    if(Number.isInteger(index)){
      if(index > this.parts.length){
        index = this.parts.length;
      }
      this.parts.splice(index, 0, newPart);
    }else{
      this.parts.push(newPart);
    }
  }

  this.removePart = function(oldPart){
    if(this.parts.includes(oldPart)){
      this.parts.splice(this.parts.indexOf(oldPart), 1)
    }
  }

  /**
   * Return the complete current URL as a concatenation of all urlParts.
   * This is not necessarily equal to the serialized URL given by the
   * URL class and constructor.
   */
  this.getUrl = function(){
    let ret = "";
    for (part of this.parts){
      ret += part.label;
    }
    return ret;
  }

}

function inputBar () {
  this.keys_pressed = [];
}
