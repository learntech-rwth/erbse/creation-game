
MTLG.lang.define({
  'en': {
    'pi_start' : 'Start',
    'pi_continue' : 'Continue',
    'pi_back' : 'Back',
    'pi_task': 'Please select the online services you are using',
    'pi_other' : 'Other',
    'pi_rejected': 'Unknown',
    'pi_selected': 'Known',
    'pi_used': 'Used',
    'pi_headline': "Personalization",
    "pi_enabled": "Enabled",
    "pi_disabled": "Disabled",
    "pi_skipError": "Please select an option for every service!",
    "News": "News",
    "Entertainment": "Entertainment", 
    "Shopping": "Purchase and Sale",
    "Communication": "Communication",
    "Finance": "Finance",
    "Others": "Others"
  },
  'de': {
    'pi_start' : 'Start',
    'pi_continue' : 'Weiter',
    'pi_back' : 'Zurück',
    'pi_task': 'Bitte wählen Sie die von Ihnen benutzten Dienste aus',
    'pi_other': 'Sonstige',
    'pi_rejected': 'Unbekannt',
    'pi_selected': 'Bekannt',
    'pi_used': 'Verwendet',
    'pi_headline': "Personalisierung",
    "pi_enabled": "An",
    "pi_disabled": "Aus",
    "pi_skipError": "Bitte wählen Sie eine Option für jeden Dienst aus!",
    "News": "Nachrichten",
    "Entertainment": "Unterhaltung", 
    "Shopping": "An- und Verkauf",
    "Communication": "Kommunikation",
    "Finance": "Finanzen",
    "Others": "Andere"
  }
});
