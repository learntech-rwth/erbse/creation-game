serviceCollection = ({

    // Entertainment

    "Netflix": {
        "name": "Netflix",
        "url": "https://www.netflix.com/de/login/",
        "category": "Entertainment",
        "logoName": "netflix.svg"
    },
    
    "Twitch": {
        "name": "Twitch",
        "url": "https://www.twitch.tv/",
        "category": "Entertainment",
        "logoName": "twitch.svg"
    },
    
    "Fandom": {
        "name": "Fandom",
        "url": "https://www.fandom.com/signin/",
        "category": "Entertainment",
        "logoName": "fandom.svg"
    },
    
    "Disney Plus": {
        "displayName": "Disney Plus",
        "name": "disneyplus",
        "url": "https://www.disneyplus.com/de-de/login/",
        "category": "Entertainment",
        "logoName": "disneyplus.svg"
    },

    "Artstation": {
        "name": "Artstation",
        "url": "https://www.artstation.com/",
        "category": "Entertainment",
        "logoName": "artstation.svg"
    },

    "Origin": {
        "name": "Origin",
        "url": "https://www.origin.com/deu/de-de/store/",
        "category": "Entertainment",
        "logoName": "origin.svg"
    },
    
    "RuneScape": {
        "name": "RuneScape",
        "url": "https://secure.runescape.com/m=weblogin/l=1/loginform?theme=runescape&mod=www/",
        "category": "Entertainment",
        "logoName": "runescape.png"
    },

    "Drakensang": {
        "name": "Drakensang",
        "url": "https://www.drakensang.com/de/einloggen/",
        "category": "Entertainment",
        "logoName": "drakensang.png"
    },

    // News

    "Spiegel": {
        "name": "Spiegel",
        "url": "https://gruppenkonto.spiegel.de/anmelden.html#/",
        "category": "News",
        "logoName": "spiegel.svg"
    },

    "Washington Times": {
        "name": "washingtontimes",
        "displayName": "Washington Times",
        "url": "https://www.washingtontimes.com/",
        "category": "News",
        "logoName": "washingtontimes.svg"
    },

    "Zeit": {
        "name": "Zeit",
        "url": "https://meine.zeit.de/anmelden/",
        "category": "News",
        "logoName": "zeit.svg"
    },

    "ZDF": {
        "name": "ZDF",
        "url": "https://www.zdf.de/",
        "category": "News",
        "logoName": "zdf.svg"
    },

    "Süddeutsche Zeitung": {
        "name": "sueddeutsche",
        "displayName": "Süddeutsche Zeitung",
        "url": "https://id.sueddeutsche.de/login/",
        "category": "News",
        "logoName": "sueddeutsche.svg"
    },

    "Frankfurter Allgemeine": {
        "name": "faz",
        "displayName": "Frankfurter Allgemeine",
        "url": "https://www.faz.net/aktuell/",
        "category": "News",
        "logoName": "faz.svg"
    },

    "Focus": {
        "name": "Focus",
        "url": "https://www.focus.de/",
        "category": "News",
        "logoName": "focus.svg"
    },

    "Rhein-Neckar-Zeitung": {
        "name": "rnz",
        "displayName": "Rhein-Neckar-Zeitung",
        "url": "https://www.rnz.de/",
        "category": "News",
        "logoName": "rnz.svg"
    },
    
    "Legal Tribune Online": {
        "name": "lto",
        "displayName": "Legal Tribune Online",
        "url": "https://www.lto.de/",
        "category": "News",
        "logoName": "lto.svg"
    },

    // Purchase and Sale
    
    "Aliexpress": {
        "name": "Aliexpress",
        "url": "https://de.aliexpress.com/",
        "category": "Shopping",
        "logoName": "aliexpress.svg"
    },

    "Amazon": {
        "name": "Amazon",
        "url": "https://www.amazon.de/ap/signin/",
        "category": "Shopping",
        "logoName": "amazon.svg"
    },

    "Ebay": {
        "name": "Ebay",
        "url": "https://www.ebay.de/signin/",
        "category": "Shopping",
        "logoName": "ebay.svg"
    },
    
    "Ebay-Kleinanzeigen": {
        "name": "Ebay-Kleinanzeigen",
        "url": "https://www.ebay-kleinanzeigen.de/m-einloggen.html?targetUrl=/",
        "category": "Shopping",
        "logoName": "ebaykleinanzeigen.svg"
    },
    
    "Mobile": {
        "name": "Mobile",
        "url": "https://login.mobile.de/mycas/login?requestLogin=true/",
        "category": "Shopping",
        "logoName": "mobile.svg"
    },

    "Otto": {
        "name": "Otto",
        "url": "https://www.otto.de/user/login/",
        "category": "Shopping",
        "logoName": "otto.svg"
    },
    
    "Idealo": {
        "name": "Idealo",
        "url": "https://www.idealo.de/",
        "category": "Shopping",
        "logoName": "idealo.svg"
    },
    
    "Immobilienscout24": {
        "name": "Immobilienscout24",
        "url": "https://sso.immobilienscout24.de/sso/login/",
        "category": "Shopping",
        "logoName": "immobilienscout.svg"
    },

    // Communication
    
    "Facebook": {
        "name": "Facebook",
        "url": "https://www.facebook.com/",
        "category": "Communication",
        "logoName": "facebook.svg"
    },

    "Twitter": {
        "name": "Twitter",
        "url": "https://twitter.com/login/",
        "category": "Communication",
        "logoName": "twitte.svg"
    },

    "Live": {
        "name": "Live",
        "url": "https://login.live.com/login.srf",
        "category": "Communication",
        "logoName": "live.svg"
    },

    "Yahoo": {
        "name": "Yahoo",
        "url": "https://login.yahoo.com/?.src=fpctx&.intl=de&.lang=de-DE/",
        "category": "Communication",
        "logoName": "yahoo.svg"
    },

    "Vk": {
        "name": "Vk",
        "url": "https://vk.com/",
        "category": "Communication",
        "logoName": "vk.svg"
    },

    "Web": {
        "name": "Web",
        "url": "https://web.de/?origin=lpc/",
        "category": "Communication",
        "logoName": "web.svg"
    },

    "Gmx": {
        "name": "Gmx",
        "url": "https://www.gmx.net/?origin=lpc",
        "category": "Communication",
        "logoName": "gmx.svg"
    },

    "Protonmail": {
        "name": "Protonmail",
        "url": "https://mail.protonmail.com/login/",
        "category": "Communication",
        "logoName": "protonmail.svg"
    },


    // Finance

    "PayPal": {
        "name": "PayPal",
        "url": "https://www.paypal.com/de/signin/",
        "category": "Finance",
        "logoName": "paypal.svg"
    },

    "Klarna": {
        "name": "Klarna",
        "url": "https://app.klarna.com/login",
        "category": "Finance",
        "logoName": "klarna.svg"
    },
    
    "Skrill": {
        "name": "Skrill",
        "url": "https://account.skrill.com/wallet/account/login?locale=de/",
        "category": "Finance",
        "logoName": "skrill.svg"
    },

    "Commerzbank": {
        "name": "Commerzbank",
        "url": "https://www.commerzbank.de/",
        "category": "Finance",
        "logoName": "commerzbank.svg"
    },

    "Deutsche Bank": {
        "name": "deutsche-bank",
        "displayName": "Deutsche Bank",
        "url": "https://meine.deutsche-bank.de/trxm/db/",
        "category": "Finance",
        "logoName": "deutschebank.svg"
    },
    
    "Paysafecard": {
        "name": "Paysafecard",
        "url": "https://login.paysafecard.com/customer-auth/?client_id=mypinsPR/",
        "category": "Finance",
        "logoName": "paysafecard.svg"
    },
    
    "Union Bank": {
        "name": "unionbank",
        "displayName": "Union Bank",
        "url": "https://www.unionbank.com/",
        "category": "Finance",
        "logoName": "unionbank.svg"
    },
    
    "Targobank": {
        "name": "Targobank",
        "url": "https://www.targobank.de/de/identification/authentification.html/",
        "category": "Finance",
        "logoName": "targobank.svg"
    },

    // Others

    "Google": {
        "name": "Google",
        "url": "https://accounts.google.com/signin/v2/",
        "category": "Others",
        "logoName": "google.svg"
    },

    "Apple": {
        "name": "Apple",
        "url": "https://secure2.store.apple.com/de/shop/signIn/",
        "category": "Others",
        "logoName": "apple.svg"
    },

    "Wikipedia": {
        "name": "Wikipedia",
        "url": "https://de.wikipedia.org/w/index.php?title=Spezial:Anmelden/",
        "category": "Others",
        "logoName": "wikipedia.svg"
    },

    "Adobe": {
        "name": "Adobe",
        "url": "https://auth.services.adobe.com/en_US/index.html#/",
        "category": "Others",
        "logoName": "adobe.svg"
    },

    "Dropbox": {
        "name": "Dropbox",
        "url": "https://www.dropbox.com/login/",
        "category": "Others",
        "logoName": "dropbox.svg"
    },

    "iCloud": {
        "name": "iCloud",
        "url": "https://www.icloud.com/",
        "category": "Others",
        "logoName": "icloud.svg"
    },

    "Slack": {
        "name": "Slack",
        "url": "https://slack.com/signin#/signin/",
        "category": "Others",
        "logoName": "slack.svg"
    },
    
    "Shopify": {
        "name": "Shopify",
        "url": "https://accounts.shopify.com/store-login/",
        "category": "Others",
        "logoName": "shopify.svg"
    },

    "Codecademy": {
        "name": "Codecademy",
        "url": "https://www.codecademy.com/",
        "category": "Others",
        "logoName": "codecademy.svg"
    },

    "Anki": {
        "name": "ankiweb",
        "displayName": "Anki",
        "url": "https://ankiweb.net/account/login/",
        "category": "Others",
        "logoName": "anki.svg"
    },
    // "Salesforce":{
    //     "name": "Salesforce",
    //     "url": "https://www.salesforce.com/de/",
    //     "category": "Software-as-a-service1"
    // },
    
    // "Microsoft Azure":{
    //     "name": "Microsoft Azure",
    //     "url": "https://azure.microsoft.com/de-de/",
    //     "category": "Software-as-a-service",
    //     "logoName": "azure.png"
    // },

    // "Adobe Creative Cloud":{
    //     "name": "Adobe Creative Cloud",
    //     "url": "https://www.adobe.com/de/creativecloud.html/",
    //     "category": "Software-as-a-service",
    //     "logoName": "adobe.svg"
    // },

    // "Box":{
    //     "name": "Box",
    //     "url": "https://www.box.com/de-de/home/",
    //     "category": "Software-as-a-service",
    //     "logoName": "box.png"
    // },

    // "Amazon Web Services SaaS":{
    //     "name": "Amazon Web Services SaaS",
    //     "url": "https://aws.amazon.com/de/",
    //     "category": "Software-as-a-service",
    //     "logoName": "aws.png"
    // },    

    // "Google G Suite":{
    //     "name": "Google G Suite",
    //     "url": "https://workspace.google.com/intl/de/",
    //     "category": "Software-as-a-service",
    //     "logoName": "gsuite.png"
    // },   
    
    // "Slack":{
    //     "name": "Slack",
    //     "url": "https://slack.com/intl/de-de/",
    //     "category": "Software-as-a-service",
    //     "logoName": "slack.png"
    // },
    
    // "Zendesk":{
    //     "name": "Zendesk",
    //     "url": "https://www.zendesk.de/",
    //     "category": "Software-as-a-service",
    //     "logoName": "zendesk.png"
    // },


    // "ProtonMail":{
    //     "name": "ProtonMail",
    //     "category": "Webmail",
    //     "logoName": "protonmail.png",
    //     "url": "https://protonmail.com/de/"
    // },
    // "Gmail":{
    //     "name": "Gmail",
    //     "category": "Webmail",
    //     "logoName": "gmail.svg",
    //     "url": "https://www.google.com/intl/de/gmail/"
    // },
    // "Outlook":{
    //     "name": "Outlook",
    //     "category": "Webmail",
    //     "logoName": "outlook.png",
    //     "url": "https://outlook.live.com/"
    // },
    // "Yahoo Mail":{
    //     "name": "Yahoo Mail",
    //     "category": "Webmail",
    //     "logoName": "yahoo.png",
    //     "url": "https://mail.yahoo.com/"
    // },
    // "Zoho":{
    //     "name": "Zoho",
    //     "category": "Webmail",
    //     "logoName": "zoho.png",
    //     "url": "https://www.zoho.com/de/mail/"
    // },
    
    
    // "JP Morgan Chase & Co.":{
    //     "name": "JP Morgan Chase & Co.",
    //     "url": "https://www.jpmorganchase.com/",
    //     "category": "Financial Institution",
    //     "logoName": "jpmorgan.png"
    // },

    // "Bank of America":{
    //     "name": "Bank of America",
    //     "url": "https://www.bankofamerica.com/",
    //     "category": "Financial Institution",
    //     "logoName": "bankofamerica.png"
    // },

    // "Wells Fargo":{
    //     "name": "Wells Fargo",
    //     "url": "https://www.wellsfargo.com/",
    //     "category": "Financial Institution",
    //     "logoName": "wellsfargo.png"
    // },

    // "Citigroup":{
    //     "name": "Citigroup",
    //     "url": "https://www.citigroup.com/citi/",
    //     "category": "Financial Institution",
    //     "logoName": "citigroup.png"
    // },

    // "PayPal":{
    //     "name": "PayPal",
    //     "url": "https://www.paypal.com/de/home/",
    //     "category": "Payment",
    //     "logoName": "paypal.png"
    // },

    // "Due":{
    //     "name": "Due",
    //     "url": "https://due.com/",
    //     "category": "Payment",
    //     "logoName": "due.png"
    // },

    // "Stripe":{
    //     "name": "Stripe",
    //     "url": "https://stripe.com/de/",
    //     "category": "Payment",
    //     "logoName": "stripe.png"
    // },
  
    // "Dwolla":{
    //     "name": "Dwolla",
    //     "url": "https://www.dwolla.com/",
    //     "category": "Payment",
    //     "logoName": "dwolla.png"
    // },
    
    // "Apple Pay":{
    //     "name": "Apple Pay",
    //     "url": "https://www.apple.com/de/apple-pay/",
    //     "category": "Payment",
    //     "logoName": "applepay.png"
    // },
   
    // "Payoneer":{
    //     "name": "Payoneer",
    //     "url": "https://www.payoneer.com/",
    //     "category": "Payment",
    //     "logoName": "payoneer.png"
    // },
    
    // "2CheckOut":{
    //     "name": "2CheckOut",
    //     "url": "https://www.2checkout.com/",
    //     "category": "Payment",
    //     "logoName": "2checkout.png"
    // },

    // "Amazon Pay":{
    //     "name": "Amazon Pay",
    //     "url": "https://pay.amazon.de/",
    //     "category": "Payment",
    //     "logoName": "amazonpay.png"
    // },


    // "Amazon":{
    //     "name": "Amazon",
    //     "url": "https://amazon.de/",
    //     "category": "Shopping",
    //     "logoName": "amazon.png"
    // },

    // "eBay":{
    //     "name": "eBay",
    //     "url": "https://www.ebay.de/",
    //     "category": "Shopping",
    //     "logoName": "ebay.png"
    // },
    
    // "Etsy":{
    //     "name": "Etsy",
    //     "url": "https://www.etsy.com/de/",
    //     "category": "Shopping",
    //     "logoName": "etsy.png"
    // },

    // "Overstock":{
    //     "name": "Overstock",
    //     "url": "https://www.overstock.com/",
    //     "category": "Shopping",
    //     "logoName": "overstock.png"
    // },
    
    // "Zappos":{
    //     "name": "Zappos",
    //     "url": "https://www.zappos.com/",
    //     "category": "Shopping",
    //     "logoName": "zappos.png"
    // },

    // "Wish":{
    //     "name": "Wish",
    //     "url": "https://www.wish.com/",
    //     "category": "Shopping",
    //     "logoName": "wish.png"
    // },
    
    // "Alibaba":{
    //     "name": "Alibaba",
    //     "url": "https://german.alibaba.com/",
    //     "category": "Shopping",
    //     "logoName": "alibaba.png"
    // },
    
    // "Aliexpress":{
    //     "name": "Aliexpress",
    //     "url": "https://de.aliexpress.com/",
    //     "category": "Shopping",
    //     "logoName": "aliexpress.png"
    // },

    // "iCloud":{
    //     "name": "iCloud",
    //     "url": "https://www.icloud.com/",
    //     "category": "Cloud Storage",
    //     "logoName": "icloud.png"
    // },
    
    // "Google Drive":{
    //     "name": "Google Drive",
    //     "url": "https://drive.google.com/",
    //     "category": "Cloud Storage",
    //     "logoName": "googledrive.png"
    // },

    // "Microsoft OneDrive":{
    //     "name": "Microsoft OneDrive",
    //     "url": "https://onedrive.live.com/about/de-de/signin/",
    //     "category": "Cloud Storage",
    //     "logoName": "onedrive.png"
    // },

    // "Dropbox":{
    //     "name": "Dropbox",
    //     "url": "https://www.dropbox.com/",
    //     "category": "Cloud Storage",
    //     "logoName": "dropbox.png"
    // },
})