services = [
		{
      "account_action": true,
      "category": "search",
      "cookies": {
        "count": 21,
        "session_cookie": false
      },
      "frequency": {
        "absolute": 4013,
        "datesrelative": 0.9090909090909091,
        "relative": 0.26325111519286276
      },
      "has_bookmarks": false,
      "hostname": "google.com",
      "name": "google",
      "only_bookmarks": false,
      "provides_downloads": false,
			"accepted": true,
			"rejected": false
    },

    {
      "account_action": true,
      "category": "webmail",
      "cookies": {
        "count": 21,
        "session_cookie": false
      },
      "frequency": {
        "absolute": 4013,
        "datesrelative": 0.9090909090909091,
        "relative": 0.26325111519286276
      },
      "has_bookmarks": false,
      "hostname": "gmail.com",
      "name": "gmail.com",
      "only_bookmarks": false,
      "provides_downloads": false,
			"accepted": true,
			"rejected": false
    },

    {
      "account_action": true,
      "cookies": {
        "count": 21,
        "session_cookie": false
      },
      "frequency": {
        "absolute": 4013,
        "datesrelative": 0.9090909090909091,
        "relative": 0.26325111519286276
      },
      "has_bookmarks": false,
      "hostname": "Youtube.com",
      "name": "youtube",
      "only_bookmarks": false,
      "provides_downloads": false,
			"accepted": true,
			"rejected": false
    }
]
  