/**
 * Container for category headline and associated service buttons
 * 
 * @param {*} x 
 * @param {*} y 
 * @param {*} width 
 * @param {*} height 
 * @param {*} stage 
 * @param {*} callback 
 */ 
function buttonAreaView(x, y, width, height, stage, callback) { 

    /**
     * merge services from browser plugin and .json file
     */
    
    this.mergedServices = mergeServices(serviceCollection, MTLG.browserExtensionServices);
    if(MTLG.getOptions().pi_logToConsole) console.log(this.mergedServices);

    // gameState['services'] = mergedServices; //categories2;

    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.stage = stage;
    if(MTLG.getOptions().pi_logToConsole) console.log(this.stage);
    //this.gameState = gameState;
    this.callback = callback;
    this.highestIndex = 0;
    this.maxPages = Math.ceil((Object.keys(otherServices).length)/10);
  
    var options = MTLG.getOptions();

    this.state = 0;
    this.keys = Object.keys(this.mergedServices);
    this.numberOfCategories = this.keys.length;

    /**
     * Split up services in pages of not more than 4 services
     */

    let pages = [];
    let pageKeys = [];
    let pageCategories = [];
    let pageOffset = 0;

    for (categoryKey in this.mergedServices) {

        let serviceKeys = Object.keys(this.mergedServices[categoryKey]);
        if(MTLG.getOptions().pi_logToConsole) console.log(serviceKeys);
        serviceKeys = serviceSorter(serviceKeys);
        if(MTLG.getOptions().pi_logToConsole) console.log(serviceKeys);
        // let categoryLength = Math.min(serviceKeys.length, 12);
        
        let categoryLength = serviceKeys.length;
        let elementsPerPage = 10;
    
        //    categoryLength = Math.min(categoryLength, 12);

        for (j = 0; j <= Math.floor((categoryLength-1)/elementsPerPage) && j < elementsPerPage; j++){ // iterate over pages of 4

            pages[j+pageOffset] = [];
            pageKeys[j+pageOffset] = [];
            pageCategories[j+pageOffset] = categoryKey;

            for (i = j*elementsPerPage; i < j*elementsPerPage+elementsPerPage && i < categoryLength; i++){
                pages[j+pageOffset].push(this.mergedServices[categoryKey][serviceKeys[i]]);
                pageKeys[j+pageOffset].push(serviceKeys[i]);                
            }
        }
        pageOffset += Math.ceil(categoryLength/elementsPerPage);
    }

    if(MTLG.getOptions().pi_logToConsole) console.log("pages")
    if(MTLG.getOptions().pi_logToConsole) console.log(pages)
    if(MTLG.getOptions().pi_logToConsole) console.log("pageKeys")
    if(MTLG.getOptions().pi_logToConsole) console.log(pageKeys)
    if(MTLG.getOptions().pi_logToConsole) console.log("pageCategories")
    if(MTLG.getOptions().pi_logToConsole) console.log(pageCategories)

    this.next = () => {
        _logger.log("Going to next page in personalization interface");
        this.stage.removeAllChildren();
        if(MTLG.getOptions().pi_logToConsole) console.log(this.mergedServices);

        if (this.state >= pages.length -1 ) {
            let pageIndex = this.state - (pages.length - 1)

            if(PersonalizationInterface.entryWasRejected() || pageIndex >= this.maxPages){
                _logger.log("Last page finished in personalization interface, proceeding to game.");  
                let resultServices = [];
                for (cat in this.mergedServices){
                    for (serv in this.mergedServices[cat]){
                        let temp = this.mergedServices[cat][serv]
                        temp.category = cat;
                        temp.hostname = serv;
                        resultServices.push(temp);
                    }
                }
                if(MTLG.getOptions().pi_logToConsole) console.log(resultServices);
                this.callback(resultServices);
            }
            else if(!PersonalizationInterface.entryWasRejected() || this.state < this.highestIndex) {
                // No entry was rejected up to this point, proceed to generate further "others" pages
                _logger.log("No entry generated rejected, generating further page with index " + this.state);
                let _pageCategory = MTLG.lang.getString('pi_other');

                let elementsPerPage = 10;
                let otherKeys = Object.keys(otherServices);
                
                // Populate keys
                let _page = [];
                let _pageKeys = [];

                for(key in otherKeys){
                    if(key >= elementsPerPage * pageIndex && key < elementsPerPage * (pageIndex+1)){
                        _pageKeys.push(otherKeys[key]);
                        _page.push(otherServices[otherKeys[key]]);
                    }
                }

                _pageKeys = serviceSorter(_pageKeys);
                
                for(pageEntry of _pageKeys){
                    if(!(pageEntry in this.mergedServices[_pageCategory])){
                        this.mergedServices[_pageCategory][pageEntry] = _page[_pageKeys.indexOf(pageEntry)];
                        this.mergedServices[_pageCategory][pageEntry]["selected"] = false;
                        this.mergedServices[_pageCategory][pageEntry]["rejected"] = false;
                        this.mergedServices[_pageCategory][pageEntry]["used"] = false;
                    }   
                }
                
                this.state++;
                if(this.state > this.highestIndex){
                    this.highestIndex = this.state;
                }
                return buttonLayout2(this, _pageCategory, _page, _pageKeys, this.stage, this.mergedServices);
            }
        } else {
            this.state++;
            _logger.log("Now displaying page " + this.state);
            return buttonLayout2(this, pageCategories[this.state], pages[this.state], pageKeys[this.state], this.stage, this.mergedServices);
        }
    }

    this.previous = () => {
        
        _logger.log("Going to previous page in personalization interface");  
        if (this.state <= 0) {
            _logger.log("Closing personalization interface");  
            PersonalizationInterface.close();
        } 
        else {
            this.stage.removeAllChildren();
            this.state--;
            if(this.state > pages.length - 1){
                let pageIndex = this.state - (pages.length - 1) - 1
                // No entry was rejected up to this point, proceed to generate further "others" pages
                let _pageCategory = MTLG.lang.getString('pi_other');

                let elementsPerPage = 10;
                let otherKeys = Object.keys(otherServices);
                
                // Populate keys
                let _page = [];
                let _pageKeys = [];

                for(key in otherKeys){
                    if(key >= elementsPerPage * pageIndex && key < elementsPerPage * (pageIndex+1)){
                        _pageKeys.push(otherKeys[key]);
                        _page.push(otherServices[otherKeys[key]]);
                    }
                }

                _pageKeys = serviceSorter(_pageKeys);

                return buttonLayout2(this, _pageCategory, _page, _pageKeys, this.stage, this.mergedServices);
            }
            _logger.log("Now displaying page " + this.state);
            return buttonLayout2(this, pageCategories[this.state], pages[this.state], pageKeys[this.state], this.stage, this.mergedServices);
        }
      }
    
    this.addServices = (services) => {
        if(!Array.isArray(services)){
            services = [services];
        }

        // add service to future category
    }

    buttonLayout2(this, pageCategories[0], pages[0], pageKeys[0], this.stage, this.mergedServices);
}

function allWereSelected(mergedServices, page, categoryKey){
    if(MTLG.getOptions().pi_logToConsole) console.log("mergedServices:");
    if(MTLG.getOptions().pi_logToConsole) console.log(mergedServices);
    if(MTLG.getOptions().pi_logToConsole) console.log("page:");
    if(MTLG.getOptions().pi_logToConsole) console.log(page);
    if(MTLG.getOptions().pi_logToConsole) console.log("categoryKey:");
    if(MTLG.getOptions().pi_logToConsole) console.log(categoryKey);
    for(service in page){
        let mergedServiceEntry; 
        if("displayName" in page[service]){
            mergedServiceEntry = mergedServices[categoryKey][page[service].displayName];
        }
        else {
            mergedServiceEntry = mergedServices[categoryKey][page[service].name];
        }
        if(MTLG.getOptions().pi_logToConsole) console.log("mergedServiceEntry");
        if(MTLG.getOptions().pi_logToConsole) console.log(mergedServiceEntry);
        let serviceStatuses = MTLG.getOptions().pi_serviceStatuses;
        let oneSelected = false;
        for(serviceStatus of serviceStatuses){
            if(serviceStatus in mergedServiceEntry && mergedServiceEntry[serviceStatus]){
                oneSelected = true; 
            }
        }
        if(!oneSelected){
            return false;
        }
    }
    return true;
}

/**
 * Layout for one category
 * OLD LAYOUT, USE buttonLayout2 INSTEAD!
 */

function buttonLayout1(buttonAreaView, categoryKey, page, pageKeys, stage, mergedServices){
    var self = [];

    this.buttonAreaView = buttonAreaView;
    this.page = page;
    this.pageKeys = pageKeys;
    this.categoryKey = categoryKey;
    this.stage = stage;
    this.mergedServices = mergedServices;
    this.options = MTLG.getOptions();

    var scaling = 0.7;
    

    let textOptions = {
        txtColor: this.options["pi_text"],
        fontSize: "40px",
        lineWidth: 1500
    }

    let task = pi_createText(MTLG.lang.getString('pi_task'), textOptions);

    let taskBounds = task.getBounds();

    task.x = (options.width-taskBounds.width)/2;
    task.y = 50;

    this.stage.addChild(task);

    let categoryTextOptions = {
        txtColor: this.options["pi_text"],
        fontSize: "60px",
        lineWidth: 1200
    }

    let categoryText = pi_createText(this.categoryKey, categoryTextOptions);

    let categoryBounds = categoryText.getBounds();

    categoryText.x = (options.width-categoryBounds.width)/2;
    categoryText.y = 200*scaling;

    stage.addChild(categoryText);


    with(MTLG.utils.uiElements) {

        container = new createjs.Container(); // container for all Services displayed in one page
        container._stage = stage;
        container.x = options.width / 2;
        container.y = 280;

        var len = 0;
        var y = 0;

        if(MTLG.getOptions().pi_logToConsole) console.log(this.category);
        for (i in this.pageKeys){
            len++;
        }
        if(MTLG.getOptions().pi_logToConsole) console.log(len);


        for (service in this.page) {

            if(MTLG.getOptions().pi_logToConsole) console.log(service);

            this.buttonContainer = new serviceButton(this.buttonAreaView, this.categoryKey, this.pageKeys[service], this.page[service], this.mergedServices);

            this.buttonContainer.x = (MTLG.getOptions().pi_buttonWidth * 1.1)*( (-len/2) + y) + (MTLG.getOptions().pi_buttonWidth * 0.05);
            this.buttonContainer.y = 0;

            container.addChild(this.buttonContainer);
            y++;
        
        }

        this.stage.addChild(container)

        function continueB(){
            if(MTLG.getOptions().pi_allowSkipping || allWereSelected(this.mergedServices, this.page, this.categoryKey)){
                this.buttonAreaView.next();
            }
        }

        let continueButton = new pi_button(options.width-280, options.height-95, MTLG.lang.getString('pi_continue'), continueB, this.stage, {txtColor: this.options["pi_navButtonText"],bgColor:this.options["pi_navButton"],strokeColor: this.options["pi_navButton"], minHeight: this.options["pi_navButtonMinHeight"], minWidth: this.options["pi_navButtonMinWidth"]});
        let backButton = new pi_button(50, options.height-100, MTLG.lang.getString('pi_back'), this.buttonAreaView.previous, this.stage, {txtColor: this.options["pi_navButtonText"],bgColor:this.options["pi_navButton"]});

    }
}

/**
 * 
 * 
 * @param {*} buttonAreaView 
 * @param {*} categoryKey 
 * @param {*} page 
 * @param {*} pageKeys 
 * @param {*} stage 
 * @param {*} mergedServices 
 */
function buttonLayout2(buttonAreaView, categoryKey, page, pageKeys, stage, mergedServices){
    var self = [];

    this.buttonAreaView = buttonAreaView;
    this.page = page;
    this.pageKeys = pageKeys;
    this.categoryKey = categoryKey;
    this.stage = stage;
    this.mergedServices = mergedServices;
    this.options = MTLG.getOptions();

    var scaling = 0.7;
    
    var elementsPerPage = 10;
    var elementsPerRow = 2;
    var elementsLeftColumn = pageKeys.length - Math.floor(elementsPerPage/elementsPerRow);
    var elementsRightColumn = elementsPerPage - elementsLeftColumn;

    // let textOptions = {
    //     txtColor: this.options["pi_text"],
    //     fontSize: "40px",
    //     lineWidth: 1500
    // }

    // let task = createText(MTLG.lang.getString('pi_task'), textOptions);

    // let taskBounds = task.getBounds();

    // task.x = (options.width-taskBounds.width)/2;
    // task.y = 50;

    // this.stage.addChild(task);

    let categoryTextOptions = {
        txtColor: this.options["pi_text"],
        fontSize: "60px",
        lineWidth: 1200
    }

    let categoryText = pi_createText(MTLG.lang.getString(this.categoryKey), categoryTextOptions);

    let categoryBounds = categoryText.getBounds();

    categoryText.x = (options.width-categoryBounds.width)/2;
    categoryText.y = 25;

    stage.addChild(categoryText);


    with(MTLG.utils.uiElements) {

        container = new createjs.Container(); // container for all Services displayed in one page
        container._stage = stage;
        container.x = 0;
        container.y = MTLG.getOptions().pi_buttonContainerY;

        let len = 0;
        let index = 0;

        if(MTLG.getOptions().pi_logToConsole) console.log(this.category);
        for (i in this.pageKeys){
            len++;
        }
        if(MTLG.getOptions().pi_logToConsole) console.log(len);

        
        for (service in this.page) {
            if(service % 2 == 1){ // is on the right side
                xOffset = 3* options.width / 4 - MTLG.getOptions().pi_buttonWidth/2;
            }
            else {
                xOffset = options.width / 4 - MTLG.getOptions().pi_buttonWidth/2;
            }

            let yOffsetStep = MTLG.getOptions().pi_buttonPadding + MTLG.getOptions().pi_buttonHeight;
            let yOffset = Math.floor(service/2) * yOffsetStep;

            if(MTLG.getOptions().pi_logToConsole) console.log(service);
            if(MTLG.getOptions().pi_logToConsole) console.log(xOffset);
            if(MTLG.getOptions().pi_logToConsole) console.log(yOffset);

            this.buttonContainer = new serviceButton(this.buttonAreaView, this.categoryKey, this.pageKeys[service], this.page[service], this.mergedServices);

            this.buttonContainer.x = xOffset;
            this.buttonContainer.y = yOffset;

            container.addChild(this.buttonContainer);
            index++;
        }

        this.stage.addChild(container)

        let errorTextOptions = {
            txtColor: this.options["pi_text"],
            fontSize: "30px",
            lineWidth: 1200
        }
        this.errorText = pi_createText(MTLG.lang.getString('pi_skipError'), errorTextOptions);
        stage.addChild(errorText);
        this.errorText.x = (options.width-categoryBounds.width)/2;
        this.errorText.y = options.height-95;
        this.errorText.visible = false;

        function continueB(){
            if(MTLG.getOptions().pi_allowSkipping || allWereSelected(this.mergedServices, this.page, this.categoryKey)){
                this.buttonAreaView.next();
            }
            else {
                // show the error message
                this.errorText.visible = true;
            }
        }

        let continueButton = new pi_button(options.width-280, options.height-95, MTLG.lang.getString('pi_continue'), continueB, this.stage, {txtColor: this.options["pi_navButtonText"],bgColor:this.options["pi_navButton"],strokeColor: this.options["pi_navButton"], minHeight: this.options["pi_navButtonMinHeight"], minWidth: this.options["pi_navButtonMinWidth"]});
        let backButton = new pi_button(30, options.height-95, MTLG.lang.getString('pi_back'), this.buttonAreaView.previous, this.stage, {txtColor: this.options["pi_navButtonText"],bgColor:this.options["pi_navButton"],strokeColor: this.options["pi_navButton"], minHeight: this.options["pi_navButtonMinHeight"], minWidth: this.options["pi_navButtonMinWidth"]});
    }
}

