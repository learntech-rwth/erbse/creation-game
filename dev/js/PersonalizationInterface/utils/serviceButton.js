function serviceButton(buttonAreaView, categoryKey, serviceKey, service, mergedServices){
    this.buttonAreaView = buttonAreaView;
    this.mergedServices = mergedServices;
    this.serviceKey = serviceKey;
    this.service = service;
    this.categoryKey = categoryKey;

    this.buttonContainer = new createjs.Container();
    this.buttonContainer._stage = stage;

    this.options = MTLG.getOptions();

    this.buttons = [];

    this.serviceStatuses = this.options['pi_serviceStatuses'];

    this.setSelected = function(index, selected){ // 
        for(tButton in this.buttons){
            this.buttons[tButton].children[0].graphics._fill.style = this.options["pi_" +this.serviceStatuses[tButton] + "Deselected"];
        }
        if(selected){
            this.buttons[index].children[0].graphics._fill.style = this.options["pi_" +this.serviceStatuses[index] + "Selected"];
        }
    }

    this.setState = function(state, selected, mergedServiceEntry){
        for(serviceStatus of this.serviceStatuses){
            mergedServiceEntry[serviceStatus] = false;
        }
        mergedServiceEntry[state] = selected;
        if(state == "rejected"){
            if(selected){
                PersonalizationInterface.modifyRejectedAmount(1);
            }
            else {
                PersonalizationInterface.modifyRejectedAmount(-1);
            }
        }
    }

    this.createButton = function(index, imageShown = true){
        let state = this.serviceStatuses[index];

        let buttonCallback = function() {
            mergedServiceEntry = this.mergedServices[this.categoryKey][this.serviceKey];
            
            if(!(state in mergedServiceEntry) || mergedServiceEntry[state] == false){ // mark as selected, remove rejected if exists
                this.setState(state, true, mergedServiceEntry);
                this.setSelected(index, true);
                _logger.log('Selected "' + this.serviceStatuses[index] + '" for service "' + this.service.name + '"');  
            }
            else if(mergedServiceEntry[state] == true){ // remove selected
                this.setState(state, false, mergedServiceEntry);
                this.setSelected(index, false);
                _logger.log('Deselected "' + this.serviceStatuses[index] + '" for service "' + this.service.name + '"');  
            }
        }.bind(this)

        var options = {
            bgColor: this.options["pi_" +this.serviceStatuses[index] + "Deselected"], 
            strokeColor: this.options["pi_" +this.serviceStatuses[index] + "Deselected"], 
            font: this.options.pi_smallText+"px Arial ",
            txtColor: this.options["pi_buttonText"]
        }       

        let button = pi_backgroundButton(this.options.pi_imageWidth + 3 * (this.options.pi_imageBorder), 0, this.options.pi_smallButtonWidth, this.options.pi_smallButtonHeight, MTLG.lang.getString("pi_" + this.serviceStatuses[index]), buttonCallback, options);

        // Calculate the width for the selected amount of buttons

        let remainingWidthForPadding = 0;
        if(imageShown){
            remainingWidthForPadding = MTLG.getOptions().pi_buttonWidth - (MTLG.getOptions().pi_imageWidth + 4 * (MTLG.getOptions().pi_imageBorder) + 4 * (MTLG.getOptions().pi_imagePadding));
        }
        else {
            remainingWidthForPadding =  MTLG.getOptions().pi_buttonWidth - (4 * (MTLG.getOptions().pi_imageBorder) + 4 * (MTLG.getOptions().pi_imagePadding));
        }
        remainingWidthForPadding -= (this.options.pi_numberOfButtons * this.options.pi_smallButtonWidth);
        remainingWidthForPadding /= (this.options.pi_numberOfButtons-1);
        if(this.options.pi_numberOfButtons == 3 && imageShown){
            button.x = this.options.pi_imageWidth + 2 * (this.options.pi_imageBorder) + 2 * (this.options.pi_imagePadding) + (this.options.pi_numberOfButtons - index - 1) * (this.options.pi_smallButtonWidth + remainingWidthForPadding);
        }
        else if(imageShown) {
            button.x = this.options.pi_imageWidth + 2 * (this.options.pi_imageBorder) + 2 * (this.options.pi_imagePadding) + (this.options.pi_numberOfButtons - index - 1) * (this.options.pi_smallButtonWidth) + ((1 - index) * remainingWidthForPadding) ;
        }
        else if(this.options.pi_numberOfButtons == 3 && !imageShown){
            button.x = + 2 * (this.options.pi_imageBorder) + 2 * (this.options.pi_imagePadding) + (this.options.pi_numberOfButtons - index - 1) * (this.options.pi_smallButtonWidth + remainingWidthForPadding);
        }
        else {
            button.x = 2 * (this.options.pi_imageBorder) + 2 * (this.options.pi_imagePadding) + (this.options.pi_numberOfButtons - index - 1) * (this.options.pi_smallButtonWidth) + ((1 - index) * remainingWidthForPadding) ;
        }

        button.y = this.options.pi_smallButtonY;
        this.buttons[index] = button;
        this.buttonContainer.addChild(button);
    }.bind(this);

    let imageShown = true;
    with(MTLG.utils.uiElements) {
        let button_bgColor;
        if("pi_" + categoryKey + "Button" in this.options){
            button_bgColor = this.options["pi_" + categoryKey + "Button"];
        }
        else {
            button_bgColor = this.options["pi_button"];
        }
        if(this.options.pi_displayLogos == true){ // create button with logo
            let logoContainer = new createjs.Container();
            logoContainer._stage = stage;

            

            let boxOptions = {
                txtColor: this.options["pi_buttonText"],
                bgColor: button_bgColor,
                strokeColor: button_bgColor,
                font: this.options.pi_smallButtonText+"px Arial ",
                textPosition: 0.3,
                line: true,
                textX: MTLG.getOptions().pi_imageWidth + 2 * (MTLG.getOptions().pi_imageBorder) + 2 * (MTLG.getOptions().pi_imagePadding),
                textY: mainTextY,
                serviceButton: true
            }

            
            let imageBorder = this.options.pi_imageBorder;
            let imagePadding = this.options.pi_imagePadding;
            
            let text = new createjs.Text(this.serviceKey, boxOptions.font, boxOptions.txtColor);
            text.textBaseline = "top";
            text.myType = "Text";
            let bounds = text.getBounds();
            var mainTextX = this.options.pi_imageWidth + 3 * (this.options.pi_imageBorder);
            var mainTextY = (this.options.pi_buttonWidth - bounds.height * text.scaleY)*boxOptions.textPosition;

            if(MTLG.getOptions().pi_logToConsole) console.log(this.serviceKey);
            let coloredBox = pi_serviceBox(0, 0, this.options.pi_buttonWidth, this.options.pi_buttonHeight, this.serviceKey, boxOptions);

            logoContainer.addChild(coloredBox);

            if(this.service["logoName"]){
                logoPath = "img/PI_img/logos/".concat(this.service["logoName"]);
            }
            else {
                logoPath = "img/PI_img/categories/".concat(this.service["category"].replace(/ /g,"_"))+".svg";
            }

            let pic = MTLG.assets.getBitmapScaled(logoPath, 1);

            if("image" in pic && pic.image != null){
                let scale = Math.min((this.options.pi_imageWidth - 2 * imageBorder - 2 * imagePadding)/pic.image.width, (this.options.pi_imageHeight - 2 * imageBorder - 2 * imagePadding)/pic.image.height); // scale logo to button size
                pic.scaleX = scale *this.options.pi_logoScale; 
                pic.scaleY = scale *this.options.pi_logoScale; 
                pic.x = imageBorder + imagePadding;
                pic.y = (this.options.pi_buttonHeight - pic.image.height * scale * this.options.pi_logoScale)/2;
    
                let logoBg = new createjs.Shape();
                // logoBg.graphics.beginFill(this.options["pi_logoBg"]).beginStroke(this.options["pi_logoBg"]).setStrokeStyle(0).drawRoundRect(imageBorder/2, imageBorder/2, this.options.pi_imageWidth + imageBorder, Math.min(this.options.pi_imageHeight, this.options.pi_buttonHeight-imageBorder), 3);
                logoBg.graphics.beginFill(this.options["pi_logoBg"]).beginStroke(this.options["pi_logoBg"]).setStrokeStyle(0).drawRoundRect(imageBorder , imageBorder, this.options.pi_imageWidth - (2 * imageBorder), this.options.pi_imageHeight - (2 * imageBorder), 2);
                logoBg.myType = "Rect";
                logoContainer.addChild(logoBg);
                
                logoContainer.addChild(pic);
    
                this.buttonContainer.addChild(logoContainer);
            }
            else {
                let options2 = {
                    txtColor: this.options["pi_buttonText"],
                    bgColor: button_bgColor,
                    strokeColor: button_bgColor,
                    font: this.options.pi_smallButtonText+"px Arial ",
                    textPosition: 0.1,
                    line: true,
                    textX: 2 * this.options.pi_imageBorder + 2 * this.options.pi_imagePadding,
                    textY: mainTextY,
                    serviceButton: true
                }
    
                var newButton = pi_serviceBox(0, 0, this.options.pi_buttonWidth, this.options.pi_buttonHeight, this.serviceKey, options2);
    
                this.buttonContainer.addChild(newButton);
                imageShown = false;
            }
        }else{ // create button without logo

            var mainTextY = (this.options.pi_buttonWidth - bounds.height * text.scaleY)*boxOptions.textPosition;

            let options2 = {
                txtColor: this.options["pi_buttonText"],
                bgColor: button_bgColor,
                strokeColor: button_bgColor,
                font: this.options.pi_smallButtonText+"px Arial ",
                textPosition: 0.1,
                line: true,
                textX: 2 * this.options.pi_imageBorder + 2 * this.options.pi_imagePadding,
                textY: mainTextY,
                serviceButton: true
            }

            var newButton = pi_serviceBox(0, 0, this.options.pi_buttonWidth, this.options.pi_buttonHeight, this.serviceKey, options2);

            this.buttonContainer.addChild(newButton);
            imageShown = false;
        }

        for(var c = 0; c < this.options.pi_numberOfButtons; c++){
            this.createButton(c, imageShown);
        }

        if(MTLG.getOptions().pi_logToConsole) console.log(service);
        for (serviceStatus of this.serviceStatuses){

            let serviceIndex = this.serviceStatuses.indexOf(serviceStatus);

            if(serviceStatus in service && service[serviceStatus] == true){
                this.setState(serviceStatus, true, service);
                this.setSelected(serviceIndex, true);
            }
        }
    }
    return this.buttonContainer;
}
