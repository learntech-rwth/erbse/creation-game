
function serviceSorter(category){
    // valid options: alphabetical, popularity (keeps the order defined in JSON), random and frequency
    switch(MTLG.getOptions().pi_serviceSorting){
        case "alphabetical":
            return sortAlphabetical(category);
            
        case "popularity": 
            // TODO: Should there be a different measurement for this?
            return category;
        
        case "random":
            return sortRandom(category);
            
        case "frequency": 
            // TODO: Actual implementation
            return sortRandom(category);
        
        default: 
            return category;
    }    
}

function sortAlphabetical(category, order){
    let multiplier = -1;
    if(order=="descending"){
        multiplier = 1;
    }
    category.sort(function(a, b){
        if(a.toUpperCase() > b.toUpperCase()){
            return -1 * multiplier;
        }
        else if(a.toUpperCase() < b.toUpperCase()) {
            return 1 * multiplier;
        }
        else {
            return 0;
        }
    })

    return category;
}

function sortAlphabeticalObject(category){
    let categories = Object.keys(category);

    categories.sort(function(a, b){
        if(a.name > b.name){
            return -1;
        }
        else {
            return 1;
        }
    })

    if(MTLG.getOptions().pi_logToConsole) console.log(categories);
    let sorted = {};
    for(entry of categories){
        sorted[entry] = category[entry];
    }

    if(MTLG.getOptions().pi_logToConsole) console.log(sorted);
    return sorted;
}

function sortRandom(category){
    pi_shuffleArray(category);
    return category;
}

/**
 * From https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
 */
function pi_shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }
  