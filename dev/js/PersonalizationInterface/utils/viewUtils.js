/**
 * Source: https://gitlab.com/learntech-rwth/MTLG-Games/phishing-url-creation-game
 */

/** 
 * Creates Button
 */
function pi_button(x, y, label, callback, stage = null, options = null){
  let defaultOptions = {
    txtColor: "#083878",
    bgColor: "#2494a2",
  }
  options = Object.assign(defaultOptions, options);
  this._view = pi_createTextBox(label, options);
  this._view.removeAllEventListeners();
  this._view.addEventListener('pressup', callback);
  this._view.x = x;
  this._view.y = y;

  stage.addChild(this._view);
}


/** 
 * Creates Box
 */
function pi_createBox(x, y, width, height, options = null) {
  let defaultOptions = {
    bgColor: "#ffffff",
    strokeColor: "#ffffff",
    alpha: 1.0,
  }
  options = Object.assign(defaultOptions, options);

  if(MTLG.getOptions().pi_logToConsole) console.log(options);

  let retCont = new createjs.Container();
  let shape = new createjs.Shape();
  shape.graphics.beginFill(options.bgColor).beginStroke(options.strokeColor).drawRect(0, 0, width, height);
  shape.myType = "Rect";
  shape.alpha = options.alpha;
  retCont.addChild(shape);
  retCont.x = x;
  retCont.y = y;
  return retCont;
}

/**
 * Creates Text
 */
function pi_createText(label, options = null) {

  let defaultOptions = {
    txtColor: "#9E9E9E",
    bgColor: "#ffffff",
    padding : {
      lr: 25, //left-right padding
      tb: 8 //top-bottom padding
    },
    fontSize: "30px",
    font: "Arial",
    lineWidth: 900
  }
  options = Object.assign(defaultOptions, options);

  let retCont = new createjs.Container();
  let text = new createjs.Text(label+" ", options.fontSize+" "+options.font, options.txtColor);
  text.set({lineWidth: (options.lineWidth-2*options.padding.lr)});
  text.textBaseline = "top";
  text.x = options.padding.lr; //TODO
  text.y = options.padding.tb;
  text.myType = "Text";
  //text.outline = 1;
  retCont.text = text;
  let bounds = text.getBounds();
  bounds.width += options.padding.lr*2; //TODO
  bounds.height += options.padding.tb*2; //TODO

  retCont.addChild(text);
  retCont.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);

  /**
   * Appends character to text in input bar
   */
  retCont.addChar = function (newChar) {
    // if(MTLG.getOptions().pi_logToConsole) console.log("add char "+newChar);
    this.text = this.text.trim()+newChar.trim();
    // logging event
    _logger.log(`Player added new character "${newChar.trim()}" via keyboard.`);
  }.bind(text);

  retCont.setText = function (newText) {
    this.text = newText;
  }.bind(text);

  /**
   * Removes last character of text in input bar
   */
  retCont.removeChar = function () {
    // if(MTLG.getOptions().pi_logToConsole) console.log("remove last char");
    this.text = this.text.slice(0, -1);
    // logging event
    _logger.log(`Player removed last character with 'Backspace' key.`);
  }.bind(text);

  /**
   * Returns text in text element
   */
  retCont.getText = function () {
    return this.text;
  }.bind(text);

  /**
   * Resets text in text element
   */
  retCont.resetText = function () {
    this.text = "";
    this.color = "#000000"
  }.bind(text);

  return retCont;
}

/** 
 * Creates TextBox
 */
function pi_createTextBox(label, options = null) {

  defaultOptions = {
    txtColor: "#000000",
    bgColor: "#ffffff",
    strokeColor: "#000000",
    padding : {
      lr: 25, //left-right padding
      tb: 8 //top-bottom padding
    }
  };
  options = Object.assign(defaultOptions, options);

  let retCont = new createjs.Container();
  retCont.cursor = "pointer"
  let text = new createjs.Text(label, "30px Arial", options.txtColor);
  text.x = options.padding.lr; //TODO
  text.y = options.padding.tb;
  text.textBaseline = "top";
  text.myType = "Text";

  let bounds = text.getBounds();
  let textWidth = bounds.width;
  let textHeight = bounds.height;
  bounds.width += options.padding.lr*2; //TODO
  bounds.height += options.padding.tb*2; //TODO
  if("minWidth" in options){
    bounds.width = Math.max(bounds.width, options.minWidth)
    if(bounds.width == options.minWidth){
      text.x = (options.minWidth-textWidth)/2; 
    }
  }
  if("minHeight" in options){
    let newHeight = Math.max(bounds.height, options.minHeight)
    if(newHeight == options.minHeight){
      text.y = (options.minHeight-textHeight)/2; 
      bounds.height = newHeight;
    }
  }

  let shape = new createjs.Shape();
  shape.graphics.beginFill(options.bgColor).beginStroke(options.strokeColor).drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
  shape.myType = "Rect";

  retCont.addChild(shape);
  retCont.addChild(text);
  retCont.setBounds(bounds.x, bounds.y, bounds.width, bounds.height);
  retCont.addEventListener('pressmove', pi_draggable);
  retCont.addEventListener('pressup', pi_hitObject);

  return retCont;
}

/**
 * Implements drag and drop of URL parts
 */
function pi_draggable (e) { // eslint-disable-line no-unused-vars
  // if Text is selected set target to Container
  let target = e.target;
  //if (e.target.toString().slice(1, 5) === 'Text' || e.target.toString().slice(1, 5) === 'Bitm' || e.target.toString.slice(1,5) === 'Rect') {
  if (["Text", "Rect"].includes(e.target.myType)){
    target = e.target.parent;
  }

  // translates local coordinates in container (where the event occurs)
  // to the general coordinates on stage
  let coords = target.localToLocal(e.localX, e.localY, MTLG.getStageContainer());
  target.x = coords.x;
  target.y = coords.y;
  
  let urlBarView = _globalController.getUrlBarView();

  coords = e.target.localToLocal(0,0, urlBarView._view);
  if(coords.x > 0 && coords.x < urlBarView.width && coords.y > 0 && coords.y < urlBarView.height){
    
    let addedViews = urlBarView.getAddedViews();
    let insertPosition = addedViews.length;

    if (addedViews.length > 0) {
      let first = addedViews[0];
      let widths = addedViews.map((x)=>{return x.width});
      
      let left = 0;
      for (let i = 0; i<widths.length; i++) {
        let currentView = addedViews[i];
        let width = currentView.width;
        let right = left + width;
        
        //if(MTLG.getOptions().pi_logToConsole) console.log("left: "+left+"; right: "+right+"; current: "+coords.x);
        if (coords.x >= left && coords.x < right) {
          let middle = left + width/2;
          // if(MTLG.getOptions().pi_logToConsole) console.log("on elem: ",addedViews[i]);
          if (coords.x < middle) {
            // here we now want to position the elem in between i-1 and i
            //if(MTLG.getOptions().pi_logToConsole) console.log("before ", currentView);
            insertPosition = i;
          } else {
            // here we now want to position the elem in between i and i+1
            //if(MTLG.getOptions().pi_logToConsole) console.log("after ", currentView);
            insertPosition = i + 1;
          }
        }
        left += width;
      }
    }
  }
  /*

  this.x = coords.x
  this.y = coords.y

  /// ////////////////
  // verbose event logging
  /*
  let globalCoords = {
    x: e.stageX,
    y: e.stageY
  }

  this.moveCoord.push({
    x: globalCoords.x,
    y: globalCoords.y
  })
  if (this.moveCoord.length > 1) {
    this.moveDiff.push(Math.sqrt(Math.pow(globalCoords.x - this.moveCoord[this.moveCoord.length - 2].x, 2), Math.pow(globalCoords.y - this.moveCoord[this.moveCoord.length - 2].y, 2)))
  } else {
    this.moveDiff.push(Math.sqrt(Math.pow(globalCoords.x - this.startX, 2), Math.pow(globalCoords.y - this.startY, 2)))
  }
  /// //////////////////
  //
  */
}
  
/**
 * Implements removal of URL parts from URL bar
 */
function pi_removeObject (e) {
  if (["Text", "Rect"].includes(e.target.myType)){
    target = e.target.parent
  }
  
  //if(MTLG.getOptions().pi_logToConsole) console.log(target);
  _globalController.removePart(target.viewObject);
  target.removeEventListener('pressdown', pi_removeObject);
  target.removeEventListener('mousedown', pi_removeObject);
}

/**
 * Implements hit test with URL bar
 */
function pi_hitObject (e) {
  // Check if bar was hit
  // Append current object to end of bar
  // Via controller

  if (["Text", "Rect"].includes(e.target.myType)){
    target = e.target.parent;
  }

  let offset = {
    x: 2,
    y: 4
  };

  let urlBarView = _globalController.getUrlBarView();

  // Check if bar was hit
  let coords = e.target.localToLocal(0,0, urlBarView._view);

  //if(MTLG.getOptions().pi_logToConsole) console.log("hit:",coords);

  if(
    coords.x > 0 && 
    coords.x < urlBarView.width && 
    coords.y > 0 && 
    coords.y < urlBarView.height
    ){
      
    let addedViews = urlBarView.getAddedViews();
    let insertPosition = addedViews.length;

    if (addedViews.length > 0) {
      let first = addedViews[0];
      let widths = addedViews.map((x)=>{return x.width});
      
      let left = 0;
      for (let i = 0; i<widths.length; i++) {
        let currentView = addedViews[i];
        let width = currentView.width;
        let right = left + width;
        
        //if(MTLG.getOptions().pi_logToConsole) console.log("left: "+left+"; right: "+right+"; current: "+coords.x);
        if (coords.x >= left && coords.x < right) {
          let middle = left + width/2;
          // if(MTLG.getOptions().pi_logToConsole) console.log("on elem: ",addedViews[i]);
          if (coords.x < middle) {
            // here we now want to position the elem in between i-1 and i
            //if(MTLG.getOptions().pi_logToConsole) console.log("before ", currentView);
            insertPosition = i;
          } else {
            // here we now want to position the elem in between i and i+1
            //if(MTLG.getOptions().pi_logToConsole) console.log("after ", currentView);
            insertPosition = i + 1;
          }
        }
        left += width;
      }
    }
    
    target.x = urlBarView.x + offset.x;
    target.y = urlBarView.y + offset.y;
    //if(MTLG.getOptions().pi_logToConsole) console.log(target.x, target.y);
    if (insertPosition == addedViews.length) {
      _globalController.addPart(target.viewObject);
    } else {
      //if(MTLG.getOptions().pi_logToConsole) console.log(insertPosition);
      _globalController.insertPart(target.viewObject, insertPosition);
    }
    target.addEventListener('pressdown', pi_removeObject);
    target.addEventListener('mousedown', pi_removeObject);
    //if(MTLG.getOptions().pi_logToConsole) console.log(target);
    
    //TODO: This does not yet work

    //target.x = global_bar._view.localToLocal(0,0, MTLG.getStageContainer()).x;
    //target.y = global_bar._view.localToLocal(0,0, MTLG.getStageContainer()).y;
    //for(item of global_bar.addedViews){
    //if(MTLG.getOptions().pi_logToConsole) console.log(item);
    //target.x += item._view.getBounds().x;
    //}
    //global_bar.addedViews.push(target.viewObject);
  } else {
    // logging event
    _logger.log(`Player moved UrlPart "${_globalController.getUrlPart(target.viewObject).getLabel()}".`);
  }
}

/**
 * Returns random number is given range
 */
function pi_getRandomNumber (min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
}

/**
 * Checks whether overlaps of UrlPartViews exist
 */
function pi_hasOverlap (urlPartView, urlPartViews) {
  for (tmpUrlPartView of urlPartViews) {
    //if(MTLG.getOptions().pi_logToConsole) console.log(tmpUrlPartView, urlPartView);
    if (tmpUrlPartView.getId() != urlPartView.getId()) {
      let isRight = tmpUrlPartView.getX() + tmpUrlPartView.width < urlPartView.getX();
      let isLeft = urlPartView.getX() + urlPartView.width < tmpUrlPartView.getX();
      let isAbove = urlPartView.getY() + urlPartView.height < tmpUrlPartView.getY();
      let isBelow = tmpUrlPartView.getY() + tmpUrlPartView.height < urlPartView.getY();
      if (isRight || isLeft || isAbove || isBelow) {
        continue;
      } else {
        //if(MTLG.getOptions().pi_logToConsole) console.log(tmpUrlPartView, urlPartView);
        return true;
      }
    }
  }
  return false;
}
