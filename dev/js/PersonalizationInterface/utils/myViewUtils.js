/** 
 * Extension of viewUtil.js from https://gitlab.com/learntech-rwth/MTLG-Games/phishing-url-creation-game
 */
function pi_backgroundButton(x, y, width, height, label, callback, options = null){

    let defaultOptions = {
      txtColor: "#083878",
      bgColor: "#2494a2",
      strokeColor: "#2494a2",
      font: "40px Arial", 
      hoverColor: '#2494a2',
      isButton: true
    }
    options = Object.assign(defaultOptions, options);
    this._view = pi_serviceBox(0, 0, width, height, label, options);
    this._view.removeAllEventListeners();
    this._view.addEventListener('pressup', callback);
    this._view.x = x;
    this._view.y = y;
  
    return this._view;
    //stage.addChild(this._view);
  }

/**
 * creates a colored box with text
 */
function pi_serviceBox(x, y, width, height, label, options = null) {
  
    defaultOptions = {
      txtColor: "#000000",
      bgColor: "#ffffff",
      strokeColor: "#ffffff",
      font: "60px Arial",
      textPosition: 0.5,
      line: false,
      textX: 0,
      textY: height/2,
      isButton: false
    };
    options = Object.assign(defaultOptions, options);
  
    let container = new createjs.Container();
    let text = new createjs.Text(label, options.font, options.txtColor);
    text.textBaseline = "top";
    text.myType = "Text";
    if(options.isButton){    
      container.cursor = "pointer"
    }
  
    let bounds = text.getBounds();
  
    if(bounds.width > width*0.9){
      text.scaleX = (width/bounds.width)*0.9;
      text.scaleY = (width/bounds.width)*0.9;
    }
  
    if(options.serviceButton){
      text.x = options.textX;
      text.y = (height - bounds.height * text.scaleY)*options.textPosition;
    }
    else {
      text.x = (width-bounds.width * text.scaleX)/2;
      text.y = (height-bounds.height * text.scaleY)*options.textPosition;
    }
    //if(MTLG.getOptions().pi_logToConsole) console.log(this.bounds);
  
    
    let shape = new createjs.Shape();
    shape.graphics.beginFill(options.bgColor).beginStroke(options.bgColor).setStrokeStyle(1).drawRect(0, 0, width, height);
    //shape.graphics.setStrokeStyle(10);
    shape.myType = "Rect";

    
    // line.graphics.setStrokeStyle(1).beginStroke("#000000");
    // line.graphics.moveTo(MTLG.getOptions().pi_imageWidth + 2 * (MTLG.getOptions().pi_imageBorder) + 10, (height - bounds.height * text.scaleY)*options.textPosition + text.getBounds.height / 2 + 15);
    // line.graphics.lineTo(MTLG.getOptions().pi_buttonWidht - 15, (height - bounds.height * text.scaleY)*options.textPosition -  15);
    // line.graphics.endStroke();

    container.addChild(shape);
    container.addChild(text);
 
    if(options.line){      
      let line = new createjs.Shape();
      line.graphics.beginFill(MTLG.getOptions().pi_serviceBox_lineColor).beginStroke(MTLG.getOptions().pi_serviceBox_lineColor).setStrokeStyle(1).drawRect(
        options.textX, 
        (height - bounds.height * text.scaleY)*options.textPosition + bounds.height + 8, 
        MTLG.getOptions().pi_buttonWidth - (options.textX + 2 * (MTLG.getOptions().pi_imageBorder) + 2 * (MTLG.getOptions().pi_imagePadding)), 
        2
      );
      line.myType = "Rect";
      container.addChild(line);
    }
    container.x = x;
    container.y = y;

    return container;
}