/**
 * An interface for personalizing service selection based on user knowledge. Set the learner profile and init it to use the learner profile.
 */
require('./PersonalizationInterface/services/services.js'); 
require('./PersonalizationInterface/services/example.js');

let PersonalizationInterface = (function(){

  let learnerProfile = null;
  let pi_global_container = null; 
  let m_container = null;
  let startGame = false;
  let pNavigate;
  let numberOfRejectedEntries = 0;

  function setLearnerProfile(lP){
    learnerProfile = lP;
    let flags = [];
    for(var i = 0; i < MTLG.getOptions().pi_numberOfButtons; i++){
      flags.push(MTLG.getOptions().pi_serviceStatuses[i]);
    }
    learnerProfile.init();
  }

  function entryWasRejected(){
    return numberOfRejectedEntries != 0;
  }

  function modifyRejectedAmount(value){
    numberOfRejectedEntries += value;
  }

  function callback(resultServices){
    if(MTLG.getOptions().pi_logToConsole) console.log("PI callback");
    closePI();
    if(learnerProfile != null){
      if(MTLG.getOptions().pi_logToConsole) console.log("Writing to passed learner profile");
      learnerProfile.setServices(resultServices);
    }
    else if(window.hasOwnProperty("_learnerProfile")){
      if(MTLG.getOptions().pi_logToConsole) console.log("Writing to global learner profile");
      _learnerProfile.setServices(resultServices);
    }
    if(startGame){
      _logger.log("Starting creation game after PI.")
      // if(window.hasOwnProperty("gameState")){
      //   gameState = {
      //     ...gameState,
      //     lastLevel: "menu",
      //     exitReason: "nextRound",
      //   };
      // }
      // else {
      //   gameState = {
      //     lastLevel: "menu",
      //     exitReason: "nextRound",
      //   };
      // }
      if(pNavigate != null){
        pNavigate.startGame();
      }
      else {
        MTLG.lc.levelFinished(gameState);
      }
    }
  };

  function closePI(){
    if(MTLG.getOptions().pi_logToConsole) console.log("Removing PI container");
    if(pi_global_container != null){
      pi_global_container.removeAllChildren();
      var stage = MTLG.getStageContainer();
      stage.removeChild(pi_global_container);
    }
    if(m_container != null){
      m_container.visible = true;
    }
  };

  function initPI(menu_container, pStartGame, navigate = null){
    MTLG.pi_isready = false;
  
    if(pStartGame){
      startGame = true;
      pNavigate = navigate;
    }
    // if(MTLG.getOptions().pi_logToConsole) console.log(services);
    var stage = MTLG.getStageContainer();
  
    var pi_container = new createjs.Container();
    pi_global_container = pi_container;

    if(menu_container != null){
      m_container = menu_container;
      m_container.visible = false;;  
    }
    stage.addChild(pi_container);
    
    var pi_background = new createjs.Shape();
    pi_background.graphics.beginFill(MTLG.getOptions()["pi_background"]).beginStroke(MTLG.getOptions()["pi_background"]).setStrokeStyle(1).drawRect(0, 0, MTLG.getOptions().width, MTLG.getOptions().height);
    pi_background.myType = "Rect";
    pi_container.addChild(pi_background);
  
    var pi_elementContainer = new createjs.Container();
    pi_container.addChild(pi_elementContainer);
  
    var options = MTLG.getOptions();
    // stage.removeAllChildren();
  
    /**
     * Introduction text
     */
    let textOptions = {
      txtColor: MTLG.getOptions()["pi_text"],
      fontSize: "60px",
      lineWidth: 1200
    }
  
    var task = pi_createText(MTLG.lang.getString('pi_task'), textOptions); 
  
    let bounds = task.getBounds();
  
    task.x = (options.width-bounds.width)/2;
    task.y = (options.height-bounds.height)*1/3;
  
    pi_elementContainer.addChild(task);
  
    MTLG.importData = function(data){ // Function to be called by the browser plugin
      MTLG.browserExtensionServices = data;
    }
  
    MTLG.pi_isready = true; // Flag that shows that the interface is ready to import services
  
    /**
     * Creates the page, which shows the services
     */
    var initButtons = function() {
      _logger.log("Starting personalization interface");
      pi_elementContainer.removeAllChildren();
      _buttonAreaView = new buttonAreaView(0, 0, MTLG.getOptions().width, MTLG.getOptions().height, pi_elementContainer, callback);
    }

    this.continueButton = new pi_button(MTLG.getOptions().width-280, MTLG.getOptions().height-95, MTLG.lang.getString('pi_continue'), initButtons, pi_elementContainer, {txtColor: MTLG.getOptions()["pi_navButtonText"],bgColor:MTLG.getOptions()["pi_navButton"],strokeColor: MTLG.getOptions()["pi_navButton"], minHeight: MTLG.getOptions()["pi_navButtonMinHeight"], minWidth: MTLG.getOptions()["pi_navButtonMinWidth"]});
    this.backToMenu = new pi_button(30, MTLG.getOptions().height-95, MTLG.lang.getString('pi_back'), closePI, pi_elementContainer, {txtColor: MTLG.getOptions()["pi_navButtonText"],bgColor:MTLG.getOptions()["pi_navButton"],strokeColor: MTLG.getOptions()["pi_navButton"], minHeight: MTLG.getOptions()["pi_navButtonMinHeight"], minWidth: MTLG.getOptions()["pi_navButtonMinWidth"]});
  };

  return {
    init: initPI,
    setLearnerProfile: setLearnerProfile,
    close: closePI,
    entryWasRejected: entryWasRejected,
    modifyRejectedAmount: modifyRejectedAmount
  }
})();


