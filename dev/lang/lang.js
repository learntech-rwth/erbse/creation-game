
MTLG.lang.define({
  en: {
    'start_game' : 'Start Game',
    'start_new_game': "Start New Game",
    'continue_game': "Continue Game",
    //////////////////////////////////
    // Start individual game language
    /////////////////////////////////
    // Settings headlines
    'settings_headline': "Settings",
    'movement_headline': "Movement-mode URL-parts",
    'lang_headline': "Language",
    'cheat_headline': "Cheat-Mode",
    'countdown_headline': "Countdown",
    // Buttons
    'button_Back': "Back",
    'button_Continue': "Continue",
    'button_Menu': "Menu",
    'button_tryAgain': "Try again",
    'button_Generate': "Generate",
    'button_Swap': 'Swap!',
    'button_Verify': "Verify",
    'button_lastTut' : "Repeat tutorial",
    'button_movement': "URL Movement",
    'settings_button': "Open Settings",
    'settings_done': "Close",
    'movement_click': "Click",
    'movement_drag': "Drag",
    'movement_clickdrag': "Click and Drag",
    'cheat_on': "Cheat on",
    'cheat_off': "Cheat off",
    'countdown_on': "Countdown on",
    'countdown_off': "Countdown off",
    // Test scenario
    'url-build-parts-exp': "Use the URL parts to build your own URL here:",
    'task': "TASK",
    'score': "SCORE",
    'points': "points",
    'result_success': "Congratulations!\nYour score: ",
    'result_fail': "That was wrong. Try again!",
    'keyboard-input-exp': "Click here to activate keyboard input",
    'url-part-type-exp': "Type in your own URL parts here:",
    'countdown_expiration_headline': "Countdown",
    'countdown_expiration_text': "Time is up!",
    'countdown_timer_text': "End of Level in:\n",
    // Finish
    'finish-title': "Congratulations, you finished the game!\n\n",
    'finish-text-1': "You earned ",
    'finish-text-2': "points over various levels!\n\n",
    'finish-text-3': "You learned about the structure of URLs and manipulated various URL parts to trick users. You took on the role of a Phisher!\n\n"+
    "Now it's your time to act:\n"+
    "Will you be able to apply your new knowledge to your everyday activities? Will you detect Phishing attacks before you fall for them?\n\n\n"+
    "Thank you for playing!\n"+
    "Please return to the survey now.",
    // Feedback of URL check
    'fb-check-target-more-true': "The URL contains more than one target!",
    'fb-check-target-contain-true': "The URL contains the target!",
    'fb-check-target-contain-false': "The URL does not contain the target!",
    'fb-check-rd-incorrect-true': "The registrable domain is correct!",
    'fb-check-rd-incorrect-false': "The registrable domain is incorrect!",
    'fb-check-rd-end-true': "The registrable domain ends correctly!",
    'fb-check-rd-end-false': "The registrable domain ends incorrectly!",
    'fb-check-scheme-true': "The URL scheme is https!",
    'fb-check-scheme-false': "The URL scheme is not https!",
    'fb-check-rd-belong-false': "The registrable domain belongs to the target!",
    'fb-check-rd-target-true': "The registrable domain contains the target!",
    'fb-check-rd-target-false': "The registrable domain does not contain the target!",
    'fb-check-subdomain-target-true': "A subdomain contains the target!",
    'fb-check-subdomain-target-false': "No subdomain contains the target!",
    'fb-check-subdomain-full-true': "A subdomain contains the full registrable domain of the target!",
    'fb-check-subdomain-full-false': "No subdomain contains the full registrable domain of the target!",
    'fb-check-target-path-true': "The path contains the target!",
    'fb-check-target-path-false': "The path does not contain the target!",
    'fb-check-tld-target-true': "The created URL has the same top-level domain as the target!",
    'fb-check-tld-target-false': "The created URL does not have the same top-level domain as the target!",
    'fb-check-tld-true': "The created URL changed the top-level domain!",
    'fb-check-tld-false': "The created URL did not change the top-level domain!",
    'fb-check-syntax-true': "The structure of the URL is correct!",
    'fb-check-syntax-false': "The structure of the URL is incorrect!",
    'fb-check-exp-tld-true': "The top-level domain was changed correctly!",
    'fb-check-exp-tld-false': "The top-level domain was not changed!",
    'fb-check-exp-rd': "The registrable domain is not correct!",
    'fb-check-typosquatting-false': 'The created URL does not contain typosquatting!',
    'fb-check-typosquatting-true': 'The created URL includes typosquatting!',
    'fb-check-ip-false': 'The created URL does not contain a valid IP address!',
    'fb-check-ip-true': 'The created URL includes a valid IP address!',
    // Level check presets
    'task-lvl-1-pre-0': "Change only the top-level domain (TLD) of the URL 'https://example.com/'!",
    'task-lvl-1-pre-1': "Change the registrable domain of the URL 'https://example.com/', but do not change the top-level domain!",
    'task-lvl-1-pre-3': "Change the host of the URL 'https://example.com/' to an IP address!",
    'task-lvl-1-pre-2': "Create any valid URL.",
    'task-lvl-3-pre-0': "Create a phishing URL that includes the target name below in its registrable domain, without changing the top-level domain (TLD).\n ∎ Target: {{TARGET_NAME}}\n ∎ TLD: {{TARGET_TLD}}",
    'task-lvl-3-pre-1': "Create a phishing URL by changing the top-level domain (TLD).\n ∎ Target: {{TARGET_NAME}}\n ∎ Original TLD: {{TARGET_TLD}}",
    'task-lvl-3-pre-2': "Create a phishing URL by typosquatting the URL.\n ∎ Target: {{TARGET_NAME}}\n ∎ Original TLD: {{TARGET_TLD}}",
    'task-lvl-4-pre-0': "Create a phishing URL that includes the target name below in its subdomain, using the registrable domain '{{DOMAINS}}'.\n ∎ Target: {{TARGET_NAME}}\n",
    'task-lvl-4-pre-1': "Create a phishing URL that includes the target AND top-level domain below in its subdomain (use: name.TLD), using the registrable domain '{{DOMAINS}}'.\n ∎ Target: {{TARGET_NAME}}\n ∎ Target TLD: {{TARGET_TLD}}",
    'task-lvl-7-pre-0': "Create a phishing URL that includes the target name below in its path, using the registrable domain '{{DOMAINS}}'.\n ∎ Target: {{TARGET_NAME}}",
    'task-lvl-7-pre-1': "Create a phishing URL that includes the target name below in its path, using the registrable domain '{{DOMAINS}}'.\n ∎ Target: {{TARGET_NAME}}",
    'task-lvl-9-pre-0': "Create a phishing URL that includes the target name below in its registrable domain, without changing the top-level domain (TLD).\n ∎ Target: {{TARGET_NAME}}\n ∎ TLD: {{TARGET_TLD}}",
    'task-lvl-9-pre-1': "Create a phishing URL that includes the target name below in its subdomain, using the registrable domain '{{DOMAINS}}'.\n ∎ Target: {{TARGET_NAME}}\n",
    'task-lvl-9-pre-2': "Create a phishing URL that includes the target AND top-level domain below in its subdomain (use: name.TLD), using the registrable domain '{{DOMAINS}}'.\n ∎ Target: {{TARGET_NAME}}\n ∎ Target TLD: {{TARGET_TLD}}",
    'task-lvl-9-pre-3': "Create a phishing URL that includes the target name below in its path, using the registrable domain '{{DOMAINS}}'.\n ∎ Target: {{TARGET_NAME}}",
    // Tutorial
    'tut-explanationModeTwo': 'Move your cursor over the displayed URL and search for new information!',
    'tut-explanationModeThree': 'Press the swap button and see what happens!',
    // Tutorial section 1
    'tut-section-1':"Tutorial I - Basic structure of a URL \n\n" ,
    'tut-section-1-1':"Welcome to our Anti-Phishing Learning Game!\n\n"+
                      "Phishing is a type of cyber attack, that can potentially affect everyone using the internet, even you. \n\n" +
                      "In a Phishing attack, the attacker pretends to be someone you trust in order to make you reveal your personal information.\n\n"+
                      "For example, an attacker (called \"Phisher\" in the following) might create a website that looks exactly like your bank's website, and get you to enter your banking information on the fake website.",
    'tut-section-1-2':"So, how can you protect yourself from Phishing?\n\n"+
                      "This game will teach you how to read links to websites (URLs) to help you identify where a link will take you.\n\n" +
      "There are certain rules that everyone (including Phishers!) have to follow when creating URLs. This makes the detection of Phishing websites possible.\n\n"+
                      "With this knowledge, you will be able to tell if a link takes you to the website you expect, or if someone is trying to phish you!",
    'tut-section-1-3':"We will begin the first tutorial with the general structure of URLs, as well as the definition of top-level domains and registrable domains.\n\n"+
      "Don't worry if you cannot remember all of these terms right away, you will be able to look them up during the game.\n\n"+
                      "The structure of a URL might appear a little daunting at first glance, however it follows a logical structure.\n"+
    "Understanding this structure cannot only be used to detect Phishing websites, it can also help you navigate the Internet in general - so there is really no downside to learning it!",
    'tut-section-1-4':"In the following tutorials you will need to gather new information by yourself. Let's start with a test: \n\n"+ "→ Move your cursor over the URL shown below!",
    'tut-section-1-5':"URLs always start with a scheme.\n\n"+
                      "For websites, this is typically HTTPS or HTTP, though this game only uses HTTPS.\n\n"+
                      "Websites that do not use HTTPS do not encrypt your input. As such, you should generally avoid entering your personal information on websites using HTTP.\n\n"+
                      "Furthermore, note that your browser might not always show you the scheme, though we will always include it in this game.",
    'tut-section-1-6':"After the scheme follows the most important part of the URL: the host part. "+
                      "It begins after the scheme and ends with the next slash \"/\" (or at the end of the URL, if there are no more slashes).\n\n"+
      "The host part is important because it identifies the origin of a website.\n\n"+
      "It can be split into smaller parts, so-called \"domains\", which are always separated by a dot.\n\n"+
      "When reading the smaller parts of the host part you should always start with the right-most domain and move to the left, domain by domain!\n",
    'tut-section-1-6-2': "Essentially, an IP address is hidden behind the host part.\n\nFor our daily use, it is usually replaced by a more understandable string.\n\n" +
      "An IP address  consists of four numbers (between 0 and 255), which are separated by dots.\n\n" +
      "You should be especially careful with such URLs, as you do not know which website it leads you to.",
    'tut-section-1-7':"The right-most domain is called top-level domain (TLD) and cannot be choosen freely.\n\n"+
      "When you need to create your own URLs in the following level, you should use valid TLDs (e.g., \".de\" or \".com\").\n",
    'tut-section-1-8':"It is not always possible to register a website directly below a TLD.\n\n"+
                      "For example, there are many websites using \"website.co.uk\" and not \"website.uk\".\n\n"+
                      "If a website was not registered under a TLD but a different point, for example \".co.uk\", we call .co.uk the \"effective top-level domain\" or \"eTLD\" for short.\n\n"+
                      "It functions like a top-level domain in that administrators can register their domain at this point.",
    'tut-section-1-9':"The domain that was actually registered for a website is then called \"registrable domain\" or \"RD\" for short. "+
                      "It includes the first domain before the TLD and the TLD itself.\n\n"+
                      "For example, the host part of the URL shown above consists of three parts: \"login\", \"amazon\", and \"de\". \"de\" is the TLD and \"amazon.de\" is the registrable domain.\n\n"+
      "The registrable domain can be choosen freely, with one important exception: it is not possible to choose an existing domain.\n\n"+
      "As such, a Phisher cannot simply use \"amazon.de\", and instead has to resort to other tricks to create similar-looking domains.",
    'tut-section-1-10':"Characters that are allowed in a domain are: numbers [0-9], letters [a-z], and hyphens [-]. Different domains are always separated by dots [.]\n\n"+
                       "Other special characters (e.g. ?, +, %, !) are not allowed in domains!\n"+
                       "This also includes the Slash [/], which marks the end of the host part.\n\n"+
      "There is no differentiation between uppercase and lowercase letters in domains.",
    'tut-section-1-11':"Now, test your knowledge about the basic structure of URLs in the first level!\n\n"+
                       "Complete the task by dragging URL parts into the designated area. "+
                       "Then, click on \"Verify\" to check if your answer is correct.\n\n" +
                       "Good luck!",
    // Tutorial section 2
    'tut-section-2':"Tutorial II: Phishing Tricks in the registrable domain (RD)\n\n",
    'tut-section-2-1':"The registrable domain (RD) can be chosen freely, subject to the rules explained in the previous tutorial. "+
                      "As such, you can already do \"evil\" things in the registrable domain!\n\n"+
                      "For example, it is possible to add a keyword, like \"secure\" to a given RD. Maybe with a hyphen \"-\", to make it still seem like a benign domain!",
    'tut-section-2-1-2': "Another method is to add small changes to the URL, called typosquatting.\n\nIn this case one character is removed, replaced, added or swapped with another character.",
    'tut-section-2-2':"Alternatively, you can change the top-level domain (TLD) to a different one. \n\n"+
                      "In the following level you will take on the role of a Phisher. To this end, you will have to create Phishing URLs for a given \"target\" (e.g. PayPal, Apple).\n\n"+
    "From now on, you will be able to create your own additional URL parts using an input field.\n\n"+
                      "Have fun creating your first \"real\" Phishing URLs in the next level!",
    'tut-section-2-3': "A tip for outside the game:\n\nIf you are unsure whether a URL is malicious or legitimate, use a search engine to find the allegedly related website!\n\n"+
    "In most cases the correct web page will be shown as the first result and you can compare the URLs.",
    // Tutorial section 3
    'tut-section-3':"Tutorial III - Phishing Tricks in Subdomains \n\n",
    'tut-section-3-1':"We have already seen that the host part of a URL is structured by domains.\n\n"+
                      "It is generally possible to add an arbitrary amount of so-called \"subdomains\" to the left of the registrable domain (RD).\n\n"+
                      "Remember: the RD is the right-most part of the host and cannot be choosen freely.\n\n"+
                      "But: All subdomains to the left of the RD can be chosen completely arbitrarily.\n\n",
    'tut-section-3-2':"For example, in the URL above \"amazon\" is only a subdomain of \"com-evil.ml\".\n\n"+
                      "This opens up a large number of possibile manipulations for Phishing URLs by adding misleading subdomains to a random domain.\n\n"+
                      "Phishers often use this technique on mobile devices with a small screen. This way you only see the beginning of the URL in the browser and not the true identity behind the URL.",
                      //"If you own a (registrable) domain, it is generally possible to alter all parts of the URL.\n\n"+
                      //"The only exception is the RD, which always has to be at the end of the host part of the URL (remember, the RD is always directly in front of the first slash after the scheme)."+
                      //"Thus, the RD plays a very important role in identifying a website.",
    'tut-section-3-3':"In the next level, you will have to create subdomains for a given registrable domain (RD) that contain a given target name.\n\n"+
                      "Try to create misleading subdomains that look as convincing as possible!\n\n"+
                      "Happy Phishing!",
    // Tutorial section 4
    'tut-section-4':"Tutorial IV - Phishing Tricks in the path\n\n",
    'tut-section-4-1':"The last part of a URL is the path. "+
      "It follows the host part and begins with a slash \"/\".\n\n"+
      "While the registrable domain (RD) identifies the owner of a website, the path describes which (sub-) website is displayed.\n\n"+
      "Sometimes the path also contains certain parameters.",
                      //"Conceptually, the host describes at which server a resource is located, while the path describes where the resource is located on the server.\n\n"+
                      //"This is similar to the directory or folder structure on your local computer.",
    'tut-section-4-2':"The path can be case sensitive and can include almost arbitrary characters (they are encoded during transmission but often shown in browsers).\n\n"+
                      "Like the folders of popular operating systems, path components are separated by a slash \"/\". \n\n"+
                      "Looking at the path, you can often see additional information about the resource you are currently requesting/looking at in your browser. \n\n"+
                      "The path ends either with the end of the URL string, or with a question mark \"?\" (whichever comes first).",
    'tut-section-4-3':"It is important to know, that the path can also be chosen completely arbitrarily!\n"+
                      "Different parts of the path are separated using slashes \"/\".\n\n"+
                      "Now let us use the path for Phishing by inserting the registrable domain of our target into it.",
    // Tutorial section 5
    'tut-section-5-1'://"Congratulations! You have completed all of the lessons on URLs in this game!\n\n"+
      "We hope that you learned which parts of a URL are important to detect Phishing websites, and which can be used to trick unsuspecting users in the last levels.\n\n"+
                      "You can now continue to the final level, which tests all your knowledge on URLs and URL manipulation.\n\n"+
                      "Good luck!",
    // Tutorial URL parts
    'url-scheme': "Scheme",
    'url-rd': "Registrable Domain",
    'url-host': "Host",
    'url-subdomain': "Subdomain",
    'url-path': "Path",
    'url-query': "Query",
    'url-fragment': "Fragment",
    'url-tld': "Top-level domain",
    'url-etld': "eTLD",
    'url-questionMark': "?",
    'copy-button': "Copy",
    'copy-button-success': "Copied!",
    'playerid' : "Player-ID",
    'help': "Help"
  },
  de: {
    'start_game': "Spiel starten",
    'start_new_game': "Neues Spiel starten",
    'continue_game': "Spiel fortsetzen",
    //////////////////////////////////
    // Start individual game language
    /////////////////////////////////
    // Settings headlines
    'settings_headline': "Einstellungen",
    'movement_headline': "Bewegungsmodus URL-Teile",
    'lang_headline': "Sprache",
    'cheat_headline': "Cheat-Modus",
    'countdown_headline': "Countdown",
    // Buttons
    'button_Back': "Zurück",
    'button_Continue': "Weiter",
    'button_Menu': "Menü",
    'button_tryAgain': "Wiederholen",
    'button_Generate': "Generieren",
    'button_Swap': 'Tausch!',
    'button_Verify': "Prüfen",
    'button_lastTut' : "Tutorial wiederholen",
    'button_movement': "URL Bewegung",
    'settings_button': "Einstellungen öffnen",
    'settings_done': "Schließen",
    'movement_click': "Klicken",
    'movement_drag': "Ziehen",
    'movement_clickdrag': "Klicken und Ziehen",
    'cheat_on': "Cheat an",
    'cheat_off': "Cheat aus",
    'countdown_on': "Countdown an",
    'countdown_off': "Countdown aus",
    // Test scenario
    'url-build-parts-exp': "Nutze die URL-Teile um deine eigene URL zu konstruieren:",
    'task': "AUFGABE",
    'score': "PUNKTZAHL",
    'points': " Punkte",
    'result_success': "Glückwunsch!\nDeine Punktzahl: ",
    'result_fail': "Das war falsch. Versuch es nochmal!",
    'keyboard-input-exp': "Klicke hier um Tastatureingabe zu aktivieren",
    'url-part-type-exp': "Tippe hier deine eigenen URL-Teile:",
    'countdown_expiration_headline': "Countdown",
    'countdown_expiration_text': "Deine Zeit ist abgelaufen.",
    'countdown_timer_text': "Levelende in:\n",
    // Finish
    'finish-title': "Glückwunsch, du bist am Ende des Spiel angelangt!\n\n",
    'finish-text-1': "Du hast ",
    'finish-text-2': "Punkte über mehrere Level gesammelt!\n\n",
    'finish-text-3': "Du hast den Aufbau einer URL kennengelernt und die verschiedenen Teile manipuliert, um Nutzerinnen und Nutzer reinzulegen. Du hast aus Perspektive eines Phishers gehandelt!\n\n"+
    "Jetzt ist dein Einsatz gefragt:\n"+
    "Kannst du das neue Wissen in deinen Alltag übertragen? Kannst du Phishing Angriffe erkennen, bevor du auf sie hereinfällst?\n\n\n"+
    "Danke fürs Spielen!\n"+
    "Kehre nun zur Umfrage zurück.",
    // Feedback of URL check
    'fb-check-target-more-true': "Die URL enthält mehr als ein Ziel!",
    'fb-check-target-contain-true': "Die URL enthält das Ziel!",
    'fb-check-target-contain-false': "Die URL enthält nicht das Ziel!",
    'fb-check-rd-incorrect-true': "Die Registrierte Domain ist korrekt!",
    'fb-check-rd-incorrect-false': "Die Registrierte Domain ist falsch!",
    'fb-check-rd-end-true': "Die Registrierte Domain endet korrekt!",
    'fb-check-rd-end-false': "Die Registrierte Domain endet nicht korrekt!",
    'fb-check-scheme-true': "Das Schema der URL ist https!",
    'fb-check-scheme-false': "Das Schema der URL ist nicht https!",
    'fb-check-rd-belong-false': "Die Registrierte Domain gehört zum Ziel!",
    'fb-check-rd-target-true': "Die Registrierte Domain enthält das Ziel!",
    'fb-check-rd-target-false': "Die Registrierte Domain enthält nicht das Ziel!",
    'fb-check-subdomain-target-true': "Eine Subdomain enthält das Ziel!",
    'fb-check-subdomain-target-false': "Keine Subdomain enthält das Ziel!",
    'fb-check-subdomain-full-true': "Eine Subdomain enthält die komplette gewünschte Registrierte Domain!",
    'fb-check-subdomain-full-false': "Keine Subdomain enthält die komplette gewünschte Registrierte Domain!",
    'fb-check-target-path-true': "Der Pfad enthält das Ziel!",
    'fb-check-target-path-false': "Der Pfad enthält das Ziel nicht!",
    'fb-check-tld-target-true': "Die konstruierte URL hat die gleiche Top-Level-Domain wie das Ziel!",
    'fb-check-tld-target-false': "Die konstruierte URL hat nicht die gleiche Top-Level-Domain wie das Ziel!",
    'fb-check-tld-true': "Die konstruierte URL hat eine veränderte Top-Level-Domain!",
    'fb-check-tld-false': "Die konstruierte URL hat keine veränderte Top-Level-Domain!",
    'fb-check-syntax-true': "Der Aufbau der URL ist korrekt!",
    'fb-check-syntax-false': "Der Aufbau der URL ist nicht korrekt!",
    'fb-check-exp-tld-true': "Die Top-Level-Domain wurde korrekt geändert!",
    'fb-check-exp-tld-false': "Die Top-Level-Domain wurde nicht verändert!",
    'fb-check-exp-rd': "Die Registrierte Domain ist nicht korrekt!",
    'fb-check-typosquatting-false': 'Die konstruierte URL enthält kein Typosquatting!',
    'fb-check-typosquatting-true': 'Die konstruierte URL enthält Typosquatting!',
    'fb-check-ip-false': 'Die konstruierte URL enthält keine valide IP Adresse!',
    'fb-check-ip-true': 'Die konstruierte URL enthält eine valide IP Adresse!',
    // Level check presets
    'task-lvl-1-pre-0': "Ändere nur die Top-Level-Domain (TLD) der URL 'https://example.com/'!",
    'task-lvl-1-pre-1': "Ändere die Registrierte Domain der URL 'https://example.com/', aber lasse die Top-Level-Domain gleich!",
    'task-lvl-1-pre-3': "Ändere den Host der URL 'https://example.com/' zu einer IP-Adresse!",
    'task-lvl-1-pre-2': "Konstruiere eine beliebige gültige URL.",
    'task-lvl-3-pre-0': "Konstruiere eine Phishing-URL, die das untengenannte Ziel innerhalb der Registrierten Domain besitzt, ohne die Top-Level-Domain (TLD) zu ändern.\n ∎ Ziel: {{TARGET_NAME}}\n ∎ TLD: {{TARGET_TLD}}",
    'task-lvl-3-pre-1': "Konstruiere eine Phishing-URL, indem du die Top-Level-Domain (TLD) änderst.\n ∎ Ziel: {{TARGET_NAME}}\n ∎ Ursprüngliche TLD: {{TARGET_TLD}}",
    'task-lvl-3-pre-2': "Konstruiere eine Phishing-URL, indem du Typosquatting auf die URL anwendest.\n ∎ Target: {{TARGET_NAME}}\n ∎ Original TLD: {{TARGET_TLD}}",
    'task-lvl-4-pre-0': "Konstruiere eine Phishing-URL, die das untengenannte Ziel in der Subdomain enthält und die Registrierte Domain '{{DOMAINS}}' benutzt.\n ∎ Ziel: {{TARGET_NAME}}",
    'task-lvl-4-pre-1': "Konstruiere eine Phishing-URL, die das untengenannte Ziel UND Top-Level-Domain in der Subdomain enhält (nutze: Name.TLD) und die Registrierte Domain '{{DOMAINS}}' benutzt.\n ∎ Ziel: {{TARGET_NAME}}\n ∎ TLD: {{TARGET_TLD}}",
    'task-lvl-7-pre-0': "Konstruiere eine Phishing-URL, die das untengenannte Ziel im Pfad enthält und die Registrierte Domain '{{DOMAINS}}' benutzt.\n ∎ Ziel: {{TARGET_NAME}}",
    'task-lvl-7-pre-1': "Konstruiere eine Phishing-URL, die das untengenannte Ziel im Pfad enthält und die Registrierte Domain '{{DOMAINS}}' benutzt.\n ∎ Ziel: {{TARGET_NAME}}",
    'task-lvl-9-pre-0': "Konstruiere eine Phishing-URL, die das untengenannte Ziel in der Registrierten Domain enthält ohne die Top-Level-Domain (TLD) zu verändern.\n ∎ Ziel: {{TARGET_NAME}}\n ∎ TLD: {{TARGET_TLD}}",
    'task-lvl-9-pre-1': "Konstruiere eine Phishing-URL, die das untengenannte Ziel in der Subdomain enthält und die Registrierte Domain '{{DOMAINS}}' benutzt.\n ∎ Ziel: {{TARGET_NAME}}",
    'task-lvl-9-pre-2': "Konstruiere eine Phishing-URL, die das untengenannte Ziel UND Top-Level-Domain in der Subdomain enhält (nutze: Name.TLD) und die Registrierte Domain '{{DOMAINS}}' benutzt.\n ∎ Ziel: {{TARGET_NAME}}\n ∎ TLD: {{TARGET_TLD}}",
    'task-lvl-9-pre-3': "Konstruiere eine Phishing-URL, die das untengenannte Ziel im Pfad enthält und die Registrierte Domain '{{DOMAINS}}' benutzt.\n ∎ Ziel: {{TARGET_NAME}}",
    // Tutorial
    'tut-explanationModeTwo': "Bewege den Cursor über die angezeigte URL und suche nach neuen Informationen!",
    'tut-explanationModeThree': 'Drücke den Tausch-Button und schau was passiert!',
    // Tutorial section 1
    'tut-section-1':"Tutorial I - Aufbau einer URL\n\n",
    'tut-section-1-1':"Herzlich Willkommen zu unserem Anti-Phishing-Lernspiel!\n\n"+
                      "Phishing ist eine Art Cyberangriff und kann potenziell jede Person treffen, die das Internet nutzt. Auch dich. \n\n" +
                      "Bei Phishing geben die Angreifenden vor, jemand vertrautes zu sein, um dich so zur Herausgabe von persönlichen Information zu bringen.\n\n"+
                      "Zum Beispiel kann ein Angreifer oder eine Angreiferin (im folgenden \"Phisher\") eine Webseite erstellen, die bis ins kleinste Detail wie die Webseite deiner genutzen Bank aussieht und dich so zum Eingeben deiner Anmeldedaten auf einer gefälschten Webseite bringen.",
    'tut-section-1-2':"Also, wie kannst du dich vor Phishing schützen?\n\n"+
                      "Dieses Lernspiel vermittelt dir, wie du Links zu Webseiten (URLs) richtig liest und identifizieren kannst, wohin ein Link dich führt.\n\n" +
    "Es gibt Regeln für die Erstellung von URLs, an die sich alle (auch Phisher!) halten müssen. Dies macht ein Erkennen von Phishing Webseiten möglich.\n\n"+
                      "Mit diesem Wissen bist du also in der Lage einzuschätzen, ob dich eine URL auf die Webseite führt, die du erwartest, oder ob du Ziel einer Phishing-Attacke bist!",
    'tut-section-1-3':"Im ersten Tutorial beginnen wir mit dem generellen Aufbau einer URL und der Definition von Top-Level Domains und Registrierten Domains.\n\n"+
    "Mach dir keine Sorgen, wenn du dir die Begriffe nicht alle merken kannst. Du kannst sie während des Spiels jederzeit nachschauen.\n\n"+
                      "Der Aufbau einer URL kann auf den ersten Blick einschüchternd wirken, folgt aber einer logischen Struktur.\n" +
    "Diese Struktur kann nicht nur zur Erkennung von Phishing Webseiten genutzt werden, sondern hilft dir auch generell dich im Internet zu bewegen - es gibt also keine Nachteile sie zu lernen!",
    'tut-section-1-4':"In den nachfolgenden Tutorials wirst du wichtige Informationen über die einzelnen Bestandteile einer URL selbst sammeln müssen. Lass uns also mit einem kleinen Test starten: \n\n"+ "→ Bewege deinen Cursor über die unten gegebene URL!\n\n",
    'tut-section-1-5':"URLs starten immer mit einem Schema.\n\n"+
                      "Für Webseiten sind dies typischerweise HTTPS oder HTTP, wobei in diesem Spiel ausschließlich HTTPS verwendet wird.\n\n"+
                      "Webseiten, die kein HTTPS verwenden, verschlüsseln deine Eingaben nicht. Daher solltest du auf Webseiten mit HTTP generell vermeiden, private Informationen einzugeben.\n\n"+
                      "Beachte weiterhin, dass dein Browser das Schema nicht immer anzeigt, auch wenn wir es hier im Spiel immer mit angeben.\n\n",
    'tut-section-1-6':"Nach dem Schema folgt der wichtigste Bestandteil der URL: der Host-Teil. " +
    "Er beginnt nach dem Schema und endet mit dem ersten einzelnen Slash \"/\" (oder am Ende der URL, wenn kein Slash mehr vorhanden ist). \n\n"+
                      "Der Host-Teil ist so wichtig, da er die Herkunft der Webseite angibt.\n\n"+
    "Er lässt sich in kleinere Bestandteile, sogenannte \"Domains\" unterteilen, die immer mit einem Punkt getrennt sind.\n\n"+
    "Beim Lesen der Bestandteile im Host-Teil solltest du mit der rechtesten Domain anfangen und dich Domain für Domain nach links arbeiten!\n",
                      //"Dieser Bestandteil kann nicht frei bestimmt werden. Du kannst zum Beispiel die Domain \"https://iownthebestdomain\" nicht einfach registrieren, obwohl diese eine gültige URL darstellt.\n\n"+
                      //"Es gibt stattdessen eine strukturierte Hierarchie für den Host Bestandteil mit Registrierungsstellen bei denen Administratoren Domains registrieren können.\n\n"+
                      //"In der Hierarchie sind die einzelnen Kennungen mit Punkten \".\" abgetrennt und je weiter rechts eine Kennungen, desto höher ist diese Kennung in der Hierarchie.",
    'tut-section-1-6-2': "Hinter dem Hostteil versteckt sich im wesentlichen eine IP-Adresse.\n\nIm alltäglichen Gebrauch wird diese meistens durch eine verständliche Zeichenkette versteckt.\n\n" +
    "Eine IP-Adresse besteht aus 4 Zahlen (zwischen 0 und 255), die durch Punkte abgetrennt werden.\n\n" +
    "Bei solchen URLs musst du vorsichtig sein, da nicht erkenntlich ist, auf welche Webseite sie dich führt.",
    'tut-section-1-7'://"Die Kennung ganz rechts ist die höchste Kennung in der Hierarchie. Diese Kennung stellt als Domain den Registrierungspunkt für andere Domains dar.\n\n"+
                      //"Zum Beispiel befinden sich viele Webseiten unterhalb von \".com\" oder \".de\" in der Hierarchie.\n\n"+
                      //"Die rechteste Kennung wird \"Top-Level Domain\" oder auch einfach \"TLD\" genannt.",
    "Die rechteste Domain heißt Top-Level Domain (TLD) und kann nicht frei gewählt werden.\n\n" +
    "Wenn du im folgenden URLs selbst erstellen musst, solltest du immer ein gültige TLD wählen (z.B. \".de\", oder \".com\").\n",
    'tut-section-1-8'://"Es ist nicht immer möglich eine Domain direkt unterhalb einer TLD zu registrieren.\n\n"+
                      //"Zum Beispiel nutzen viele Webseiten die Form \"website.co.uk\" und nicht \"website.uk\".\n\n"+
                      //"Wenn eine Webseite nicht unterhalb einer Top-Level Domain registriert wird, sondern unterhalb einer weiteren Kennung wie zum Beispiel \".co.uk\", dann bezeichnen wir .co.uk als \"effektive Top-Level Domain\" beziehungsweise \"eTLD\".\n\n"+
                      //"Diese funktioniert genauso wie eine Top-Level Domain und Administratoren können Webseiten unterhalb dieser Domains registrieren.",
  "",
    'tut-section-1-9':"Die Domain, die dann für eine Webseite registriert wird, heißt \"Registrierte Domain\" oder kurz \"RD\". "+
                      "Diese beinhaltet die erste Domain vor der TLD und die TLD selbst.\n\n"+
      "Zum Beispiel besteht der Host-Teil der oben angezeigten URL aus drei Teilen: \"login\", \"amazon\" und \"de\". \"de\" ist die TLD, und \"amazon.de\" ist die Registrierte Domain.\n\n" +
      //"Als Beispiel: Die RD von...\n\"example.co.uk\" ist \"example.co.uk\"\n\"sub.example.co.uk\" ist auch \"example.co.uk\"\n\"sub.example.com\" ist \"example.com\"."+
      "Die Registrierte Domain kann frei gewählt werden, mit einer wichtigen Ausnahme: Es können keine schon existierenden Domains gewählt werden.\n\n"+
      "Daher kann ein Phisher nicht einfach \"amazon.de\" benutzen, sondern muss andere Tricks verwenden, um eine ähnlich-aussehende Domain zu erstellen.",
      //"Diese Tricks wollen wir in späteren Leveln etwas genauer anschauen.",
    'tut-section-1-10':"Erlaubte Zeichen einer Domain sind: Zahlen [0-9], Buchstaben [a-z] und Bindestriche [-]. Einzelne Domains werden immer durch Punkte [.] getrennt\n\n"+
    "Andere Sonderzeichen (z.B. ?, #, %, !) sind in der Domain nicht erlaubt!\n"+
    "Das beinhaltet auch den Slash [/], der das Ende des Host-Teils angibt.\n\n"+
    "In Domains gibt es keinen Unterschied zwischen Groß- und Kleinbuchstaben.",
                       //"Es ist darüber hinaus möglich, auch Sonderzeichen in Domain Kennungen zu verwenden (z. B. \"ä\" oder \"ö\").\n\n"+
                       //"Diese Sonderzeichen werden nicht von allen Browsern unterstützt und sind nicht Teil des Lernspiels.",
    'tut-section-1-11':"Teste nun im ersten Level dein Wissen über den Aufbau von URLs!\n\n"+
    "Um eine URL zu erstellen ziehst du die einzelnen URL Bestandteile in den vorgesehenen Bereich. Anschließend kannst du deine Lösung prüfen.\n\n"+
      //"Schließe das erste Level ab, indem du die Aufgabe durch Ziehen der URL Bestandteile in den vorgesehenen Bereich erfüllst, und mit einem Klick auf \"Prüfen\" abschließt."+
                       //"Schließe das erste Level ab, indem du die Aufgabe durch das Ziehen der URL Bestandteile in den vorgesehenen Bereich löst. "+
                       //"Im Anschluss klicke auf \"Prüfen\", um zu überprüfen, ob deine Antwort korrekt ist. \n\n" +
                       "Viel Erfolg!",
    // Tutorial section 2
    'tut-section-2':"Tutorial II - Phishing-Tricks in der Registrierten Domain (RD)\n\n",
    'tut-section-2-1':"Die Registierte Domain (RD) ist, unter Beachtung der Regeln aus dem letzten Tutorial, frei zu bestimmen. "+
                      "So kann man bereits mit der Registrierten Domain \"böse\" Dinge anstellen!\n\n"+
                      "Zum Beispiel ist es möglich ein Schlüsselwort, etwa \"secure\", in die RD einzubauen. Vielleicht mit einem Bindestrich \"-\", damit die geänderte Domain immernoch wie eine seriöse Domain aussieht!",
    'tut-section-2-1-2': "Eine weitere Methode ist das leichte abändern der URL, was auch Typosquatting genannt wird.\n\nDabei wird ein Buchstabe weggelassen, ersetzt, hinzugefügt oder die Position von zwei Buchstaben geändert.",
    'tut-section-2-2':"Alternativ kannst du auch die TLD (Top-Level Domain) ändern. \n\n"+// (obwohl die \"großen\" Organisationen ihre Webseite bereits unter den meisten TLDs registriert haben werden). \n\n"+
                      "Im folgenden Level wirst du selbst zum Phisher. Hierfür sollst du für verschiedene \"Ziele\" (z.B. PayPal, Apple) eigene Phishing URLs erstellen.\n\n"+
                      "Dafür kannst du neben den vorgeschlagenen URL-Elementen auch über die Eingabezeile neue Elemente erstellen.\n\n"+
                      "Viel Spaß beim Erstellen deiner ersten \"realen\" Phishing URLs!",
    'tut-section-2-3': "Ein Tipp für außerhalb des Spiels:\n\nBist du unsicher ob eine URL bösartig oder legitim ist, suche mit einer Suchmaschine nach der angeblich zugehörigen Webseite!\n\n"+
    "In den meisten Fällen wird dir dann die richtige Webseite als erstes Ergebnis angezeigt und du kannst die URLs vergleichen.",
    // Tutorial section 3
    'tut-section-3':"Tutorial III - Phishing-Tricks in Subdomains \n\n",
    'tut-section-3-1':"Wir haben bereits gesehen, dass der Host-Teil der URL durch Domains strukturiert ist.\n\n"+
                      "Es ist generell möglich vor der Registrierten Domain (RD) beliebig viele weitere sogenannte \"Subdomains\" hinzufügen.\n\n"+
                      "Nicht vergessen: Die RD ist der Teil des Hosts ganz rechts und ist nicht frei wählbar.\n\nAber: Alle Subdomains vor der RD können völlig frei gewählt werden.\n\n",
                      //"Die Kennungen vor der TLD nennt man Subdomains.",
    'tut-section-3-2':"Zum Beispiel ist in der oben angezeigten URL \"amazon\" lediglich eine Subdomain von \"com-evil.ml\".\n\n"+
                      "Dies eröffnet eine große Anzahl an Manipulationsmöglichkeiten für Phishing, indem vor zufällige RDs irreführende Subdomains hinzugefügt werden.\n\n"+
                      "Phisher nutzen diese Technik oft bei mobilen Geräten mit einem kleinen Bildschirm. So sieht man nur den Beginn der URL in der Browserleiste und nicht die wahre Identität hinter der URL.",
                      //"Wenn dir eine (registrierte) Domain gehört, ist es generell möglich alle Bestandteile einer URL zu ändern.\n\n"+
                      //"Die einzige Ausnahme stellt die RD dar, welche immer am Ende des Hosts einer URL ist (Nicht vergessen, die RD steht immer direkt vor dem ersten einzelnen Slash einer URL). "+
                      //"Zur Erinnerung: Da die RD sich von allen exitierenden RDs unterscheiden muss ist sie nicht frei wählbar, und spielt eine sehr wichtige Rolle beim Identifizieren einer Webseite.",
    'tut-section-3-3':"Im nächsten Level wirst du zu einer gegebenen Registrierten Domain (RD) eine Subdomain hinzufügen müssen, die das Ziel enthält. \n\n"+
                      "Versuche möglichst überzeugende aber irreführende Subdomains zu konstruieren!\n\n"+
                      "Fröhliches Phishen!",
    // Tutorial section 4
    'tut-section-4':"Tutorial IV - Phishing-Tricks im Pfad\n\n",
    'tut-section-4-1':"Das letzte Element der URL ist der Pfad. "+
    "Er folgt auf den Host und beginnt mit einem einfachen Slash \"/\".\n\n" +
      //" Nach dem Host kann ein Pfad hinzugefügt werden.\n\n"+
      "Während die Registrierte Domain (RD) angibt, wer eine Webseite anbietet, beschreibt der Pfad welche (Unter-) Webseite angezeigt wird.\n\n"
      +"Manchmal werden im Pfad auch bestimmte Parameter angegeben.",
                      //"Dies ist vergleichbar zu der Verzeichnis- und Ordnerstruktur auf deinem eigenen Computer.",
    'tut-section-4-2':"",//"Im Pfad kann, anders als im Host, Groß- und Kleinschreibung beachtet werden und es können mehr Zeichen enthalten sein. \n\n"+ //(Diese werden codiert während der Übertragung, aber werden oft im Browser angezeigt).\n\n"+
                      //"Betrachtet man den Pfad, kann man oft zusätzliche Informationen über die Webseite sehen, die man gerade im Browser betrachtet. \n\n",
                      //"Der Pfad beendet entweder die URL oder endet mit einem Fragezeichen \"?\" (je nachdem was zuerst vorkommt).",
    'tut-section-4-3':"Wichtig zu wissen ist, dass der Pfad ebenfalls völlig frei wählbar ist! "+
                      "Die einzelnen Pfadbestandteile werden per Slash \"/\" abgetrennt. \n\n"+
    /*"Nach dem \"?\" folgt eine Abfrage. Die folgenden Parameter werden an die im Pfad spezifizierte Ressource übermittelt.\n"+
                      "Wie auch immer, dieser Unterschied ist schon etwas anspruchsvoller und wird nicht benötigt beim Spielen des folgenden Levels.\n\n"+
                      */
                      "Lass uns jetzt den Pfad für Phishing nutzen, indem wir dort die Registrierte Domain des Ziels einfügen!",
    // Tutorial section 5
    'tut-section-5-1':"Wir hoffen du hast in den letzten Leveln gelernt, welche Bestandteile einer URL wichtig sind um Phishing Websiten zu erkennen, und welche zur Täuschung genutzt werden können.\n\n"+
    "Du kannst nun das finale Level starten, welches all dein Wissen zu URLs und den Möglichkeiten der Manipulation einer URL testet.\n\n"+
                      "Viel Erfolg!",
    // Tutorial URL parts
    'url-scheme': "Schema",
    'url-rd': "Registrierte Domain",
    'url-host': "Host",
    'url-subdomain': "Subdomain",
    'url-path': "Pfad",
    'url-query': "Abfrage",
    'url-fragment': "Fragment",
    'url-tld': "Top-Level Domain",
    'url-etld': "eTLD",
    'url-questionMark': "?",
    'copy-button': "Kopieren",
    'copy-button-success': "Kopiert!",
    'playerid' : "Player-ID",
    'help': "Hilfe"
  },
});
