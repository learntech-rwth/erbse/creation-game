/**
 * @Date:   2019-03-06T15:07:33+01:00
 * @Last modified time: 2019-07-03T19:32:48+02:00
 */



MTLG.loadOptions({
  "width":1920, //game width in pixels
  "height":1080, //game height in pixels
  "languages":["en","de"], //Supported languages. First language should be the most complete.
  "countdown":180, //idle time countdown
  "fps":"60", //Frames per second
  "playernumber":4, //Maximum number of supported players
  "FilterTouches": true, //Tangible setting: true means a tangible is recognized as one touch event. False: 4 events.
  "webgl": false, //Using webgl enables using more graphical effects
  "overlayMenu": false, //Starting the overlay menu automatically
  "LCDMbaseUrl": 'localhost:5080',
  "useCountdown": false,
  "cheatMode": false,
  "devMode": true,
  "movement": "clickdrag",
  "uuidTextFont": "42px Arial",
  "logServerBaseUrl": "",//"https://erbse.elearn.rwth-aachen.de",
  "characterImg" : "hacker-new.png",
});
