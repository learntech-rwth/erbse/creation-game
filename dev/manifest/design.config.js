/**
 * @Date:   2019-03-06T15:07:33+01:00
 * @Last modified time: 2019-07-03T19:32:48+02:00
 */



MTLG.loadOptions({
  "bgColor": "#4b6584",//"#2d3258", // background color
  "title_txtColor":"#d1d8e0",
  "title_fontSize":"60px",
  "menubutton_bgColor": "#f7b731",
  "button_bgColor1": "#1B9CFC",//"#4b7bec",//"#2494a2",
  "button_txtColor1": "#000000",//"#083878",
  "button_bgColor2": "#2C3A47",//"#576574",//"#cc0000",
  "button_txtColor2": "#ffffff",
  "box_bgColor": "#ffffff",
  "box_alpha": 1.0,
  "text_txtColor": "#9E9E9E",
  "text_bgColor": "#ffffff",
  "text_padding": {
    lr: 25, //left-right padding
    tb: 8 //top-bottom padding
  },
  "text_fontSize": "30px",
  "text_font": "Arial",
  "text_lineWidth": 900,
  "textbox_txtColor": "#000000",
  "textbox_bgColor": "#ffffff",
  "textbox_strkColor": "#000000" ,
  "textbox_padding" : {
    lr: 25, //left-right padding
    tb: 8 //top-bottom padding
  },
  "textboxTutorial_fontSize": "46px Arial",
  "textboxTutorial_txtColor": "#000000",
  "textboxTutorial_bgColor": "#a5b1c2",//"#2d3258",//MTLG.getOptions().bgColor,
  "textboxTutorial_padding": {
    lr: 25, //left-right padding
    tb: 8 //top-bottom padding
  },
  "textboxTutorial2_bgColor": "#778ca3",
  "textboxTutorial2_txtColor": "#ffffff",
  "urlPart_lineView": "#eb3b5a",//"#ff0000",
  "urlPart_explanation_bgColor": "#d1d8e0",//"#ffffff",
  "urlPart_explanation_txtColor": "#000000",
  "urlPart_explanation_fontSize": "30px Arial",
  "urlPart_explanation_padding": {
    lr: 8, //left-right padding
    tb: 8 //top-bottom padding
  },
  "textbox_urlPart_explanation_bgColor": "#a5b1c2",
  "textbox_urlPart_explanation_txtColor": "#000000",
  "textbox_result_bgColor": "#182C61",
  "textbox_result_txtColor": "#ffffff",
  "textbox_result_fontSize": "bold 40px",
  "textbox_countdown_bgColor": "#182C61",
  "textbox_countdown_txtColor": "#ffffff",
  "textbox_countdown_fontSize": "bold 40px",
  "textbox_statusArea_bgColor": "#304b78",
  "textbox_statusArea_txtColor": "#ffffff",
  "textbox_statusArea_fontSize": "bold 35px",
  "urlPart_bgColor": "#ffffff",
  "urlPart_txtColor": "#fa8231",
  "urlPart_generated_txtColor": "#fa8231",//"#fa8231",
  "urlPart_generated_bgColor": "#d2fef9",
  "countdown_color": "#ffffff",
});
