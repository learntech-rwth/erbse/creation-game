# A Phisher's Bag of Tricks (Creation Game)

The game is automatically deployed to https://lernspiele.informatik.rwth-aachen.de/games/creation-game/

## Gameplay 

To understand the different techniques of creating a phishing URL, the player is taught the URL structure and has to manipulate existing URLs in A Phisher's Bag of Tricks, turning them into potential phishing-URLs. 

## MTLG-Framework 

The game was created using the MTLG-Framework (Multitouch-Learning-Game-Framework). It was developed as an aid to create platform-independent educational games using JavaScript with support for large Multitouch-Displays. The framework can be found [here](https://gitlab.com/mtlg-framework/mtlg-gameframe). 

## Building the game 

A tutorial on how to build the game yourself from this repository can be found [here](https://gitlab.com/mtlg-framework/mtlg-gameframe/-/wikis/mtlg/installation).
